package com.cargo.ground.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 联程航班管理
 */
public class FlightManagement  implements Serializable {
    //ID
    private Long id;

    //航班号ID
    private Long flightId;

    //航班开始日期
    private Date beginDate;

    //航班结束日期
    private Date endDate;

    //班期
    private String schedule;

    //起飞站
    private Long takeOff;

    //目的站
    private Long purpose;

    //预计起飞时间
    private Date takeOffDate;

    //预计降落时间
    private Date purposeDate;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取航班号ID
     *
     * @return FLIGHT_ID - 航班号ID
     */
    public Long getFlightId() {
        return flightId;
    }

    /**
     * 设置航班号ID
     *
     * @param flightId 航班号ID
     */
    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    /**
     * 获取航班开始日期
     *
     * @return BEGIN_DATE - 航班开始日期
     */
    public Date getBeginDate() {
        return beginDate;
    }

    /**
     * 设置航班开始日期
     *
     * @param beginDate 航班开始日期
     */
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    /**
     * 获取航班结束日期
     *
     * @return END_DATE - 航班结束日期
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * 设置航班结束日期
     *
     * @param endDate 航班结束日期
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取班期
     *
     * @return SCHEDULE - 班期
     */
    public String getSchedule() {
        return schedule;
    }

    /**
     * 设置班期
     *
     * @param schedule 班期
     */
    public void setSchedule(String schedule) {
        this.schedule = schedule == null ? null : schedule.trim();
    }

    /**
     * 获取起飞站
     *
     * @return TAKE_OFF - 起飞站
     */
    public Long getTakeOff() {
        return takeOff;
    }

    /**
     * 设置起飞站
     *
     * @param takeOff 起飞站
     */
    public void setTakeOff(Long takeOff) {
        this.takeOff = takeOff;
    }

    /**
     * 获取目的站
     *
     * @return PURPOSE - 目的站
     */
    public Long getPurpose() {
        return purpose;
    }

    /**
     * 设置目的站
     *
     * @param purpose 目的站
     */
    public void setPurpose(Long purpose) {
        this.purpose = purpose;
    }

    /**
     * 获取预计起飞时间
     *
     * @return TAKE_OFF_DATE - 预计起飞时间
     */
    public Date getTakeOffDate() {
        return takeOffDate;
    }

    /**
     * 设置预计起飞时间
     *
     * @param takeOffDate 预计起飞时间
     */
    public void setTakeOffDate(Date takeOffDate) {
        this.takeOffDate = takeOffDate;
    }

    /**
     * 获取预计降落时间
     *
     * @return PURPOSE_DATE - 预计降落时间
     */
    public Date getPurposeDate() {
        return purposeDate;
    }

    /**
     * 设置预计降落时间
     *
     * @param purposeDate 预计降落时间
     */
    public void setPurposeDate(Date purposeDate) {
        this.purposeDate = purposeDate;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}