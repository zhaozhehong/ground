package com.cargo.ground.entity;

import java.util.Date;

public class RunAptitude {
    //主键
    private Long id;

    //限制条件
    private String limitCondition;

    //限制条件具体内容
    private String limitContent;

    //承运人
    private String carrier;

    //航班号
    private String airNum;

    //飞机号
    private String aircraftNum;

    //机型
    private String aircraftType;

    //航线
    private String airLine;

    //有效期类型
    private String periodType;

    //限制环节
    private String limitLink;

    //提示语
    private String cueWords;

    //数据维护部门
    private String maintainDept;

    //起始日期
    private Date startDate;

    //截止日期
    private Date endDate;

    //限制措施
    private String limitMeasure;

    //维护人员
    private String maintainer;

    //维护时间
    private Date maintenanceTime;

    //航班id的集合
    private String airLineIds;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取限制条件
     *
     * @return LIMIT_CONDITION - 限制条件
     */
    public String getLimitCondition() {
        return limitCondition;
    }

    /**
     * 设置限制条件
     *
     * @param limitCondition 限制条件
     */
    public void setLimitCondition(String limitCondition) {
        this.limitCondition = limitCondition == null ? null : limitCondition.trim();
    }

    /**
     * 获取限制条件具体内容
     *
     * @return LIMIT_CONTENT - 限制条件具体内容
     */
    public String getLimitContent() {
        return limitContent;
    }

    /**
     * 设置限制条件具体内容
     *
     * @param limitContent 限制条件具体内容
     */
    public void setLimitContent(String limitContent) {
        this.limitContent = limitContent == null ? null : limitContent.trim();
    }

    /**
     * 获取承运人
     *
     * @return CARRIER - 承运人
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * 设置承运人
     *
     * @param carrier 承运人
     */
    public void setCarrier(String carrier) {
        this.carrier = carrier == null ? null : carrier.trim();
    }

    /**
     * 获取航班号
     *
     * @return AIR_NUM - 航班号
     */
    public String getAirNum() {
        return airNum;
    }

    /**
     * 设置航班号
     *
     * @param airNum 航班号
     */
    public void setAirNum(String airNum) {
        this.airNum = airNum == null ? null : airNum.trim();
    }

    /**
     * 获取飞机号
     *
     * @return AIRCRAFT_NUM - 飞机号
     */
    public String getAircraftNum() {
        return aircraftNum;
    }

    /**
     * 设置飞机号
     *
     * @param aircraftNum 飞机号
     */
    public void setAircraftNum(String aircraftNum) {
        this.aircraftNum = aircraftNum == null ? null : aircraftNum.trim();
    }

    /**
     * 获取机型
     *
     * @return AIRCRAFT_TYPE - 机型
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * 设置机型
     *
     * @param aircraftType 机型
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType == null ? null : aircraftType.trim();
    }

    /**
     * 获取航线
     *
     * @return AIR_LINE - 航线
     */
    public String getAirLine() {
        return airLine;
    }

    /**
     * 设置航线
     *
     * @param airLine 航线
     */
    public void setAirLine(String airLine) {
        this.airLine = airLine == null ? null : airLine.trim();
    }

    /**
     * 获取有效期类型
     *
     * @return PERIOD_TYPE - 有效期类型
     */
    public String getPeriodType() {
        return periodType;
    }

    /**
     * 设置有效期类型
     *
     * @param periodType 有效期类型
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType == null ? null : periodType.trim();
    }

    /**
     * 获取限制环节
     *
     * @return LIMIT_LINK - 限制环节
     */
    public String getLimitLink() {
        return limitLink;
    }

    /**
     * 设置限制环节
     *
     * @param limitLink 限制环节
     */
    public void setLimitLink(String limitLink) {
        this.limitLink = limitLink == null ? null : limitLink.trim();
    }

    /**
     * 获取提示语
     *
     * @return CUE_WORDS - 提示语
     */
    public String getCueWords() {
        return cueWords;
    }

    /**
     * 设置提示语
     *
     * @param cueWords 提示语
     */
    public void setCueWords(String cueWords) {
        this.cueWords = cueWords == null ? null : cueWords.trim();
    }

    /**
     * 获取数据维护部门
     *
     * @return MAINTAIN_DEPT - 数据维护部门
     */
    public String getMaintainDept() {
        return maintainDept;
    }

    /**
     * 设置数据维护部门
     *
     * @param maintainDept 数据维护部门
     */
    public void setMaintainDept(String maintainDept) {
        this.maintainDept = maintainDept == null ? null : maintainDept.trim();
    }

    /**
     * 获取起始日期
     *
     * @return START_DATE - 起始日期
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * 设置起始日期
     *
     * @param startDate 起始日期
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 获取截止日期
     *
     * @return END_DATE - 截止日期
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * 设置截止日期
     *
     * @param endDate 截止日期
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取限制措施
     *
     * @return LIMIT_MEASURE - 限制措施
     */
    public String getLimitMeasure() {
        return limitMeasure;
    }

    /**
     * 设置限制措施
     *
     * @param limitMeasure 限制措施
     */
    public void setLimitMeasure(String limitMeasure) {
        this.limitMeasure = limitMeasure == null ? null : limitMeasure.trim();
    }

    /**
     * 获取维护人员
     *
     * @return MAINTAINER - 维护人员
     */
    public String getMaintainer() {
        return maintainer;
    }

    /**
     * 设置维护人员
     *
     * @param maintainer 维护人员
     */
    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer == null ? null : maintainer.trim();
    }

    /**
     * 获取维护时间
     *
     * @return MAINTENANCE_TIME - 维护时间
     */
    public Date getMaintenanceTime() {
        return maintenanceTime;
    }

    /**
     * 设置维护时间
     *
     * @param maintenanceTime 维护时间
     */
    public void setMaintenanceTime(Date maintenanceTime) {
        this.maintenanceTime = maintenanceTime;
    }

    /**
     * 获取航班id的集合
     *
     * @return AIR_LINE_IDS - 航班id的集合
     */
    public String getAirLineIds() {
        return airLineIds;
    }

    /**
     * 设置航班id的集合
     *
     * @param airLineIds 航班id的集合
     */
    public void setAirLineIds(String airLineIds) {
        this.airLineIds = airLineIds == null ? null : airLineIds.trim();
    }
}