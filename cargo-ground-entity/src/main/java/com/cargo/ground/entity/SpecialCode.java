package com.cargo.ground.entity;

import java.util.Date;

/**
 * 特码管理
 */
public class SpecialCode {
    //ID
    private Long id;

    //特种货物代码
    private String code;

    //含义
    private String meaning;

    //自提单号
    private Long isSelf;

    //中文简写
    private String chineseShorthand;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取特种货物代码
     *
     * @return CODE - 特种货物代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置特种货物代码
     *
     * @param code 特种货物代码
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取含义
     *
     * @return MEANING - 含义
     */
    public String getMeaning() {
        return meaning;
    }

    /**
     * 设置含义
     *
     * @param meaning 含义
     */
    public void setMeaning(String meaning) {
        this.meaning = meaning == null ? null : meaning.trim();
    }

    /**
     * 获取自提单号
     *
     * @return IS_SELF - 自提单号
     */
    public Long getIsSelf() {
        return isSelf;
    }

    /**
     * 设置自提单号
     *
     * @param isSelf 自提单号
     */
    public void setIsSelf(Long isSelf) {
        this.isSelf = isSelf;
    }

    /**
     * 获取中文简写
     *
     * @return CHINESE_SHORTHAND - 中文简写
     */
    public String getChineseShorthand() {
        return chineseShorthand;
    }

    /**
     * 设置中文简写
     *
     * @param chineseShorthand 中文简写
     */
    public void setChineseShorthand(String chineseShorthand) {
        this.chineseShorthand = chineseShorthand == null ? null : chineseShorthand.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}