package com.cargo.ground.entity;

import java.util.Date;

/**
 * 电报地址管理
 * @author: liyiting
 */
public class TelegraphAddress {
    //ID
    private Long id;

    //电报地址
    private String telegraphAddress;

    //电报地址组
    private String addressGroup;

    //地址
    private String address;

    //目的站三字代码
    private String purposeCode;

    //备注
    private String remark;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取电报地址
     *
     * @return TELEGRAPH_ADDRESS - 电报地址
     */
    public String getTelegraphAddress() {
        return telegraphAddress;
    }

    /**
     * 设置电报地址
     *
     * @param telegraphAddress 电报地址
     */
    public void setTelegraphAddress(String telegraphAddress) {
        this.telegraphAddress = telegraphAddress == null ? null : telegraphAddress.trim();
    }

    /**
     * 获取电报地址组
     *
     * @return ADDRESS_GROUP - 电报地址组
     */
    public String getAddressGroup() {
        return addressGroup;
    }

    /**
     * 设置电报地址组
     *
     * @param addressGroup 电报地址组
     */
    public void setAddressGroup(String addressGroup) {
        this.addressGroup = addressGroup == null ? null : addressGroup.trim();
    }

    /**
     * 获取地址
     *
     * @return ADDRESS - 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置地址
     *
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取目的站三字代码
     *
     * @return PURPOSE_CODE - 目的站三字代码
     */
    public String getPurposeCode() {
        return purposeCode;
    }

    /**
     * 设置目的站三字代码
     *
     * @param purposeCode 目的站三字代码
     */
    public void setPurposeCode(String purposeCode) {
        this.purposeCode = purposeCode == null ? null : purposeCode.trim();
    }

    /**
     * 获取备注
     *
     * @return REMARK - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}