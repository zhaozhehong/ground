package com.cargo.ground.entity;

import java.util.Date;

public class BatteryAirLine {
    //主键
    private Long id;

    //航班号
    private String airNum;

    //目的站
    private String endStation;

    //承运人
    private String carrier;

    //开始时间
    private Date startTime;

    //结束时间
    private Date endTime;

    //备注信息
    private String remark;

    //创建人
    private String creator;

    //创建时间
    private Date createTime;

    //修改人
    private String modifier;

    //修改时间
    private Date updateTime;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取航班号
     *
     * @return AIR_NUM - 航班号
     */
    public String getAirNum() {
        return airNum;
    }

    /**
     * 设置航班号
     *
     * @param airNum 航班号
     */
    public void setAirNum(String airNum) {
        this.airNum = airNum == null ? null : airNum.trim();
    }

    /**
     * 获取目的站
     *
     * @return END_STATION - 目的站
     */
    public String getEndStation() {
        return endStation;
    }

    /**
     * 设置目的站
     *
     * @param endStation 目的站
     */
    public void setEndStation(String endStation) {
        this.endStation = endStation == null ? null : endStation.trim();
    }

    /**
     * 获取承运人
     *
     * @return CARRIER - 承运人
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * 设置承运人
     *
     * @param carrier 承运人
     */
    public void setCarrier(String carrier) {
        this.carrier = carrier == null ? null : carrier.trim();
    }

    /**
     * 获取开始时间
     *
     * @return START_TIME - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return END_TIME - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取备注信息
     *
     * @return REMARK - 备注信息
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注信息
     *
     * @param remark 备注信息
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取创建人
     *
     * @return CREATOR - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_TIME - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改人
     *
     * @return MODIFIER - 修改人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置修改人
     *
     * @param modifier 修改人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_TIME - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}