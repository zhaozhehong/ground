package com.cargo.ground.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 航班基础信息表
 */
public class FlightInfo  implements Serializable {
    //ID
    private Long id;

    //航班号
    private String flightNumber;

    //机型
    private String flightModel;

    //航程(KM)
    private Long flightRange;

    //货机标识
    private Long isCargoPlane;

    //总重量
    private Long flightWeight;

    //总体积
    private Long flightVolume;

    //开放比例
    private Long proportion;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取航班号
     *
     * @return FLIGHT_NUMBER - 航班号
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * 设置航班号
     *
     * @param flightNumber 航班号
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * 获取机型
     *
     * @return FLIGHT_MODEL - 机型
     */
    public String getFlightModel() {
        return flightModel;
    }

    /**
     * 设置机型
     *
     * @param flightModel 机型
     */
    public void setFlightModel(String flightModel) {
        this.flightModel = flightModel == null ? null : flightModel.trim();
    }

    /**
     * 获取航程(KM)
     *
     * @return FLIGHT_RANGE - 航程(KM)
     */
    public Long getFlightRange() {
        return flightRange;
    }

    /**
     * 设置航程(KM)
     *
     * @param flightRange 航程(KM)
     */
    public void setFlightRange(Long flightRange) {
        this.flightRange = flightRange;
    }

    /**
     * 获取货机标识
     *
     * @return IS_CARGO_PLANE - 货机标识
     */
    public Long getIsCargoPlane() {
        return isCargoPlane;
    }

    /**
     * 设置货机标识
     *
     * @param isCargoPlane 货机标识
     */
    public void setIsCargoPlane(Long isCargoPlane) {
        this.isCargoPlane = isCargoPlane;
    }

    /**
     * 获取总重量
     *
     * @return FLIGHT_WEIGHT - 总重量
     */
    public Long getFlightWeight() {
        return flightWeight;
    }

    /**
     * 设置总重量
     *
     * @param flightWeight 总重量
     */
    public void setFlightWeight(Long flightWeight) {
        this.flightWeight = flightWeight;
    }

    /**
     * 获取总体积
     *
     * @return FLIGHT_VOLUME - 总体积
     */
    public Long getFlightVolume() {
        return flightVolume;
    }

    /**
     * 设置总体积
     *
     * @param flightVolume 总体积
     */
    public void setFlightVolume(Long flightVolume) {
        this.flightVolume = flightVolume;
    }

    /**
     * 获取开放比例
     *
     * @return PROPORTION - 开放比例
     */
    public Long getProportion() {
        return proportion;
    }

    /**
     * 设置开放比例
     *
     * @param proportion 开放比例
     */
    public void setProportion(Long proportion) {
        this.proportion = proportion;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}