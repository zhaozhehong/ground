package com.cargo.ground.entity;

import java.util.Date;

public class Proxy {
    //主键
    private Long id;

    //航深代理人代码
    private String airAgentCode;

    //物流园代理人代码
    private String logisticsAgentCode;

    //物流园代理人名称
    private String logisticsAgentName;

    //物流园代理人描述
    private String logisticsAgentRemark;

    //操作日期
    private Date operationDate;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取航深代理人代码
     *
     * @return AIR_AGENT_CODE - 航深代理人代码
     */
    public String getAirAgentCode() {
        return airAgentCode;
    }

    /**
     * 设置航深代理人代码
     *
     * @param airAgentCode 航深代理人代码
     */
    public void setAirAgentCode(String airAgentCode) {
        this.airAgentCode = airAgentCode == null ? null : airAgentCode.trim();
    }

    /**
     * 获取物流园代理人代码
     *
     * @return LOGISTICS_AGENT_CODE - 物流园代理人代码
     */
    public String getLogisticsAgentCode() {
        return logisticsAgentCode;
    }

    /**
     * 设置物流园代理人代码
     *
     * @param logisticsAgentCode 物流园代理人代码
     */
    public void setLogisticsAgentCode(String logisticsAgentCode) {
        this.logisticsAgentCode = logisticsAgentCode == null ? null : logisticsAgentCode.trim();
    }

    /**
     * 获取物流园代理人名称
     *
     * @return LOGISTICS_AGENT_NAME - 物流园代理人名称
     */
    public String getLogisticsAgentName() {
        return logisticsAgentName;
    }

    /**
     * 设置物流园代理人名称
     *
     * @param logisticsAgentName 物流园代理人名称
     */
    public void setLogisticsAgentName(String logisticsAgentName) {
        this.logisticsAgentName = logisticsAgentName == null ? null : logisticsAgentName.trim();
    }

    /**
     * 获取物流园代理人描述
     *
     * @return LOGISTICS_AGENT_REMARK - 物流园代理人描述
     */
    public String getLogisticsAgentRemark() {
        return logisticsAgentRemark;
    }

    /**
     * 设置物流园代理人描述
     *
     * @param logisticsAgentRemark 物流园代理人描述
     */
    public void setLogisticsAgentRemark(String logisticsAgentRemark) {
        this.logisticsAgentRemark = logisticsAgentRemark == null ? null : logisticsAgentRemark.trim();
    }

    /**
     * 获取操作日期
     *
     * @return OPERATION_DATE - 操作日期
     */
    public Date getOperationDate() {
        return operationDate;
    }

    /**
     * 设置操作日期
     *
     * @param operationDate 操作日期
     */
    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }
}