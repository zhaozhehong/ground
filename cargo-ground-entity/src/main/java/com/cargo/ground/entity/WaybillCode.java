package com.cargo.ground.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 运单特码
 * @author: liyiting
 */
public class WaybillCode  implements Serializable {
    //ID
    private Long id;

    //特殊代码
    private String code;

    //特码名称
    private String name;

    //简写
    private String shortening;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取特殊代码
     *
     * @return CODE - 特殊代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置特殊代码
     *
     * @param code 特殊代码
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取特码名称
     *
     * @return NAME - 特码名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置特码名称
     *
     * @param name 特码名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取简写
     *
     * @return SHORTENING - 简写
     */
    public String getShortening() {
        return shortening;
    }

    /**
     * 设置简写
     *
     * @param shortening 简写
     */
    public void setShortening(String shortening) {
        this.shortening = shortening == null ? null : shortening.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}