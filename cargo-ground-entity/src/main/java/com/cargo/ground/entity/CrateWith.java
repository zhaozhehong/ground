package com.cargo.ground.entity;

import java.io.Serializable;

/**
 * 板箱搭配
 * @author: liyiting
 */
public class CrateWith  implements Serializable {
    //ID
    private Long id;

    //机型ID
    private Long modelId;

    //板
    private Long board;

    //箱
    private Long box;

    //备注
    private String remark;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取机型ID
     *
     * @return MODEL_ID - 机型ID
     */
    public Long getModelId() {
        return modelId;
    }

    /**
     * 设置机型ID
     *
     * @param modelId 机型ID
     */
    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    /**
     * 获取板
     *
     * @return BOARD - 板
     */
    public Long getBoard() {
        return board;
    }

    /**
     * 设置板
     *
     * @param board 板
     */
    public void setBoard(Long board) {
        this.board = board;
    }

    /**
     * 获取箱
     *
     * @return BOX - 箱
     */
    public Long getBox() {
        return box;
    }

    /**
     * 设置箱
     *
     * @param box 箱
     */
    public void setBox(Long box) {
        this.box = box;
    }

    /**
     * 获取备注
     *
     * @return REMARK - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}