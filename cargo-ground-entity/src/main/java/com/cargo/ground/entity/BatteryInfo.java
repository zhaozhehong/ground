package com.cargo.ground.entity;

import java.util.Date;

public class BatteryInfo {
    //主键
    private Long id;

    //设备和锂电池型号
    private String devicesBaryType;

    //锂电池型号
    private String batteryType;

    //设备型号
    private String devicesType;

    //开始时间
    private Date startTime;

    //截止时间
    private Date endTime;

    //添加人
    private String creator;

    //添加时间
    private Date createTime;

    //备注
    private String remark;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取设备和锂电池型号
     *
     * @return DEVICES_BARY_TYPE - 设备和锂电池型号
     */
    public String getDevicesBaryType() {
        return devicesBaryType;
    }

    /**
     * 设置设备和锂电池型号
     *
     * @param devicesBaryType 设备和锂电池型号
     */
    public void setDevicesBaryType(String devicesBaryType) {
        this.devicesBaryType = devicesBaryType == null ? null : devicesBaryType.trim();
    }

    /**
     * 获取锂电池型号
     *
     * @return BATTERY_TYPE - 锂电池型号
     */
    public String getBatteryType() {
        return batteryType;
    }

    /**
     * 设置锂电池型号
     *
     * @param batteryType 锂电池型号
     */
    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType == null ? null : batteryType.trim();
    }

    /**
     * 获取设备型号
     *
     * @return DEVICES_TYPE - 设备型号
     */
    public String getDevicesType() {
        return devicesType;
    }

    /**
     * 设置设备型号
     *
     * @param devicesType 设备型号
     */
    public void setDevicesType(String devicesType) {
        this.devicesType = devicesType == null ? null : devicesType.trim();
    }

    /**
     * 获取开始时间
     *
     * @return START_TIME - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取截止时间
     *
     * @return END_TIME - 截止时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置截止时间
     *
     * @param endTime 截止时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取添加人
     *
     * @return CREATOR - 添加人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置添加人
     *
     * @param creator 添加人
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * 获取添加时间
     *
     * @return CREATE_TIME - 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置添加时间
     *
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取备注
     *
     * @return REMARK - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}