package com.cargo.ground.entity;

import java.util.Date;

/**
 * 串口设置
 */
public class SerialPortSetting {
    //ID
    private Long id;

    //端口
    private Long port;

    //波特率
    private Long baudRate;

    //校验位
    private Long checkBit;

    //数据位
    private Long numberBit;

    //停止位
    private Long stopBit;

    //端口状态
    private Long portStatus;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取端口
     *
     * @return PORT - 端口
     */
    public Long getPort() {
        return port;
    }

    /**
     * 设置端口
     *
     * @param port 端口
     */
    public void setPort(Long port) {
        this.port = port;
    }

    /**
     * 获取波特率
     *
     * @return BAUD_RATE - 波特率
     */
    public Long getBaudRate() {
        return baudRate;
    }

    /**
     * 设置波特率
     *
     * @param baudRate 波特率
     */
    public void setBaudRate(Long baudRate) {
        this.baudRate = baudRate;
    }

    /**
     * 获取校验位
     *
     * @return CHECK_BIT - 校验位
     */
    public Long getCheckBit() {
        return checkBit;
    }

    /**
     * 设置校验位
     *
     * @param checkBit 校验位
     */
    public void setCheckBit(Long checkBit) {
        this.checkBit = checkBit;
    }

    /**
     * 获取数据位
     *
     * @return NUMBER_BIT - 数据位
     */
    public Long getNumberBit() {
        return numberBit;
    }

    /**
     * 设置数据位
     *
     * @param numberBit 数据位
     */
    public void setNumberBit(Long numberBit) {
        this.numberBit = numberBit;
    }

    /**
     * 获取停止位
     *
     * @return STOP_BIT - 停止位
     */
    public Long getStopBit() {
        return stopBit;
    }

    /**
     * 设置停止位
     *
     * @param stopBit 停止位
     */
    public void setStopBit(Long stopBit) {
        this.stopBit = stopBit;
    }

    /**
     * 获取端口状态
     *
     * @return PORT_STATUS - 端口状态
     */
    public Long getPortStatus() {
        return portStatus;
    }

    /**
     * 设置端口状态
     *
     * @param portStatus 端口状态
     */
    public void setPortStatus(Long portStatus) {
        this.portStatus = portStatus;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}