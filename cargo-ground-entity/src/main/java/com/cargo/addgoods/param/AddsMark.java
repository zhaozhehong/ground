package com.cargo.addgoods.param;

import java.util.List;

/**
 * @Desc 加货备注接口参数
 * @LastPeson xuxu
 **/
public class AddsMark {

   private Double volume;
   private String spaces;
   private String containerNum;
   private List<String> codes;

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getSpaces() {
        return spaces;
    }

    public void setSpaces(String spaces) {
        this.spaces = spaces;
    }

    public String getContainerNum() {
        return containerNum;
    }

    public void setContainerNum(String containerNum) {
        this.containerNum = containerNum;
    }

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }
}
