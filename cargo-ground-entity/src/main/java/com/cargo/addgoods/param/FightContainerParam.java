package com.cargo.addgoods.param;

/**
 * @Desc
 * @LastPeson xuxu
 **/
public class FightContainerParam {

    String flightNum;
    String containerNum;

    public String getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(String flightNum) {
        this.flightNum = flightNum;
    }

    public String getContainerNum() {
        return containerNum;
    }

    public void setContainerNum(String containerNum) {
        this.containerNum = containerNum;
    }
}
