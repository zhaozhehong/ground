package com.cargo.addgoods.enums;

/**
 * @Desc 运单状态
 *    0 未收运  1 已收运 2 退货 3 异常
 * @LastPeson xuxu
 **/
public enum BillEnum {

    UN_RECEIVE("未收运",0),
    RECEIVE("已收运",1),
    BE_RETURNED("退货",2),
    EXP("异常",3);

    private String desc;
    private Integer value;

    BillEnum(String _desc,Integer _value) {
        this.desc = _desc;
        this.value = _value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
