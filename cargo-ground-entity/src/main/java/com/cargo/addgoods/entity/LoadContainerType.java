package com.cargo.addgoods.entity;

import java.io.Serializable;
//容器类型
import java.util.Date;
public class LoadContainerType implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long containerTypeId;
	private Long modelId;
	private String containerType;
	private Long maxWeight;
	private Float maxBulk;
	private String createUser;
	private String updateUser;
	private Date createTime;
	private Date updateTime;
	private Integer isDeleted;
	public Long getContainerTypeId() {
		return containerTypeId;
	}
	public void setContainerTypeId(Long containerTypeId) {
		this.containerTypeId = containerTypeId;
	}
	public Long getModelId() {
		return modelId;
	}
	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}
	public String getContainerType() {
		return containerType;
	}
	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}
	public Long getMaxWeight() {
		return maxWeight;
	}
	public void setMaxWeight(Long maxWeight) {
		this.maxWeight = maxWeight;
	}
	public Float getMaxBulk() {
		return maxBulk;
	}
	public void setMaxBulk(Float maxBulk) {
		this.maxBulk = maxBulk;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	@Override
	public String toString() {
		return "LoadContainerType [containerTypeId=" + containerTypeId + ", modelId=" + modelId + ", containerType="
				+ containerType + ", maxWeight=" + maxWeight + ", maxBulk=" + maxBulk + ", createUser=" + createUser
				+ ", updateUser=" + updateUser + ", createTime=" + createTime + ", updateTime=" + updateTime
				+ ", isDeleted=" + isDeleted + "]";
	}

	
	
}
