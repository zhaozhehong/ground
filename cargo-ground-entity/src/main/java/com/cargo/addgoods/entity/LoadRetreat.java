package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;

public class LoadRetreat implements Serializable {
	private static final long serialVersionUID = 1l;

    /**
     * 退运id
     */
    private Long retreatId;
	  /**
     * 收运重量
     */
    private Long collectionWeight;
    /**
     * 收运件数
     */
    private Long collectionAmount;
    /**
     * 退运重量
     */
    private Long returnWeight;
    /**
     * 退运件数
     */
    private Long returnAmount;
    /**
     * 退运类型（0是非锂电池 1是锂电池）
     */
    private Integer returnType;
    /**
     * 退运时间
     */
    private Date returnDate;
    /**
     * 退运人
     */
    private String returnUser;
    /**
     * 违规详情
     */
    private String remark;
    /**
     * 退运原因
     */
    private String retreatCause;
    /**
     * 退运原因代码
     */
    private String retreatCode;
    /**
     * 运单id
     */
    private Long billId;
	public Long getRetreatId() {
		return retreatId;
	}
	public void setRetreatId(Long retreatId) {
		this.retreatId = retreatId;
	}
	public Long getCollectionWeight() {
		return collectionWeight;
	}
	public void setCollectionWeight(Long collectionWeight) {
		this.collectionWeight = collectionWeight;
	}
	public Long getCollectionAmount() {
		return collectionAmount;
	}
	public void setCollectionAmount(Long collectionAmount) {
		this.collectionAmount = collectionAmount;
	}
	public Long getReturnWeight() {
		return returnWeight;
	}
	public void setReturnWeight(Long returnWeight) {
		this.returnWeight = returnWeight;
	}
	public Long getReturnAmount() {
		return returnAmount;
	}
	public void setReturnAmount(Long returnAmount) {
		this.returnAmount = returnAmount;
	}
	public Integer getReturnType() {
		return returnType;
	}
	public void setReturnType(Integer returnType) {
		this.returnType = returnType;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public String getReturnUser() {
		return returnUser;
	}
	public void setReturnUser(String returnUser) {
		this.returnUser = returnUser;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRetreatCause() {
		return retreatCause;
	}
	public void setRetreatCause(String retreatCause) {
		this.retreatCause = retreatCause;
	}
	public Long getBillId() {
		return billId;
	}
	public void setBillId(Long billId) {
		this.billId = billId;
	}
	
	public String getRetreatCode() {
		return retreatCode;
	}
	public void setRetreatCode(String retreatCode) {
		this.retreatCode = retreatCode;
	}
	public LoadRetreat() {
		super();
	}
	public LoadRetreat(Long retreatId, Long collectionWeight, Long collectionAmount, Long returnWeight,
			Long returnAmount, Integer returnType, Date returnDate, String returnUser, String remark,
			String retreatCause, String retreatCode, Long billId) {
		super();
		this.retreatId = retreatId;
		this.collectionWeight = collectionWeight;
		this.collectionAmount = collectionAmount;
		this.returnWeight = returnWeight;
		this.returnAmount = returnAmount;
		this.returnType = returnType;
		this.returnDate = returnDate;
		this.returnUser = returnUser;
		this.remark = remark;
		this.retreatCause = retreatCause;
		this.retreatCode = retreatCode;
		this.billId = billId;
	}
	@Override
	public String toString() {
		return "TbLoadRetreat [retreatId=" + retreatId + ", collectionWeight=" + collectionWeight
				+ ", collectionAmount=" + collectionAmount + ", returnWeight=" + returnWeight + ", returnAmount="
				+ returnAmount + ", returnType=" + returnType + ", returnDate=" + returnDate + ", returnUser="
				+ returnUser + ", remark=" + remark + ", retreatCause=" + retreatCause + ", retreatCode=" + retreatCode
				+ ", billId=" + billId + "]";
	}
	
}
