package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;

import cn.afterturn.easypoi.excel.annotation.Excel;

public class LoadWeight implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long weightId;//轻泡超重id
	
	@Excel(name = "前缀", orderNum = "0")
	private String prefix;//前缀
	
	@Excel(name = "单号", orderNum = "1")
	private String oddNumber;//单号
	
	@Excel(name = "开单件数", orderNum = "2")
	private Long amount;//开单件数
	
	@Excel(name = "开单重量", orderNum = "3")
	private Long billWeight;//开单重量
	
	@Excel(name = "真实重量", orderNum = "4")
	private Long actualWeight;//真实重量
	
	@Excel(name = "操作人", orderNum = "5")
	private String operate;//操作人
	
	@Excel(name = "操作事情", orderNum = "6")
	private String event;//操作事情
	
	@Excel(name = "描述", orderNum = "7")
	private String remark;//描述
	
	private Date modifyDate;//修改时间
	
	private Date createDate;//创建时间
	public Long getWeightId() {
		return weightId;
	}
	public void setWeightId(Long weightId) {
		this.weightId = weightId;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getOddNumber() {
		return oddNumber;
	}
	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Long getBillWeight() {
		return billWeight;
	}
	public void setBillWeight(Long billWeight) {
		this.billWeight = billWeight;
	}
	public Long getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(Long actualWeight) {
		this.actualWeight = actualWeight;
	}
	public String getOperate() {
		return operate;
	}
	public void setOperate(String operate) {
		this.operate = operate;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public LoadWeight(Long weightId, String prefix, String oddNumber, Long amount, Long billWeight, Long actualWeight,
			String operate, String event, String remark, Date modifyDate, Date createDate) {
		super();
		this.weightId = weightId;
		this.prefix = prefix;
		this.oddNumber = oddNumber;
		this.amount = amount;
		this.billWeight = billWeight;
		this.actualWeight = actualWeight;
		this.operate = operate;
		this.event = event;
		this.remark = remark;
		this.modifyDate = modifyDate;
		this.createDate = createDate;
	}
	public LoadWeight() {
		super();
	}
	@Override
	public String toString() {
		return "LoadWeight [weightId=" + weightId + ", prefix=" + prefix + ", oddNumber=" + oddNumber + ", amount="
				+ amount + ", billWeight=" + billWeight + ", actualWeight=" + actualWeight + ", operate=" + operate
				+ ", event=" + event + ", remark=" + remark + ", modifyDate=" + modifyDate + ", createDate="
				+ createDate + "]";
	}
	
	
	
	
	
	
	
}
