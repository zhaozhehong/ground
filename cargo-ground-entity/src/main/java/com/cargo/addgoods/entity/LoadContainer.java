package com.cargo.addgoods.entity;

import java.io.Serializable;
/**
 * 基础容器管理之后投入使用的容器
 */
import java.util.Date;

public class LoadContainer implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 集装器ID
     */
    private Long containerId;
	 /**
     * 是否删除
     */
    private Integer isDeleted;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改用户
     */
    private String updateUser;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建用户
     */
    private String createUser;
    /**
     * 备注
     */
    private String remark;
    /**
     * 操作人
     */
    private String operator;
    /**
     * 目的地
     */
    private String end;
    /**
     * 录入时间
     */
    private Date enterTime;
    /**
     * 存放区域
     */
    private String depositRegion;
    /**
     * 状态（0 空板箱 1 已加货 2 上舱单 3 拉货  4 出港 5 异常 ）
     */
    private Integer status;
    /**
     * 容器目的站
     */
    private String containerEnd;
    /**
     * 容器始发站
     */
    private String containerStart;
    /**
     * 出港航班
     */
    private String outFlight;
    /**
     * 停场时长
     */
    private String stopDuration;
    /**
     * 进港航班日期
     */
    private Date joinFlightDate;
    /**
     * 进港航班
     */
    private String joinFlight;
    /**
     * 容器号
     */
    private String containerNumber;
    /**
     * 航班动态id
     */
    private Long flightId;
    
    private String personal;//所属人
    
    private String goodsCode;//加货备注编码
    
    private String goodsName;//加货备注名称
    
    private String identify;//释放标识
    
    private String containerType;//容器类型

    private Integer released;//是否释放(0为正常，1为释放)
    
    private String container;//容器类型和容器拼接
    
    //释放时间
    private Date releaseDate;


	public Long getContainerId() {
		return containerId;
	}


	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}


	public Integer getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Date getUpdateTime() {
		return updateTime;
	}


	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getOperator() {
		return operator;
	}


	public void setOperator(String operator) {
		this.operator = operator;
	}


	public String getEnd() {
		return end;
	}


	public void setEnd(String end) {
		this.end = end;
	}


	public Date getEnterTime() {
		return enterTime;
	}


	public void setEnterTime(Date enterTime) {
		this.enterTime = enterTime;
	}


	public String getDepositRegion() {
		return depositRegion;
	}


	public void setDepositRegion(String depositRegion) {
		this.depositRegion = depositRegion;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getContainerEnd() {
		return containerEnd;
	}


	public void setContainerEnd(String containerEnd) {
		this.containerEnd = containerEnd;
	}


	public String getContainerStart() {
		return containerStart;
	}


	public void setContainerStart(String containerStart) {
		this.containerStart = containerStart;
	}


	public String getOutFlight() {
		return outFlight;
	}


	public void setOutFlight(String outFlight) {
		this.outFlight = outFlight;
	}


	public String getStopDuration() {
		return stopDuration;
	}


	public void setStopDuration(String stopDuration) {
		this.stopDuration = stopDuration;
	}


	public Date getJoinFlightDate() {
		return joinFlightDate;
	}


	public void setJoinFlightDate(Date joinFlightDate) {
		this.joinFlightDate = joinFlightDate;
	}


	public String getJoinFlight() {
		return joinFlight;
	}


	public void setJoinFlight(String joinFlight) {
		this.joinFlight = joinFlight;
	}


	public Long getFlightId() {
		return flightId;
	}


	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}


	public String getPersonal() {
		return personal;
	}


	public void setPersonal(String personal) {
		this.personal = personal;
	}


	public String getGoodsCode() {
		return goodsCode;
	}


	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}


	public String getGoodsName() {
		return goodsName;
	}


	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}


	public String getIdentify() {
		return identify;
	}


	public void setIdentify(String identify) {
		this.identify = identify;
	}


	public Integer getReleased() {
		return released;
	}


	public void setReleased(Integer released) {
		this.released = released;
	}


	public String getContainerNumber() {
		return containerNumber;
	}


	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}


	public String getContainerType() {
		return containerType;
	}


	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}


	public String getContainer() {
		return container;
	}


	public void setContainer(String container) {
		this.container = container;
	}

	

	public Date getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}


	public LoadContainer() {
		super();
	}


	

	

	

}