package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;
/***
 * 字典明细表
 * @author Administrator
 *
 */
public class DictInfo implements Serializable {

	private static final long serialVersionUID = 1L;
    /**
     * 字典明细ID主键
     */
    private Long dictInfoId;
	  /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 字典值
     */
    private String dictInfoValue;
    /**
     * 字典名称
     */
    private String dictInfoName;
    /**
     * 字典类型中的值
     */
    private String dictValue;


	public Long getDictInfoId() {
		return dictInfoId;
	}


	public void setDictInfoId(Long dictInfoId) {
		this.dictInfoId = dictInfoId;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public String getDictInfoValue() {
		return dictInfoValue;
	}


	public void setDictInfoValue(String dictInfoValue) {
		this.dictInfoValue = dictInfoValue;
	}


	public String getDictInfoName() {
		return dictInfoName;
	}


	public void setDictInfoName(String dictInfoName) {
		this.dictInfoName = dictInfoName;
	}


	public String getDictValue() {
		return dictValue;
	}


	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}


	public DictInfo() {
		super();
	}


	public DictInfo(Long dictInfoId, Date createTime, String createUser, String dictInfoValue, String dictInfoName,
			String dictValue) {
		super();
		this.dictInfoId = dictInfoId;
		this.createTime = createTime;
		this.createUser = createUser;
		this.dictInfoValue = dictInfoValue;
		this.dictInfoName = dictInfoName;
		this.dictValue = dictValue;
	}


	@Override
	public String toString() {
		return "DictInfo [dictInfoId=" + dictInfoId + ", createTime=" + createTime + ", createUser=" + createUser
				+ ", dictInfoValue=" + dictInfoValue + ", dictInfoName=" + dictInfoName + ", dictValue=" + dictValue
				+ "]";
	}

	

}