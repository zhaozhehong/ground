package com.cargo.addgoods.entity;

import java.io.Serializable;
/**
 * 字典类型表
 */
import java.util.Date;

public class Dicttype implements Serializable {

	private static final long serialVersionUID = 1L;
	 /**
     * 字典类型ID主键
     */
    private Long dictId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 类型值(不可修改)
     */
    private String dictValue;
    /**
     * 类型名称
     */
    private String dictName;
    
    
   

	public Long getDictId() {
		return dictId;
	}




	public void setDictId(Long dictId) {
		this.dictId = dictId;
	}




	public Date getCreateTime() {
		return createTime;
	}




	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getDictValue() {
		return dictValue;
	}




	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}




	public String getDictName() {
		return dictName;
	}




	public void setDictName(String dictName) {
		this.dictName = dictName;
	}




	public Dicttype() {
		super();
	}




	public Dicttype(Long dictId, Date createTime, String createUser, String dictValue, String dictName) {
		super();
		this.dictId = dictId;
		this.createTime = createTime;
		this.createUser = createUser;
		this.dictValue = dictValue;
		this.dictName = dictName;
	}




	@Override
	public String toString() {
		return "Dicttype [dictId=" + dictId + ", createTime=" + createTime + ", createUser=" + createUser
				+ ", dictValue=" + dictValue + ", dictName=" + dictName + "]";
	}
	
	


}