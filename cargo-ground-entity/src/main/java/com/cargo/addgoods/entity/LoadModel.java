package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;

//机型维护表
public class LoadModel implements Serializable{
	private static final long serialVersionUID = 1l;
	
	private Long modelId;//机型id
	private String type;//机型
	private Long load;//最大载重
	private Integer style;//机型类型
	private String remark;//备注
	private Date createTime;//创建时间
	private Date updateTime;//修改时间
	private String createUser;//创建人
	private String updateUser;//修改人
	private Integer isDeleted;//是否删除
	public Long getModelId() {
		return modelId;
	}
	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getLoad() {
		return load;
	}
	public void setLoad(Long load) {
		this.load = load;
	}
	public Integer getStyle() {
		return style;
	}
	public void setStyle(Integer style) {
		this.style = style;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	@Override
	public String toString() {
		return "LoadModel [modelId=" + modelId + ", type=" + type + ", load=" + load + ", style=" + style + ", remark="
				+ remark + ", createTime=" + createTime + ", updateTime=" + updateTime + ", createUser=" + createUser
				+ ", updateUser=" + updateUser + ", isDeleted=" + isDeleted + "]";
	}
	
	
	
}
