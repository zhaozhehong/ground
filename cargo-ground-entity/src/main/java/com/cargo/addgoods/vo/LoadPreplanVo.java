package com.cargo.addgoods.vo;

/**
 * 
 * 
 * @author yuwei
 * @Description: 预配清单的vo
 * 2018年11月17日上午9:55:59
 */
public class LoadPreplanVo {
	
	  /**
     * 单号
     */
    private String oddNumber;
    /**
     * 前缀
     */
    private String prefix;
    
    /**
     * 代理人
     */
    private String agent;
    
    /**
     * 目的站
     */
    private String end;
    
    /**
     * 起始站
     */
    private String startPort;
    
    /**
     * 舱位
     */
    private String cabin;
	   /**
     * 体积
     */
    private Double volume;
    /**
     * 重量
     */
    private Long weight;
    /**
     * 件数
     */
    private Long amount;
    
    /**
     * 预配航班
     */
    private String flightNumber;
    
    /**
     * 货物代码
     */
    private String cargoCode;
    /**
     * 货物
     */
    private String cargoName;
    
    /**
     * 特码
     */
    private String specialCode;
    
    /**
     * 状态(通过这个状态来判断是不是可还原)
     */
    private Integer status;
    
    private String container;//容器类型和容器拼接

	/**
	 * 承运人
	 * @return
	 */
	private String carrier;

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getOddNumber() {
		return oddNumber;
	}

	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getStartPort() {
		return startPort;
	}

	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Long getWeight() {
		return weight;
	}

	public void setWeight(Long weight) {
		this.weight = weight;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getCargoCode() {
		return cargoCode;
	}

	public void setCargoCode(String cargoCode) {
		this.cargoCode = cargoCode;
	}

	public String getCargoName() {
		return cargoName;
	}

	public void setCargoName(String cargoName) {
		this.cargoName = cargoName;
	}

	public String getSpecialCode() {
		return specialCode;
	}

	public void setSpecialCode(String specialCode) {
		this.specialCode = specialCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}
    
    

}
