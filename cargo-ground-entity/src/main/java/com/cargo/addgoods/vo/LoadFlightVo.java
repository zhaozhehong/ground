package com.cargo.addgoods.vo;

import java.util.Date;

/**
 * 航班动态vo
 * @author Administrator
 *
 */

public class LoadFlightVo  {

	
    /**
     * 航班日期(开始)
     */
    private String startDate;
    /**
     * 航班日期(结束)
     */
    private String endDate;
    
    
    private String end;//目的站或者航程的结束
    
	private String startPort;//起始站或者航程的开始
	
	private String flightType;//机型
	
	private String airNumber;//飞机号
	
	 /**
     * 航班号
     */
    private String flightNumber;//从这个参数以上的都是航班查询条件

    
    
	/**
     * 航班性质(国航还是深航)
     */
    private Integer flightProperty;
    /**
     * 机位
     */
    private String seat;
    /**
     * 计飞时间
     */
    private Date planDate;
    
    /**
     * 预飞时间
     */
    private Date prepareDate;
    /**
     * 取消或者正常
     */
    private String flightCancel;
    /**
     * 延误
     */
    private String flightDelay;
    /**
     * 备降
     */
    private String flightPrepare;
    
    /**
     * 预达时间
     */
    private Date prepareArriveDate;
    /**
     * 计达时间
     */
    private Date planArriveDate;
    /**
     * 实达时间
     */
    private Date actualArriveDate;
    /**
     * 航班结载时间
     */
    private Date goodsDate;
    
    /**
     * 机口
     */
    private String machine;
    
    /**
     * 航班日期
     */
    private Date flightDate;
    
    /**
     * 承运人
     */
    private String freighter;
    
	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartPort() {
		return startPort;
	}

	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getAirNumber() {
		return airNumber;
	}

	public void setAirNumber(String airNumber) {
		this.airNumber = airNumber;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public Integer getFlightProperty() {
		return flightProperty;
	}

	public void setFlightProperty(Integer flightProperty) {
		this.flightProperty = flightProperty;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public Date getPlanDate() {
		return planDate;
	}

	public void setPlanDate(Date planDate) {
		this.planDate = planDate;
	}

	public Date getPrepareDate() {
		return prepareDate;
	}

	public void setPrepareDate(Date prepareDate) {
		this.prepareDate = prepareDate;
	}

	public String getFlightCancel() {
		return flightCancel;
	}

	public void setFlightCancel(String flightCancel) {
		this.flightCancel = flightCancel;
	}

	public String getFlightDelay() {
		return flightDelay;
	}

	public void setFlightDelay(String flightDelay) {
		this.flightDelay = flightDelay;
	}

	public String getFlightPrepare() {
		return flightPrepare;
	}

	public void setFlightPrepare(String flightPrepare) {
		this.flightPrepare = flightPrepare;
	}

	public Date getPrepareArriveDate() {
		return prepareArriveDate;
	}

	public void setPrepareArriveDate(Date prepareArriveDate) {
		this.prepareArriveDate = prepareArriveDate;
	}

	public Date getPlanArriveDate() {
		return planArriveDate;
	}

	public void setPlanArriveDate(Date planArriveDate) {
		this.planArriveDate = planArriveDate;
	}

	public Date getActualArriveDate() {
		return actualArriveDate;
	}

	public void setActualArriveDate(Date actualArriveDate) {
		this.actualArriveDate = actualArriveDate;
	}

	public Date getGoodsDate() {
		return goodsDate;
	}

	public void setGoodsDate(Date goodsDate) {
		this.goodsDate = goodsDate;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getFreighter() {
		return freighter;
	}

	public void setFreighter(String freighter) {
		this.freighter = freighter;
	}
    
  
    
    
    
}