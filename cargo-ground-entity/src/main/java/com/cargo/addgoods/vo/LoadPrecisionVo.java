package com.cargo.addgoods.vo;

//精度范围    参数VO
public class LoadPrecisionVo {
	private Long precisionId;			//精度ID
	private String precisionName;		//名称
	private String precisionType;		//类型
	private Long precisionValue;		//值
	private String precisionComment;	//备注
	private String terminalCode;		//航站代码
	private String terminalName;		//航站名称
	private String register;			//登记人
	private String registTime;			//登记时间
	private String createUser;
	private String createTime;
	private String updateUser;
	private String updateTime;

	public Long getPrecisionId() {
		return precisionId;
	}

	public void setPrecisionId(Long precisionId) {
		this.precisionId = precisionId;
	}

	public String getPrecisionName() {
		return precisionName;
	}

	public void setPrecisionName(String precisionName) {
		this.precisionName = precisionName;
	}

	public String getPrecisionType() {
		return precisionType;
	}

	public void setPrecisionType(String precisionType) {
		this.precisionType = precisionType;
	}

	public Long getPrecisionValue() {
		return precisionValue;
	}

	public void setPrecisionValue(Long precisionValue) {
		this.precisionValue = precisionValue;
	}

	public String getPrecisionComment() {
		return precisionComment;
	}

	public void setPrecisionComment(String precisionComment) {
		this.precisionComment = precisionComment;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getTerminalName() {
		return terminalName;
	}

	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getRegistTime() {
		return registTime;
	}

	public void setRegistTime(String registTime) {
		this.registTime = registTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
}
