package com.cargo.addgoods.vo;

/**
 * @Desc 舱位
 * @LastPeson xuxu
 **/
public class WarehouseSpace {

    // 舱位
    public static final String F = "F";
    public static final String C = "C";
    public static final String Y = "Y";
    public static final String N = "N";
    public static final String Z = "Z";
}
