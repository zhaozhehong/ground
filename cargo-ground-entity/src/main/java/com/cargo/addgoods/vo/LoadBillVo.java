package com.cargo.addgoods.vo;
/**
 * 运单表vo
 * @author Administrator
 *
 */

public class LoadBillVo  {

	 /**
     * 航班号
     */
    private String flightNumber;
    /**
     * 航班日期(开始)
     */
    private String startDate;
    /**
     * 航班日期(结束)
     */
    private String endDate;
    
    /**
     * 单号
     */
    private String oddNumber;
    /**
     * 前缀
     */
    private String prefix;
    
    /**
     * 代理人
     */
    private String agent;
    /**
     * 核单人
     */
    private String auditing;
    /**
     * 制单人
     */
    private String documentMaker;
    
    /**
     * 运单类型
     */
    private String billType;
    
    /**
     * 收运状态
     */
    private String status;
    
    /**
     * 核单日期(开始)
     */
    private String auditStartDate;
    /**
     * 核单日期(结束)
     */
    private String auditEndDate;
    /**
     * 航程结束(目的站)
     */
    private String end;
    /**
     * 航程开始(起始站)
     */
    private String startPort;
    
    /**
     * 特码
     */
    private String specialCode;
    
	public String getSpecialCode() {
		return specialCode;
	}
	public void setSpecialCode(String specialCode) {
		this.specialCode = specialCode;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getOddNumber() {
		return oddNumber;
	}
	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getAuditing() {
		return auditing;
	}
	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}
	public String getDocumentMaker() {
		return documentMaker;
	}
	public void setDocumentMaker(String documentMaker) {
		this.documentMaker = documentMaker;
	}
	public String getBillType() {
		return billType;
	}
	public void setBillType(String billType) {
		this.billType = billType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAuditStartDate() {
		return auditStartDate;
	}
	public void setAuditStartDate(String auditStartDate) {
		this.auditStartDate = auditStartDate;
	}
	public String getAuditEndDate() {
		return auditEndDate;
	}
	public void setAuditEndDate(String auditEndDate) {
		this.auditEndDate = auditEndDate;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getStartPort() {
		return startPort;
	}
	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}
    
    
    
    
}