package com.cargo.addgoods.vo;

import java.util.List;
import java.util.Set;

/**
 * @Desc
 * @LastPeson xuxu
 **/
public class LoadPplanVo {

    //预配清单
    private List<LoadPreplanVo> loadPreplanVos;

    //总体积
    private Double volume = 0.00d;

    //总件数
    private Long amount = 0L;

    //总重量
    private Long weight = 0L;

    //舱位
    private Set<String> warehouseSpaces;

    //加货备注
    private List<String> notes;

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public List<LoadPreplanVo> getLoadPreplanVos() {
        return loadPreplanVos;
    }

    public void setLoadPreplanVos(List<LoadPreplanVo> loadPreplanVos) {
        this.loadPreplanVos = loadPreplanVos;
    }

    public Set<String> getWarehouseSpaces() {
        return warehouseSpaces;
    }

    public void setWarehouseSpaces(Set<String> warehouseSpaces) {
        this.warehouseSpaces = warehouseSpaces;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }
}
