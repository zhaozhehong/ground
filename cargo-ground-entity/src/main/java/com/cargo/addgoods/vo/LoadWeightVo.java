package com.cargo.addgoods.vo;
/**
 * 轻泡超重表vo
 * @author Administrator
 *
 */

public class LoadWeightVo {
	private String prefix;//前缀
	private String oddNumber;//单号
	private String operate;//操作人
	private String startDate;//开始时间
	private String endDate;//结束时间
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getOddNumber() {
		return oddNumber;
	}
	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}
	public String getOperate() {
		return operate;
	}
	public void setOperate(String operate) {
		this.operate = operate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	
	
	
}
