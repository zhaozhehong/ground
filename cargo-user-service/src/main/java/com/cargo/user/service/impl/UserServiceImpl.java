package com.cargo.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.user.entity.User;
import com.cargo.user.mapper.UserMapper;
import com.cargo.user.service.UserService;

/**
 * @author wsw
 * 2018年11月21日下午2:01:51
 * @Description: 用户服务    数据源  事务
 */
@Service("userService")
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	/**
	 * 查询添加从数据源
	 */
	@Override
	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	public User selectUserByUserName(String userName) {
		return userMapper.selectUserByUserName(userName);
	}

	/**
	 * 保存方法，添加事务 和数据源
	 */
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public int save(User user) {
		 int a = userMapper.save(user);
		 if(a>0) {
			 throw new NullPointerException();
		 }
		 return 1;
	}

}
