package com.cargo.user.service;

import  com.cargo.user.entity.User;

public interface UserService {

	User selectUserByUserName(String userName);
	int save(User user);
}
