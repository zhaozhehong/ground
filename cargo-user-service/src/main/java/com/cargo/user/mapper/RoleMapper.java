package com.cargo.user.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.user.entity.Role;

public interface RoleMapper extends BaseMapper<Role> {
}