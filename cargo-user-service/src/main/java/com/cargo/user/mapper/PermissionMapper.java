package com.cargo.user.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.user.entity.Permission;

public interface PermissionMapper extends BaseMapper<Permission> {
}