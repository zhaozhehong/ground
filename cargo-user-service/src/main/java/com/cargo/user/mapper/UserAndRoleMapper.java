package com.cargo.user.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.user.entity.UserAndRole;

public interface UserAndRoleMapper extends BaseMapper<UserAndRole> {
}