package com.cargo.ground;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest

 
@WebAppConfiguration
public class UserControllerTest {
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    
  

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build(); //初始化MockMvc对象
    }

  //   @Test
     public void getUsers() throws Exception {
         mockMvc.perform(MockMvcRequestBuilders.post("/testredis")
                .accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print());
     }
     @Test
     public void getTopic() throws Exception {
        
         String responseString = mockMvc.perform(MockMvcRequestBuilders.post("/send/topic1")
	                 )
	                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())  
	                .andReturn().getResponse().getContentAsString();
	       
		 
 	    System.out.println("--------返回的json = " + responseString);
// 	    
 	    
    	    responseString = mockMvc.perform(MockMvcRequestBuilders.post("/send/log")
               )
              .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())  
              .andReturn().getResponse().getContentAsString();
     
 	   System.out.println("--------返回的json = " + responseString);
     }
     
     
    
   // @Test
    public void getUsersPage() throws Exception {
//    	String page="1";
//    	String limit="4";
//		 SysLogEntity userSearch=new SysLogEntity();
//		 userSearch.setId(2L);
//		 userSearch.setUserName("张三");
//		 String responseString = mockMvc.perform(MockMvcRequestBuilders.post("/getUsersPage")
//	                .param("page", page).param("limit", limit))
//	                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())  
//	                .andReturn().getResponse().getContentAsString();
//	       
//		 
//    	    System.out.println("--------返回的json = " + responseString);
		 
//		 
//		   responseString = mockMvc.perform(MockMvcRequestBuilders.post("/getUsers")
//	                .param("id", "12"))
//	                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())  
//	                .andReturn().getResponse().getContentAsString();
//		   System.out.println("--------返回的json = " + responseString);
    }
    
//  @Test
//  public void getUser() throws Exception {
//      mockMvc.perform(MockMvcRequestBuilders.post("/getUser").param("id", "12")
//              .accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print());
//  }
   
  
   

}