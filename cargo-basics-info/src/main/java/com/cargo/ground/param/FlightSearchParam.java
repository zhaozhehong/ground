package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 航班号
 */
public class FlightSearchParam extends PageSearchParam {
    private String flightNumber;

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
}
