package com.cargo.ground.param;

public class BatteryInfoSearchParam {
	// 设备型号
	private String devicesType;

	// 开始时间
	private String startTime;

	// 截止时间
	private String endTime;

	/**
	 * @return the devicesType
	 */
	public String getDevicesType() {
		return devicesType;
	}

	/**
	 * @param devicesType the devicesType to set
	 */
	public void setDevicesType(String devicesType) {
		this.devicesType = devicesType;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}
