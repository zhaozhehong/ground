package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 承运人
 * @author: liyiting
 */
public class CarrierSearchParam extends PageSearchParam {
    private String code;
    private String name;
    private Long prefix;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrefix() {
        return prefix;
    }

    public void setPrefix(Long prefix) {
        this.prefix = prefix;
    }

}
