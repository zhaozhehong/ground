package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 电报地址管理
 * @author: liyiting
 */
public class TelegraphAddressSearchParam extends PageSearchParam {

    //电报地址
    private String telegraphAddress;

    //电报地址组
    private String addressGroup;

    //地址
    private String address;

    //目的站三字代码
    private String purposeCode;

    /**
     * 获取电报地址
     *
     * @return TELEGRAPH_ADDRESS - 电报地址
     */
    public String getTelegraphAddress() {
        return telegraphAddress;
    }

    /**
     * 设置电报地址
     *
     * @param telegraphAddress 电报地址
     */
    public void setTelegraphAddress(String telegraphAddress) {
        this.telegraphAddress = telegraphAddress == null ? null : telegraphAddress.trim();
    }

    /**
     * 获取电报地址组
     *
     * @return ADDRESS_GROUP - 电报地址组
     */
    public String getAddressGroup() {
        return addressGroup;
    }

    /**
     * 设置电报地址组
     *
     * @param addressGroup 电报地址组
     */
    public void setAddressGroup(String addressGroup) {
        this.addressGroup = addressGroup == null ? null : addressGroup.trim();
    }

    /**
     * 获取地址
     *
     * @return ADDRESS - 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置地址
     *
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取目的站三字代码
     *
     * @return PURPOSE_CODE - 目的站三字代码
     */
    public String getPurposeCode() {
        return purposeCode;
    }

    /**
     * 设置目的站三字代码
     *
     * @param purposeCode 目的站三字代码
     */
    public void setPurposeCode(String purposeCode) {
        this.purposeCode = purposeCode == null ? null : purposeCode.trim();
    }
}