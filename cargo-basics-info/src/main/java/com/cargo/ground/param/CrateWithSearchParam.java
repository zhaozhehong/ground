package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 板箱搭配
 * @author: liyiting
 */
public class CrateWithSearchParam extends PageSearchParam {
    private String modelName;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
