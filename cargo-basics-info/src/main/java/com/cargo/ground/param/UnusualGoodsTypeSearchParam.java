package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 不正常货物类型
 * @author: liyiting
 */
public class UnusualGoodsTypeSearchParam extends PageSearchParam {
    //特殊代码
    private String unusualCode;

    public String getUnusualCode() {
        return unusualCode;
    }

    public void setUnusualCode(String unusualCode) {
        this.unusualCode = unusualCode;
    }
}
