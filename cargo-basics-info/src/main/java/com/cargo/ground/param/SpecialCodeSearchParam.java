package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 特码管理
 * @author: liyiting
 */
public class SpecialCodeSearchParam extends PageSearchParam {
    //特殊代码
    private String code;

    //特码名称
    private String meaning;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
}
