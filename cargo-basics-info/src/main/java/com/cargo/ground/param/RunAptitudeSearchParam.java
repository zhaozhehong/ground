package com.cargo.ground.param;

public class RunAptitudeSearchParam {

	   //限制条件
    private String limitCondition;
    //承运人
    private String carrier;
    //航班号
    private String airNum;
    //飞机号
    private String aircraftNum;
    //机型
    private String aircraftType;
    //航线
	private String startStation;
	// 航线
	private String endStation;

	/**
	 * @return the startStation
	 */
	public String getStartStation() {
		return startStation;
	}
	/**
	 * @param startStation the startStation to set
	 */
	public void setStartStation(String startStation) {
		this.startStation = startStation;
	}
	/**
	 * @return the endStation
	 */
	public String getEndStation() {
		return endStation;
	}
	/**
	 * @param endStation the endStation to set
	 */
	public void setEndStation(String endStation) {
		this.endStation = endStation;
	}
	/**
	 * @return the limitCondition
	 */
	public String getLimitCondition() {
		return limitCondition;
	}
	/**
	 * @param limitCondition the limitCondition to set
	 */
	public void setLimitCondition(String limitCondition) {
		this.limitCondition = limitCondition;
	}
	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}
	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	/**
	 * @return the airNum
	 */
	public String getAirNum() {
		return airNum;
	}
	/**
	 * @param airNum the airNum to set
	 */
	public void setAirNum(String airNum) {
		this.airNum = airNum;
	}
	/**
	 * @return the aircraftNum
	 */
	public String getAircraftNum() {
		return aircraftNum;
	}
	/**
	 * @param aircraftNum the aircraftNum to set
	 */
	public void setAircraftNum(String aircraftNum) {
		this.aircraftNum = aircraftNum;
	}
	/**
	 * @return the aircraftType
	 */
	public String getAircraftType() {
		return aircraftType;
	}
	/**
	 * @param aircraftType the aircraftType to set
	 */
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}


}
