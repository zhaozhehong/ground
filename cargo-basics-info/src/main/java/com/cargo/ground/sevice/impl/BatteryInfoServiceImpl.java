package com.cargo.ground.sevice.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.common.base.impl.BaseServiceImpl;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.ground.entity.BatteryAirLine;
import com.cargo.ground.entity.BatteryInfo;
import com.cargo.ground.mapper.BatteryAirLineMapper;
import com.cargo.ground.mapper.BatteryInfoMapper;
import com.cargo.ground.param.BatteryAirLineSearchParam;
import com.cargo.ground.sevice.BatteryInfoService;

@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 36000, rollbackFor = Exception.class)
@Service
public class BatteryInfoServiceImpl extends BaseServiceImpl<BatteryInfo> implements BatteryInfoService {
	@Resource
	private BatteryInfoMapper batteryInfoMapper;
	@Resource
	private BatteryAirLineMapper batteryAirLineMapper;

	@Override
	public BaseMapper<BatteryInfo> getMapper() {
		return batteryInfoMapper;
	}
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<BatteryAirLine> queryBatteryAirLineListByParam(BatteryAirLineSearchParam searchParam) {
		// TODO Auto-generated method stub
		return batteryAirLineMapper.queryBatteryAirLineListByParam(searchParam);
	}
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void addBatchBatteryAirLine(List<BatteryAirLine> list) {
		for (BatteryAirLine batteryAirLine : list) {
			batteryAirLineMapper.save(batteryAirLine);
		}
	}
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public int delBatteryAirLineById(Long id) {
		return batteryAirLineMapper.delById(id);
	}
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<BatteryInfo> queryWithPageByParam(Map<String, Object> parmMap) {
		return batteryInfoMapper.queryWithPageByParam(parmMap);
	}
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void addBatchBatteryInfo(List<BatteryInfo> list) {
		for (BatteryInfo batteryInfo : list) {
			batteryInfoMapper.save(batteryInfo);
		}
	}
}
