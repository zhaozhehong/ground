package com.cargo.ground.mapper;

import java.util.List;
import java.util.Map;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.BatteryInfo;

public interface BatteryInfoMapper extends BaseMapper<BatteryInfo> {
	List<BatteryInfo> queryWithPageByParam(Map<String, Object> parmMap);
}