package com.cargo.ground.mapper;

import java.util.List;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.BatteryAirLine;
import com.cargo.ground.param.BatteryAirLineSearchParam;

public interface BatteryAirLineMapper extends BaseMapper<BatteryAirLine> {
	List<BatteryAirLine> queryBatteryAirLineListByParam(BatteryAirLineSearchParam searchParam);
}