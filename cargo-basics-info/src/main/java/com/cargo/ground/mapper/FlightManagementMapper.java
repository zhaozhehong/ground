package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.FlightManagement;

import java.util.List;
import java.util.Map;

public interface FlightManagementMapper extends BaseMapper<FlightManagement> {
    public List pages(Map<String, Object> parmMap);
}