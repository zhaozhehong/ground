package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.UnusualGoodsType;

import java.util.List;
import java.util.Map;

/**
 * 不正常货物类型
 * @author: liyiting
 */
public interface UnusualGoodsTypeMapper extends BaseMapper<UnusualGoodsType> {
    public List<UnusualGoodsType> pages(Map<String, Object> parmMap);
}