package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.AirLine;

public interface AirLineMapper extends BaseMapper<AirLine> {
}