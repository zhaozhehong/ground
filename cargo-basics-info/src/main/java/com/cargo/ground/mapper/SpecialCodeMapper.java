package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.SpecialCode;

import java.util.List;
import java.util.Map;

public interface SpecialCodeMapper extends BaseMapper<SpecialCode> {
    List<SpecialCode> pages(Map<String, Object> parmMap);
}