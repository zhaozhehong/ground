package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.GoodsFlight;

import java.util.List;
import java.util.Map;

/**
 * 报T货航班
 * @author: liyiting
 */
public interface GoodsFlightMapper extends BaseMapper<GoodsFlight> {
    public List<GoodsFlight> pages(Map<String, Object> parmMap);
}