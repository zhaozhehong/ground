package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.Purpose;

public interface PurposeMapper extends BaseMapper<Purpose> {
}