package com.cargo.ground.mapper;


import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.WaybillCode;

import java.util.List;
import java.util.Map;

/**
 * 运单特码
 * @author: liyiting
 */
public interface WaybillCodeMapper extends BaseMapper<WaybillCode> {
    public List<WaybillCode> pages(Map<String, Object> parmMap);
}