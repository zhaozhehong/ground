package com.cargo.ground.mapper;

import java.util.List;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.RunAptitude;
import com.cargo.ground.param.RunAptitudeSearchParam;

public interface RunAptitudeMapper extends BaseMapper<RunAptitude> {
	List<RunAptitude> queryListByParam(RunAptitudeSearchParam searchParam);
}