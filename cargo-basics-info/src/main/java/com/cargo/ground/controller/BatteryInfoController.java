package com.cargo.ground.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cargo.ground.common.page.Page;
import com.cargo.ground.entity.BatteryInfo;
import com.cargo.ground.enums.ExceptionEnum;
import com.cargo.ground.excel.ExcelUtil;
import com.cargo.ground.excel.ExportExcelUtil;
import com.cargo.ground.param.BatteryInfoSearchParam;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.ground.sevice.BatteryInfoService;
import com.cargo.ground.tool.DateUtil;
import com.cargo.ground.tool.JsonUtil;
import com.cargo.ground.util.MapTransUtil;

import jxl.Sheet;
import jxl.Workbook;

/**
 * @ClassName: BatteryController
 * @Description: 锂电池信息管理控制器
 * @author: zengjianwen
 * @date: 2018年11月20日 上午9:55:56
 */
@RestController
@RequestMapping("/batteryInfo")
public class BatteryInfoController {
	private static final Logger logger = LoggerFactory.getLogger(BatteryInfoController.class);
	@Autowired
	private BatteryInfoService batteryInfoService;
	@Autowired
	private Page<BatteryInfo> page;

	/**
	 * 分页查询
	 */
	@RequestMapping(value = "/queryWithPageByParam")
	public Result<BatteryInfo> queryWithPageByParam(BatteryInfoSearchParam searchParam) {
		logger.info("查询入参>>>" + JsonUtil.beanToJson(searchParam));
		// 把参数转为map即可
		Map<String, Object> parmMap = MapTransUtil.param2Map(searchParam, page);
		// 查询所有的合同
		List<BatteryInfo> list = batteryInfoService.queryWithPageByParam(parmMap);
		page.setResults(list);
		return ResultUtil.success(page);
	}
	// 添加
	@RequestMapping(value = "/add")
	public Result<String> add(BatteryInfo batteryInfo) {
		int result = batteryInfoService.save(batteryInfo);
		// 返回增删改的操作结果
		return result > 0 ? ResultUtil.success() : ResultUtil.error("添加失败");
	}
	// 编辑
	@RequestMapping(value = "/update")
	public Result<String> update(BatteryInfo batteryInfo) {
		int result = batteryInfoService.update(batteryInfo);
		// 返回增删改的操作结果
		return result > 0 ? ResultUtil.success() : ResultUtil.error("修改失败");
	}
	// 删除
	@RequestMapping(value = "/delete")
	public Result<String> delete(Long[] ids) {
		int result = batteryInfoService.delArray(ids);
		// 返回增删改的操作结果
		return result > 0 ? ResultUtil.success() : ResultUtil.error("删除失败");
	}
	/**
	 * @Title: importExport
	 * @Description: 导入
	 * @param file
	 * @param request
	 * @return
	 * @return: Result<String>
	 */
	@RequestMapping(value = "/importExport", method = RequestMethod.POST)
	public Result<String> importExport(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		File newFile = null;
		Result<String> checkExcel = ExcelUtil.checkExcel(file);
		if (checkExcel.getCode() == 0) {
			try {
				String temp = request.getSession().getServletContext().getRealPath(File.separator) + "temp"; // 临时目录
				File tempFile = new File(temp);
				if (!tempFile.exists()) {
					tempFile.mkdirs();
				}
				String filePath = temp + File.separator + file.getOriginalFilename();
				newFile = new File(filePath);
				file.transferTo(newFile);
				// 读取excel 返回操作集合
				List<BatteryInfo> list = readExcel(newFile, request);
				if (CollectionUtils.isEmpty(list)) {
					return ResultUtil.error(ExceptionEnum.CODE_800008);
				}
				if (list.size() > 5000) {
					return ResultUtil.error(ExceptionEnum.CODE_800009);
				}
				batteryInfoService.addBatchBatteryInfo(list);
			} catch (Exception e) {
				e.printStackTrace();
				return ResultUtil.error(ExceptionEnum.CODE_900000);
			} finally {
				if (newFile != null)
					newFile.delete();
			}
		}
		return ResultUtil.success();
	}
	// ============
	@RequestMapping("/checkCount")
	public Result<String> checkCount(BatteryInfoSearchParam searchParam) {
		Map<String, Object> parmMap = MapTransUtil.param2Map(searchParam, page);
		List<BatteryInfo> list = batteryInfoService.queryWithPageByParam(parmMap);
		if (CollectionUtils.isEmpty(list)) {
			return ResultUtil.error(ExceptionEnum.CODE_900001);
		}
		if (list.size() > 50000) {
			return ResultUtil.error(ExceptionEnum.CODE_900002);
		}
		return ResultUtil.success();
	}
	/**
	 * @Title: exportToExcel
	 * @Description: 导出
	 * @param searchParam
	 * @param response
	 * @return: void
	 */
	@RequestMapping("/exportToExcel")
	public void exportToExcel(BatteryInfoSearchParam searchParam, HttpServletResponse response) {
		Map<String, Object> parmMap = MapTransUtil.param2Map(searchParam, page);
		List<BatteryInfo> list = batteryInfoService.queryWithPageByParam(parmMap);
		if (null == list || list.size() == 0) {
			return;
		}
		if (list.size() > 50000) {
			return;
		}
		ArrayList<String> realheaders = new ArrayList<String>();
		realheaders.addAll(Arrays.asList("设备和锂电池型号", "锂电池型号", "设备型号", "开始时间", "截止时间", "添加人", "添加时间", "备注"));
		ArrayList<String> propertiesList = new ArrayList<String>();
		propertiesList.addAll(Arrays.asList("devicesBaryType", "batteryType", "devicesType", "startTime", "endTime",
				"creator", "createTime", "remark"));
		String sheetName = DateUtil.format(new Date(), DateUtil.YMD);
		response.reset();
		ExportExcelUtil.exportExcel("锂电池信息.xls", sheetName, realheaders, propertiesList, list, response, true);
	}
	public List<BatteryInfo> readExcel(File file, HttpServletRequest request) throws Exception {
		List<BatteryInfo> list = new ArrayList<BatteryInfo>();
		Workbook workbook = Workbook.getWorkbook(file);
		Sheet sheet[] = workbook.getSheets();
		String lab = null;
		for (int a = 0; a < sheet.length; a++) {
			// 去掉标题，从第2行开始取值
			for (int i = 1; i < sheet[a].getRows(); i++) {
				BatteryInfo record = new BatteryInfo();
				for (int j = 0; j < sheet[a].getColumns(); j++) {
					lab = sheet[a].getCell(j, i).getContents();
					if (StringUtils.isNotBlank(lab)) {
						lab = lab.trim();
					}
					if (j == 0 && StringUtils.isNotBlank(lab)) {
						record.setDevicesBaryType(lab);
					}
					if (j == 1 && StringUtils.isNotBlank(lab)) {
						record.setBatteryType(lab);
					}
					if (j == 2 && StringUtils.isNotBlank(lab)) {
						record.setDevicesType(lab);
					}
					if (j == 3 && StringUtils.isNotBlank(lab)) {
						record.setStartTime(new Date());
					}
					if (j == 4 && StringUtils.isNotBlank(lab)) {
						record.setEndTime(new Date());
					}
					if (j == 6 && StringUtils.isNotBlank(lab)) {
						record.setCreator(lab);
					}
					if (j == 7 && StringUtils.isNotBlank(lab)) {
						record.setCreateTime(new Date());
					}
					if (j == 9 && StringUtils.isNotBlank(lab)) {
						record.setRemark(lab);
					}
				}
				list.add(record);
			}
		}
		return list;
	}
}
