package com.cargo.ground.sevice;


import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseService;
import com.cargo.ground.entity.user.User;
@Service
public interface UserService extends BaseService<User> {
	User getUserByLoginName(String username);
	User getUserById(Integer id);
}