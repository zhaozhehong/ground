package com.cargo.ground.sevice;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseService;
import com.cargo.ground.entity.SysLog;
@Service
public interface SysLogService extends BaseService<SysLog> {
	List<SysLog> selectSysLogList(Map<String, Object> parmMap);
}