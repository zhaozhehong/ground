package com.cargo.ground.sevice;


import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseService;
import com.cargo.ground.entity.log.OperateLogEntity;
@Service
public interface OperateLogService extends BaseService<OperateLogEntity> {
}