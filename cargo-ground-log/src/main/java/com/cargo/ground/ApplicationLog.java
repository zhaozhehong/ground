package com.cargo.ground;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@MapperScan(value = "com.cargo.ground.mapper")
@SpringBootApplication
public class ApplicationLog {
	public static void main(String[] args) {
		// SpringApplication.run(ApplicationLog.class, args);
		new SpringApplicationBuilder(ApplicationLog.class).web(true).run(args);
	}

}