package com.cargo.ground.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.ground.entity.log.OperateLogEntity;
import com.cargo.ground.sevice.OperateLogService;
import com.cargo.ground.util.PageDataResult;
 
@RestController
public class OperateLogController {
	
	 
	
	@Autowired
	private  OperateLogService   operateLogService;
 
	private static final Logger logger = LoggerFactory
			.getLogger(OperateLogController.class);
	
	 
 
	/**
	 * 分页查询系统日志列表
	 * @return ok/fail
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getOperateLogPage", method = RequestMethod.POST)
	@ResponseBody
	public PageDataResult getOperateLogPage(@RequestParam("page") Integer page,
			@RequestParam("limit") Integer limit, OperateLogEntity operateLogSearch){
		 
		try {
			// return operateLogService.getPage(operateLogSearch, page, limit);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("查询异常");
		}
		return null;
	}
	
    @RequestMapping("/getOperateLog")
    public OperateLogEntity getUser(Long id) {
        return operateLogService.selectById(id);
    }
 
    
    
}