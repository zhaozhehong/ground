package com.cargo.ground.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.ground.common.page.Page;
import com.cargo.ground.entity.SysLog;
import com.cargo.ground.entity.user.User;
import com.cargo.ground.enums.ExceptionEnum;
import com.cargo.ground.param.SysLogSearchParam;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.ground.sevice.SysLogService;
import com.cargo.ground.sevice.UserService;
import com.cargo.ground.tool.MD5Util;
import com.cargo.ground.tool.PasswordHash;
import com.cargo.ground.util.MapTransUtil;

/**
 * @ClassName: SysLogController
 * @Description: 这是一个Demo 供大家参照
 * @author: zengjianwen
 * @date: 2018年11月20日 上午10:14:05
 */
@RestController
@RequestMapping("/sysLog")
public class SysLogController {
	private static final Logger logger = LoggerFactory.getLogger(SysLogController.class);
	@Autowired
	private SysLogService sysLogService;
	@Autowired
	private UserService userService;
	@Autowired
	private Page<SysLog> page;

	// 测试 模拟一个登陆的例子 看如何封装返回值
	@RequestMapping(value = "/testlogin")
	public Result<String> testlogin(String username, String password) {
		User user = userService.getUserByLoginName(username);
		if (user == null) {
			return ResultUtil.error(ExceptionEnum.USER_NOT_FIND);
		}
		// 拼装验证密码需要的hash（迭代次数:盐:密码）
		boolean flag = false;
		try {
			flag = PasswordHash.validatePassword(MD5Util.MD5Encode(password), "");
		} catch (Exception e1) {
			e1.printStackTrace();
			return ResultUtil.error("加密算法错误，请检查日志");
		}
		if (flag) {
			// 登录成功把token返回客户端
			Map<String, Object> authMap = new HashMap<String, Object>();
			authMap.put("authToken", "");
			return ResultUtil.success(authMap);
		} else {
			return ResultUtil.error(ExceptionEnum.USER_INFO_ERROR);
		}
	}
	// 测试 看如何封装增删改操作的返回值
	@RequestMapping(value = "/testsave")
	public Result<String> testsave() {
		// 模拟封装参数
		SysLog sysLog = new SysLog();
		sysLog.setServerId("ID:kjxyf-52298-1540973155887-1:1:1:1:16");
		sysLog.setCreatetime(new Date());
		sysLog.setMessages("aaaa");
		int result = sysLogService.save(sysLog);
		Long fdId = sysLog.getFdId();
		System.out.println("插入返回主键>>>>>" + fdId);
		// 返回增删改的操作结果
		return result > 0 ? ResultUtil.success() : ResultUtil.error("添加失败");
	}
	/**
	 * @Title: getUserById
	 * @Description: 测试异常处理
	 * @param id
	 * @return
	 * @return: User
	 */
	@RequestMapping(value = "getUserById/{id}")
	public Result<User> getUserById(@PathVariable("id") Integer id) {
		return ResultUtil.success(userService.getUserById(id));
	}

	/**
	 * 分页查询
	 */
	@RequestMapping(value = "/queryWithPageByParam")
	public Result<SysLog> queryWithPageByParam(SysLogSearchParam searchParam) {
		// 模拟前端传递过来的参数
		searchParam.setLogname("2018-11-07 16:43:41");
		searchParam.setServerId("ID:kjxyf-65406-1541580219780-1:1:1:1:1");
		searchParam.setPageNo(2 + "");
		// 把参数转为map即可
		Map<String, Object> parmMap = MapTransUtil.param2Map(searchParam, page);
		// 查询所有的合同
		List<SysLog> userList = sysLogService.selectSysLogList(parmMap);
		page.setResults(userList);
		return ResultUtil.success(page);
	}
	/**
	 * 不带参数的分页查询
	 */
	@RequestMapping(value = "/queryWithPage")
	public Result<SysLog> queryWithPage(SysLogSearchParam searchParam) {
		// 封装参数
		Map<String, Object> parmMap = new HashMap<String, Object>();
		parmMap.put("page", page);
		// 查询所有的
		List<SysLog> userList = sysLogService.selectSysLogList(parmMap);
		page.setResults(userList);
		return ResultUtil.success(page);
	}
	@RequestMapping(value = "/selectById")
	public Result<SysLog> selectById() {
		SysLog sysLog = sysLogService.selectById(1541581647396L);
		return ResultUtil.success(sysLog);
	}
	@RequestMapping(value = "/save")
	public int save() {
		SysLog sysLog = new SysLog();
		sysLog.setServerId("ID:kjxyf-52298-1540973155887-1:1:1:1:16");
		sysLog.setCreatetime(new Date());
		sysLog.setLogDateTime(new Date());
		sysLog.setLogname("aaaa");
		sysLog.setMessages("aaaa");
		sysLog.setLogLevel("aaaa");
		int save = sysLogService.save(sysLog);
		Long selectCount = sysLogService.selectCount(sysLog);
		System.out.println("selectCount>>>>>" + selectCount);
		Long fdId = sysLog.getFdId();
		System.out.println("插入返回fdId>>>>>" + fdId);
		return save;
	}
}
