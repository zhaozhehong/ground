package com.cargo.ground.mapper;

import java.util.List;
import java.util.Map;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.SysLog;

public interface SysLogMapper extends BaseMapper<SysLog> {
	int insertSelective(SysLog record);
	List<SysLog> getUserList(Map<String, Object> params);
}