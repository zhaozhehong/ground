package com.cargo.ground.service.user;

/**地面系统项目
 * @Description 
 * @author kj_xiaoyifei
 * @date 2018年11月7日
**/
import com.cargo.ground.common.base.BaseService;
import com.cargo.ground.entity.user.User;
public interface BaseUserService   extends BaseService<User>  {

    /**
     * @Author kj_wangbin
     * @Description 登录查询用户信息
     * @Date 14:16 2018/8/28
     * @Param [userName]
     * @return User
     **/
    User getUserByLoginName(String userName);

    /**
     * @Author kj_wangbin
     * @Description 根据用户名查询用户数据
     * @Date 11:43 2018/8/14
     * @Param [userName]
     * @return com.kj.base.entity.model.User
     **/
    User getUserByName(String userName);

    /**
     * 根据用户id查询用户信息
     * @param id
     * @return
     */
    com.cargo.ground.entity.user.User getUserById(String id);

    /**
     * @Author KevinKai
     * 获取用户信息
     */
	// Pager<User> findUserPage(Pager<User> page, User user);

    /**
     * 根据用户名查询是否含有重名
     * @param userName
     * @return
     */
    boolean getIsUserByName(String userName);

    /**
     * 添加
     *
     * @param user
     * @return
     */
    void saveUser(User user);

    /**
     * 更新
     *
     * @param user
     * @return
     */
    void updateUser(User user);

    /**
     * 删除user
     *
     * @param id
     */
    void deleteUserById(String id);

    /**
     * 锁定
     * @param id
     * @param bool
     */
    void blockingUser(String id ,Boolean bool);

    /**
     * 用户解除锁定
     * @param user
     */
    void unlockUser(User user);
}

