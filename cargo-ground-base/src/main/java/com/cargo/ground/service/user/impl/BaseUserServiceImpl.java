package com.cargo.ground.service.user.impl;

import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.common.base.impl.BaseServiceImpl;
import com.cargo.ground.entity.user.User;
import com.cargo.ground.service.user.BaseUserService;

/**
 * @Package: [com.kj.base.service.shiro.impl.ShiroUserServiceImpl]
 * @ClassName: [ShiroUserServiceImpl]
 * @Description: [用户权限校验相关服务类]
 * @Author: [kj_wangbin]
 * @CreateDate: [2018/8/14 11:32]
 * @Version: [v1.0]
 */
@Service("baseUserService")
public class BaseUserServiceImpl extends BaseServiceImpl<User> implements BaseUserService  {

	@Override
	public User getUserByLoginName(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserByName(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getIsUserByName(String userName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deleteUserById(String id) {
		// TODO Auto-generated method stub
	}

	@Override
	public void blockingUser(String id, Boolean bool) {
		// TODO Auto-generated method stub
	}

	@Override
	public void unlockUser(User user) {
		// TODO Auto-generated method stub
	}

	@Override
	public BaseMapper<User> getMapper() {
		// TODO Auto-generated method stub
		return null;
	}
	// @Resource
	// private BaseUserMapper baseUserMapper;
	//
	// @Override
	// public BaseMapper<User> getMapper() {
	// return baseUserMapper;
	// }
	//
	// /**
	// * @Author kj_wangbin
	// * @Description 根据用户名查询用户数据
	// * @Date 11:43 2018/8/14
	// * @Param [userName]
	// * @Return com.kj.base.entity.model.User
	// **/
	// public User getUserByLoginName(String userName) {
	// //根据用户名及enableFlag = true查询用户信息
	// List<User> userList = baseUserMapper.selectUserInfoByUserNameAndEnableFlag(userName, "1");
	// User user = null;
	// if (userList != null && userList.size() > 0) {
	// user = userList.get(0);
	// }
	// return user;
	// }
	//
	// /**
	// * @Author kj_wangbin
	// * @Description 根据用户名查询用户数据
	// * @Date 11:43 2018/8/14
	// * @Param [userName]
	// * @Return com.kj.base.entity.model.User
	// **/
	// @Override
	// public User getUserByName(String userName) {
	// User user = new User();
	// user.setLoginName(userName);
	// user.setEnableFlag("1");
	// //根据用户名及enableFlag = true查询用户信息
	// List<User> userList = baseUserMapper.selectUserInfoByUserName(user);
	// if (userList != null && userList.size() > 0) {
	// user = userList.get(0);
	// }
	// return user;
	// }
	//
	// /**
	// * 根据用户id查找用户信息
	// * @param id
	// * @Return
	// */
	// @Override
	// public User getUserById(String id) {
	// return baseUserMapper.selectById(id);
	// }
	//
	// ;
	//
	// /**
	// * @param page
	// * @param user
	// * @Return
	// * @Author KevinKai
	// */
	// // @Override
	// // @Transactional(rollbackFor = Exception.class)
	// // public Page<User> findUserPage(Page page, User user) {
	// // // 设置分页参数
	// // user.setPage(page);
	// // // 执行分页查询
	// // page.setList(baseUserMapper.selectUserInfoByUserName(user));
	// // //查询总条数
	// // page.setCount(baseUserMapper.countUserInfo(user));
	// // return page;
	// // }
	//
	// /**
	// * 根据用户名查询是否含有重名
	// * @param userName
	// * @Return
	// */
	// @Override
	// public boolean getIsUserByName(String userName) {
	// User user = new User();
	// user.setLoginName(userName);
	// return baseUserMapper.selectUserInfoByUserName(user).size() > 0 ? false : true;
	// }
	//
	// /**
	// * 添加
	// * @param user
	// * @Return
	// */
	// @Override
	// @Transactional(rollbackFor = Exception.class)
	// public void saveUser(User user) {
	// baseUserMapper.insertUserRole(user);
	// baseUserMapper.saveUser(user);
	// }
	//
	// /**
	// * 修改更新
	// * @param user
	// * @Return
	// */
	// @Override
	// @Transactional(rollbackFor = Exception.class)
	// public void updateUser(User user) {
	// baseUserMapper.clearUserRole(user.getId());
	// baseUserMapper.saveUser(user);
	// baseUserMapper.updateUser(user);
	// }
	//
	// /**
	// * 删除user（状态删除）
	// * @param id
	// */
	// @Override
	// @Transactional(readOnly = false)
	// public void deleteUserById(String id) {
	// baseUserMapper.delById(id);
	//
	// }
	//
	// /**
	// * 用户锁定
	// * @param id
	// * @param bool
	// */
	// @Override
	// public void blockingUser(String id, Boolean bool) {
	// String isLock = bool ? "1" : "0";
	// baseUserMapper.blockingUser(id, isLock);
	// }
	//
	// /**
	// * 解锁帐号
	// * @param user
	// */
	// @Override
	// public void unlockUser(User user) {
	// user.setRetryTimes("0");
	// user.setRetryStartTimes(null);
	// user.setRetryEndTimes(null);
	// user.setIsLocked("0");
	// baseUserMapper.updateUser(user);
	// }

}
