package com.cargo.ground.response;

/**
 * @ClassName: Result
 * @Description: 封装返回值结果
 * @author: zengjianwen
 * @date: 2018年11月16日 上午11:25:52
 * @param <T>
 */
  public class Result<T> {

	// code 状态值：0 极为成功，其他数值代表失败
	private Integer code;// 状态码
	// msg 提示信息，若code为0时，为success
      private String msg;//信息
	// data 返回值，使用泛型兼容不同的类型
      private Object data;//数据

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}
	public Result(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	public Result(Integer code, String msg, Object data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
}
