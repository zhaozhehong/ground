package com.cargo.ground.datasource;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author wsw 2018年11月20日下午2:18:47
 * @Description:注解解析
 */
@Aspect
@Order(1)
@Component
public class DataSourceAspect {

	@Around("@annotation(dataSourceTypeAnno)")
	public Object doAround(ProceedingJoinPoint pjp, DataSourceTypeAnno dataSourceTypeAnno) {
		DbType sourceEnum = dataSourceTypeAnno.value();
		switch (sourceEnum) {
			case oracleCargoGroundMaster:
				DatabaseContextHolder.setDbType(DbType.oracleCargoGroundMaster);
				break;
			case oracleCargoGroundSlave:
				DatabaseContextHolder.setDbType(DbType.oracleCargoGroundSlave);
				break;
			default:
				DatabaseContextHolder.setDbType(DbType.oracleCargoGroundSlave);
				break;
		}
		Object result = null;
		try {
			result = pjp.proceed();
		} catch (Throwable throwable) {
			throwable.printStackTrace();
		} finally {
			DatabaseContextHolder.clearDbType();
		}
		return result;
	}

}
