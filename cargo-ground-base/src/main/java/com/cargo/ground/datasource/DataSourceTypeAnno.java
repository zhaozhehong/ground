package com.cargo.ground.datasource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wsw
 * 2018年11月20日下午2:20:40
 * @Description:  自定义注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSourceTypeAnno {
	DbType value() default DbType.oracleCargoGroundSlave;
}


