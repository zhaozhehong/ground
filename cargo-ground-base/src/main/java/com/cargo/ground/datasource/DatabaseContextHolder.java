package com.cargo.ground.datasource;
/**
 * @author wsw
 * 2018年11月21日上午11:16:32
 * @Description: 数据库处理
 */
public class DatabaseContextHolder {

    private static final ThreadLocal<DbType> contextHolder = new ThreadLocal<>();

    public static void setDbType(DbType dbType) {
        if (dbType == null) {
            throw new NullPointerException();
        }
        contextHolder.set(dbType);
    }

    public static DbType getDbType() {
        return contextHolder.get();
    }

    public static void clearDbType() {
        contextHolder.remove();
    }


}
