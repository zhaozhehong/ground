package com.cargo.ground.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.cargo.ground.common.page.Page;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * entity基础类
 * @author Kevinkai
 * @version 2018-08-20
 */
public class BaseEntitys<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 删除标记（0：正常；1：删除；2：审核；）
     */
    public static final String DEL_FLAG_NORMAL = "0";

    /**
     * 实体编号（唯一标识）
     */
    protected String id;
    /**
     * 备注
     */
    protected String remarks;
    /**
     * 创建者
     */
    protected String createUserId;
    /**
     * 创建日期
     */
    protected Date createDate;
    /**
     * 更新者
     */
    protected String lastUpdateUserId;
    /**
     * 更新日期
     */
    protected Date lastUpdateDate;
    /**
     * 删除标记（0：正常；1：删除；2：审核）
     */
    protected String delFlag;
    /**
     * 当前实体分页对象
     */
	protected Page<T> page;

    public BaseEntitys() {
        this.delFlag = DEL_FLAG_NORMAL;
    }

    public BaseEntitys(String id) {
        this();
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Length(min = 0, max = 255)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId == null ? null : createUserId.trim();
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    public void setLastUpdateUserId(String lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId == null ? null : lastUpdateUserId.trim();
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Length(min = 1, max = 1)
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

	/**
	 * @return the page
	 */
	public Page<T> getPage() {
		return page;
	}
	/**
	 * @param page the page to set
	 */
	public void setPage(Page<T> page) {
		this.page = page;
	}


}
