package com.cargo.ground.entity.user;

import com.cargo.ground.entity.BaseEntitys;
import com.cargo.ground.entity.role.Role;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * 用户实体类
 */
public class User extends BaseEntitys {

    private static final long serialVersionUID = 1L;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 密码
     */
    private String password;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 部门id
     */
    private String departId;
    /**
     * 工号
     */
    private String no;
    /**
     * 用户真实姓名
     */
    private String name;
    /**
     * 性别
     */
    private String gender;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 是否第一次登录 0：第一次  1：非第一次
     */
    private String firstLogin;
    /**
     * 锁定状态 0：正常  1：锁定
     */
    private String isLocked;
    /**
     * 管理员情况 1：管理员
     */
    private String isAdmin;
    /**
     * 重试次数
     */
    private String retryTimes;
    /**
     * 重试次数统计开始时间
     */
    private Date retryStartTimes;
    /**
     * 重试次数统计结束时间
     */
    private Date retryEndTimes;
    /**
     * 权限集合
     */
    private List<String> permissionList = new LinkedList<String>();
    /**
     * 角色集合
     */
    private List<Role> roleList = new LinkedList<Role>();
    /**
     * 是否可用 0：可用  1：不可用
     */
    private String enableFlag;

    public User() {
    }

    public User(String password, String loginName) {
        this.password = password;
        this.loginName = loginName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

    public boolean getIsLocked() {
        return "0".equals(this.isLocked) ? false : true;
    }

    public void setIsLocked(String isLocked) {
        this.isLocked = isLocked;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getRetryTimes() {
        return retryTimes == null ? 0 : (Integer.parseInt(this.retryTimes));
    }

    public void setRetryTimes(String retryTimes) {
        this.retryTimes = retryTimes;
    }

    public Date getRetryStartTimes() {
        return retryStartTimes;
    }

    public void setRetryStartTimes(Date retryStartTimes) {
        this.retryStartTimes = retryStartTimes;
    }

    public Date getRetryEndTimes() {
        return retryEndTimes;
    }

    public void setRetryEndTimes(Date retryEndTimes) {
        this.retryEndTimes = retryEndTimes;
    }

    public List<String> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<String> permissionList) {
        this.permissionList = permissionList;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }
}