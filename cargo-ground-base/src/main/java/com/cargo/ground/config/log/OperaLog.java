package com.cargo.ground.config.log;

/**地面系统项目
 * @Description  自定义注解，拦截Controller
 * @author kj_xiaoyifei
 * @date 2018年10月31日
**/
 
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.PARAMETER })
@Documented
public @interface OperaLog {
    String description() default "";
}