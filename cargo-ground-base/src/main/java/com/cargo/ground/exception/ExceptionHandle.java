package com.cargo.ground.exception;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cargo.ground.enums.ExceptionEnum;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;

/**
 * @ClassName: ExceptionHandle
 * @Description: 异常处理切面
 * @author: zengjianwen
 * @date: 2018年11月16日 下午7:15:06
 */
@ControllerAdvice
public class ExceptionHandle {

    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
	public Result<T> handle(Exception e) {
        if (e instanceof BusinessException) {
            BusinessException girlException = (BusinessException) e;
            return ResultUtil.error(girlException.getCode(), girlException.getMessage());
        }else {
            logger.error("【系统异常】{}", e);
			return ResultUtil.error(ExceptionEnum.UNKNOWN_ERROR);
        }
    }
}
