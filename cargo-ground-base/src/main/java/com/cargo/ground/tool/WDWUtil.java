package com.cargo.ground.tool;

/**
 * @ClassName: WDWUtil
 * @Description: TODO
 * @author: zengjianwen
 * @date: 2018年11月20日 下午2:48:53
 */
public class WDWUtil {
	    public static boolean isExcel2003(String filePath)  {  
	        return filePath.matches("^.+\\.(?i)(xls)$");  
	    }  
	    public static boolean isExcel2007(String filePath)  {  
	        return filePath.matches("^.+\\.(?i)(xlsx)$");  
	    }  
}
