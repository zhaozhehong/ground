package com.cargo.ground.util.wBridge.HandlesStrMsg;

import com.cargo.ground.util.StringUtil;
import com.cargo.ground.util.wBridge.AnalysisStr;

/**
 * @Desc 除皮 数据的特殊解析
 * @LastPeson xuxu
 **/
public class DefaultHandleBackMsg extends HandlebackMsgInf {

    public DefaultHandleBackMsg(String send,String[] bDesc) {
        super.setSend(send);
        super.setBackeDesc(bDesc);
    }

    @Override
    public void handle() {
        //对返回进行特殊处理
        //去皮指令格式
        int i = 0;
        //打印结果
        StringBuffer result = new StringBuffer();
        //解析
        String str = AnalysisStr.analysisRecv(super.getBackeMsg());
        //分割
        String[] args = str.split(",");
        //展示结果
        for(String e : args){
            //补齐 //补备注 //换行
            result.append(e).append(",")
                    .append(StringUtil.StringFill(25-e.length()))
                    .append(super.getBackeDesc()[i++]).append("\n");
        }
        //格式化打印结果
        System.err.println("格式化:\n"+result);
    }

}
