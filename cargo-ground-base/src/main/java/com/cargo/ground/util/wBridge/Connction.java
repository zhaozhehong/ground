package com.cargo.ground.util.wBridge;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @Desc 和地磅建立连接, 后期可以改成 udp连接池
 * @LastPeson xuxu
 **/
public class Connction {

    /**
     * 1: 建立连接(后期可以考虑建立连接池)
     * 2: 将16进制的字符串数据变更成字节数组 发送
     * 3: 接收字节数组转成字符串
     * @param send 发送的指令
     * @return 返回指令数据
     */
    public static String sendAndGetMsg(String send) {
        String backMsg = null;
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();//创建套接字;
            InetAddress address = InetAddress.getByName(Constants.UDP_IP);  //服务器地址
            //删除空格,转成字节数组
            byte[] buf = getBy2(send);
            System.err.println("客户端发送的数据为:" + send);
            DatagramPacket dataGramPacket = new DatagramPacket(buf, buf.length, address, Constants.UDP_PORT);
            socket.send(dataGramPacket);  //通过套接字发送数据
            //接收服务端返回的数据
            byte[] backbuf = new byte[1024];
            DatagramPacket backPacket = new DatagramPacket(backbuf, backbuf.length);
            socket.receive(backPacket);
           // backMsg = new String(backbuf, 0, backPacket.getLength());
            backMsg = bytesToHexFun(backbuf);
            System.err.println("客户端接收到:" + backMsg.length()+":"+backMsg);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("发送/接收 udp 指令失败");
        } finally {
            socket.close();
        }
        return backMsg;
    }

    public static byte[] getBy2(String send){
        //去除空格
        String[] arr = send.split(" ");
        //0x
        byte[] rel = new byte[arr.length];
        for (int i = 0; i < arr.length; i++) {
            rel[i] = Byte.parseByte(arr[i],16);
        }
        return  rel;
    }

    public static String bytesToHexFun(byte[] bytes) {
        StringBuilder buf = new StringBuilder(bytes.length * 2);
        for(byte b : bytes) { // 使用String的format方法进行转换
            buf.append(String.format("%02x", new Integer(b & 0xff)));
        }

        return buf.toString();
    }

}
