package com.cargo.ground.util.wBridge.HandlesStrMsg;

/**
 * @Desc 公共处理地磅返回标准
 * @LastPeson xuxu
 **/
public abstract class HandlebackMsgInf {
      private String send;//发送指令
      private String backeMsg;//返回指令
      private String[] backeDesc;//返回格式化
      private Object back;//返回给应用层的对象


      //核心处理逻辑
      abstract void handle();

      //获得特殊处理的返回
      public Object getBacke() {
            return back;
      }
      public String getBackeMsg() {
            return backeMsg;
      }
      public void setBackeMsg(String backeMsg) {
            this.back = backeMsg;
            this.backeMsg = backeMsg;
      }
      public String getSend() {
            return send;
      }
      public void setSend(String send) {
            this.send = send;
      }
      public String[] getBackeDesc() {
            return backeDesc;
      }
      public void setBackeDesc(String[] backeDesc) {
            this.backeDesc = backeDesc;
      }
}
