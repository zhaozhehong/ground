package com.cargo.ground.util;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

public class StringUtil extends StringUtils {

    public static String toString(Object obj) {
        if (null == obj) {
            return "";
        }
        return (String) obj.toString();
    }
    //判断容器类是否为空
    public static boolean checkCollection(@SuppressWarnings("rawtypes") Collection col) {
		if (col != null && !col.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

    //填充空字符串
    public static String StringFill(int len) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }
}
