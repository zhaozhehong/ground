package com.cargo.ground.util;

import org.apache.log4j.Logger;

/**
 * @Package: [com.cargo.ground.utils.LoggerUtil]
 * @ClassName: [LoggerUtil]
 * @Description: [日志工具类]
 * @Author: [kj_wangbin]
 * @CreateDate: [2018/8/14 10:35]
 * @Version: [v1.0]
 */
public class LoggerUtil {

    /**
     * 得到当前class名称，系统所有使用logger的地方直接调用此方法
     * @return
     */
    public static Logger getLogger() {
        StackTraceElement[] stackEle = new RuntimeException().getStackTrace();
        return Logger.getLogger(stackEle[1].getClassName());
    }
}
