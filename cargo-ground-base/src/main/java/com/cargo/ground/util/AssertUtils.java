package com.cargo.ground.util;

import org.apache.commons.lang3.StringUtils;

import com.cargo.ground.common.exception.BpmException;

public class AssertUtils {
	/**
	 * 是否等于false
	 * @param istrue
	 * @param message
	 */
	public static void isNotTrue(boolean istrue , String message){
		if(!istrue) {
			throw new BpmException(message);
		}
	}
	
	/**
	 * 是否等于true
	 * @param istrue
	 * @param message
	 */
	public static void isTrue(boolean istrue , String message){
		if(istrue) {
			throw new BpmException(message);
		}
	}
	
	
	public static void isBlank(final CharSequence cs ,String message){
		if(StringUtils.isBlank(cs)){
			throw new BpmException(message);
		}
	}
	
	public static void isNotBlank(final CharSequence cs ,String message){
		if(StringUtils.isNotBlank(cs)){
			throw new BpmException(message);
		}
	}
}
