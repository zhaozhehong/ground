package com.cargo.ground.util.wBridge.test;

import com.cargo.ground.util.wBridge.Constants;

import java.io.IOException;
import java.net.*;

/**
 * @Desc udp 服务端 (测试)
 * @LastPeson xuxu
 **/
public class TestSocketServer {

    public static void main(String[] args) {
        server();
    }

    public static void server(){
        try {
            InetAddress address = InetAddress.getLocalHost();
            //创建DatagramSocket对象
            DatagramSocket socket = new DatagramSocket(Constants.UDP_PORT,address);
            byte[] buf = new byte[Constants.size];  //定义byte数组
            DatagramPacket packet = new DatagramPacket(buf, buf.length);  //创建DatagramPacket对象
            System.out.println("服务端启动：" +socket.getLocalAddress()+":"+socket.getLocalPort());
            socket.receive(packet);  //通过套接字接收数据
            String getMsg = new String(buf, 0, packet.getLength());
            System.out.println("服务端接收完成,客户端发送的数据为：" + getMsg);

            //从服务器返回给客户端数据
            InetAddress clientAddress = packet.getAddress(); //获得客户端的IP地址
            int clientPort = packet.getPort(); //获得客户端的端口号
            SocketAddress sendAddress = packet.getSocketAddress();
           //测试,返回 除皮 指令
            String feedback = "02 30 30 31 2c 30 30 2c 31 32 33 34 35 31 32 33 34 35 2c 30 33 2c 30 30 2c 30 30 2c 2b 30 30 30 30 30 30 30 2c 2b 30 30 30 30 30 30 32 2c 2b 30 30 30 30 30 30 30 2c 34 32 37 39 36 2c 34 32 38 34 34 2c 2b 30 30 30 30 30 30 30 2c 31 43 03";
            byte[] backbuf = feedback.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(backbuf, backbuf.length, sendAddress); //封装返回给客户端的数据
            socket.send(sendPacket);  //通过套接字反馈服务器数据
            socket.close();  //关闭套接字
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }

    }


}
