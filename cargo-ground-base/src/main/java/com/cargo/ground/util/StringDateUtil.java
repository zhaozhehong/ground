package com.cargo.ground.util;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Package: [com.cargo.ground.util.StringDateUtil]
 * @ClassName: [StringDateUtil]
 * @Description: [格式化时间工具类]
 * @Author: [kj_wangbin]
 * @CreateDate: [2018/8/27 17:07]
 * @Version: [v1.0]
 */

public class StringDateUtil {
	static Logger logger = LoggerUtil.getLogger();

	/**
	 * yyyyMMdd 20121225
	 */
	public static final String DATE_PATTERN_yyyyMMdd = "yyyyMMdd";

	/**
	 * yyyyMMddHHmmss 20121225202020
	 */
	public static final String DATE_PATTERN_yyyyMMddHHMMss = "yyyyMMddHHmmss";

	/**
	 * 工单号前缀长度 20121225202020
	 */
	public static final int SWO_ORDER_PREFIX_LENGTH = 14;

	/**
	 * 每天服务工单数量级：百万
	 */
	public static final int MAX_SWO_ORDER_SCALE_EVERYDAY = 6;

	/**
	 * 服务工单号长度
	 */
	public static final int SWO_ORDER_NO_LENGTH = SWO_ORDER_PREFIX_LENGTH + MAX_SWO_ORDER_SCALE_EVERYDAY;

	/**
	 * yyyy-MM-dd 2012-12-25
	 */
	public static final String DATE_PATTERN_yyyy_MM_dd = "yyyy-MM-dd";

	/**
	 * HH:mm:ss 20:20:20
	 */
	public static final String DATE_PATTERN_HH_MM_ss = "HH:mm:ss";

	/**
	 * yyyy-MM-dd HH:mm:ss 2012-12-25 20:20:20
	 */
	public static final String DATE_PATTERN_yyyy_MM_dd_HH_MM_ss = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 一天的开始时间点 00:00:00
	 */
	public static final String START_OF_DAY = " 00:00:00";

	/**
	 * :
	 */
	public static final String COLON = ":";

	/**
	 * 00
	 */
	public static final String DEFAULT_SECONDS = "00";
	/**
	 * yyyy
	 */
	public static final String DATE_NEXT_YEAR_yyyy = "yyyy";

	/**
	 * 计算第二天的开始
	 * 
	 * @exception ParseException
	 */
	public static Date startOfNextDay(Date aDay) throws ParseException {
		return startOfDay(anotherDay(aDay, 1));
	}

	/**
	 * 获取相对于date前后的某一天
	 * 
	 * @param i 正数获得其后某一天，负数获得之前某一天
	 * @exception ParseException
	 */
	public static Date anotherDay(Date date, int i) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, i);
		Date result = cal.getTime();
		return result;
	}

	/**
	 * 获取相对于date前后的某一月
	 * 
	 * @param startDate
	 * @param addnos
	 * @return
	 */
	public static Date addMonth(Date startDate, int addnos) {
		Calendar cc = Calendar.getInstance();
		if (startDate != null) {
			cc.setTime(startDate);
			cc.add(Calendar.MONTH, addnos);
			return cc.getTime();
		} else {
			return null;
		}
	}

	/**
	 * 获取相对于date前后的某一时间点
	 * 
	 * @param date
	 * @param i    分钟
	 * @return
	 * @exception ParseException
	 */
	public static Date anotherTime(Date date, int i) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, i);
		Date result = cal.getTime();
		return result;
	}

	/**
	 * 获取相对于date前后的某一天
	 * 
	 * @param i 正数获得其后某一天，负数获得之前某一天
	 * @exception ParseException
	 */
	public static String anotherDateToStr(String sdate, int i) throws ParseException {
		Date date = strToDate(sdate);
		Date newDate = anotherDay(date, i);
		return dateToStr(newDate);
	}

	/**
	 * 去掉时分秒
	 * 
	 * @return
	 */
	public static Date dateToDate(Date date) {
		Date strtodate = null;
		try {
			strtodate = startOfDay(date);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}
		return strtodate;
	}

	/**
	 * 获取指定的format，每次重建避免线程安全问题 yyyy_MM_dd_HH_MM_ss
	 * 
	 * @return
	 */
	public static SimpleDateFormat getDATE_PATTERN_yyyy_MM_dd_HH_MM_ss() {
		return new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd_HH_MM_ss);
	}

	/**
	 * 获取指定的format，每次重建避免线程安全问题
	 * 
	 * @return
	 */
	public static SimpleDateFormat getDEFAULT_FORMATTER() {
		return new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd);
	}

	/**
	 * 获取指定的format，每次重建避免线程安全问题
	 * 
	 * @return
	 */
	public static String getDEFAULT_FORMATTER(Date date) {
		return getDEFAULT_FORMATTER().format(date);
	}

	/**
	 * 将带有时分秒的date置为0点0分0秒，也就是忽略时分秒
	 * 
	 * @param date
	 * @return
	 * @exception ParseException
	 */
	public static Date startOfDay(Date date) throws ParseException {
		return new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd)
				.parse(new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd).format(date));
	}

	/**
	 * 构造格式为HH:mm:ss的Date时间，例如20:20:20
	 */
	public static Date constructTimeByStrHhmmss(String timeStr) {
		try {
			return new SimpleDateFormat(DATE_PATTERN_HH_MM_ss).parse(timeStr);
		} catch (ParseException e) {
			logger.warn("【" + timeStr + "】格式化错误！");
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * 将短时间格式字符串转换为date类型
	 * 
	 * @param strDate
	 * @return
	 */
	public static Date strToDate(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 将yyyyMMdd格式字符串转date类型
	 * 
	 * @param strDate
	 * @return
	 */
	public static Date stryyyyMMddToDate(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		Date strtodate = null;
		try {
			strtodate = formatter.parse(strDate);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}
		return strtodate;
	}

	/**
	 * 将短时间格式字符串转换为时间 yyyy-MM-dd HH:mm:ss
	 * 
	 * @param strDate
	 * @return
	 */
	public static Date strToTime(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd_HH_MM_ss);
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 将短时间格式时间转换为字符串 yyyy-MM-dd
	 * 
	 * @param dateDate
	 * @param
	 * @return
	 */
	public static String dateToStr(Date dateDate) {
		if (dateDate == null) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return formatter.format(dateDate);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * 将短时间格式时间转换为字符串 yyyy-MM-dd HH:mm:ss
	 * 
	 * @param dateDate
	 * @return
	 */
	public static String dateTimeToStr(Date dateDate) {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd_HH_MM_ss);
		return formatter.format(dateDate);
	}

	/**
	 * 将字符串 yyyy-MM-dd HH:mm:ss 格式时间转换为date
	 * 
	 * @param
	 * @return
	 */
	public static Date strTodateTime(String dateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd_HH_MM_ss);
		try {
			return formatter.parse(dateStr);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * 将短时间格式时间转换为字符串 yyyyMMddHHMMss
	 * 
	 * @param dateDate
	 * @return
	 */
	public static String dateTimeToStryyyyMMddHHMMss(Date dateDate) {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN_yyyyMMddHHMMss);
		return formatter.format(dateDate);
	}

	/**
	 * 根据一个日期，返回是星期几的字符串
	 * 
	 * @param sdate
	 * @return
	 */
	public static String getWeek(String sdate) {
		// 再转换为时间
		Date date = strToDate(sdate);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		// int hour=c.get(Calendar.DAY_OF_WEEK);
		// hour中存的就是星期几了，其范围 1~7
		// 1=星期日 7=星期六，其他类推
		return new SimpleDateFormat("EEEE").format(c.getTime());
	}

	public static String getWeekStr(String sdate) {
		String str = "";
		str = getWeek(sdate);
		if ("1".equals(str)) {
			str = "星期日";
		} else if ("2".equals(str)) {
			str = "星期一";
		} else if ("3".equals(str)) {
			str = "星期二";
		} else if ("4".equals(str)) {
			str = "星期三";
		} else if ("5".equals(str)) {
			str = "星期四";
		} else if ("6".equals(str)) {
			str = "星期五";
		} else if ("7".equals(str)) {
			str = "星期六";
		}
		return str;
	}

	/**
	 * 计算两个时间间隔
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static long getDays(String date1, String date2) {
		if (date1 == null || date1.equals(""))
			return 0;
		if (date2 == null || date2.equals(""))
			return 0;
		// 转换为标准时间
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		java.util.Date mydate = null;
		try {
			date = myFormatter.parse(date1);
			mydate = myFormatter.parse(date2);
		} catch (Exception e) {
		}
		long day = (date.getTime() - mydate.getTime()) / (24 * 60 * 60 * 1000);
		return day;
	}

	/**
	 * 日期的差额转换为整数天
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int getDays(Date startDate, Date endDate) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			startDate = sdf.parse(sdf.format(startDate));
			endDate = sdf.parse(sdf.format(endDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(endDate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));

	}

	/**
	 * 得到下一年
	 * 
	 * @param date
	 * @return
	 * @exception ParseException
	 */
	public static Date nextYear(Date date) throws ParseException {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.YEAR, 1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		return new SimpleDateFormat(DATE_NEXT_YEAR_yyyy).parse(sdf.format(c.getTime()));
	}

	/**
	 * 得到本月第一天的日期
	 * 
	 * @return Date
	 * @Methods Name getFirstDayOfMonth
	 */
	public static Date getFirstDayOfMonth(Date date) {
		Calendar cDay = Calendar.getInstance();
		cDay.setTime(date);
		cDay.set(Calendar.DAY_OF_MONTH, 1);
		return cDay.getTime();
	}

	/**
	 * 得到本月最后一天的日期
	 * 
	 * @return Date
	 * @Methods Name getLastDayOfMonth
	 */
	public static Date getLastDayOfMonth(Date date) {
		Calendar cDay = Calendar.getInstance();
		cDay.setTime(date);
		cDay.set(Calendar.DAY_OF_MONTH, cDay.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cDay.getTime();
	}

	/**
	 * 将短时间格式时间转换为字符串 yyyy-MM-dd
	 * 
	 * @param dateDate
	 * @param
	 * @return
	 */
	public static String yearToStr(Date dateDate) {
		if (dateDate == null) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		try {
			return formatter.format(dateDate);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * 将短时间格式字符串转换为date类型
	 * 
	 * @param strDate
	 * @return
	 */
	public static Date strToDateHH(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 去掉秒
	 * 
	 * @param strDate
	 * @return
	 */
	public static String getExcludeSecond(String strDate) {
		Date date;
		try {
			date = getDATE_PATTERN_yyyy_MM_dd_HH_MM_ss().parse(strDate);
		} catch (ParseException e) {
			return "";
		}
		SimpleDateFormat formatterShort = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return formatterShort.format(date);
	}

	public static String getWeekAndDate(String strDate) {
		Date date = strToDateHH(strDate);
		DateFormat df4 = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.LONG); // 显示日期，周，上下午，时间（精确到秒）
		return df4.format(date);
	}

	/**
	 * 时间戳转换成日期格式字符串
	 * 
	 * @param seconds   精确到秒的字符串
	 * @param formatStr
	 * @return
	 */
	public static String timeStamp2Date(String seconds, String format) {
		if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
			return "";
		}
		if (format == null || format.isEmpty()) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(new Date(Long.valueOf(seconds + "000")));
	}

	/**
	 * 日期格式字符串转换成时间戳
	 * 
	 * @param date   字符串日期
	 * @param format 如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String date2TimeStamp(String date_str, String format) {
		if (format == null || format.isEmpty()) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return String.valueOf(sdf.parse(date_str).getTime() / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
      /**
  	 * 时间戳转换成日期格式字符串
  	 * 
  	 * @param seconds   精确到秒的字符串
  	 * @param formatStr
  	 * @return
     * @throws ParseException 
  	 */
  	public static Date timeStampToDate(String seconds, String format) throws ParseException {
  	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  	    return sdf.parse(timeStamp2Date(seconds, format));
  	}


	public static void main(String[] args) {
//		String strDate = "2016-06-14 00:00:00";
//		System.out.println(getWeekAndDate(strDate));
//		Date date = strToDateHH(strDate);
//
//		DateFormat df1 = DateFormat.getDateInstance();// 日期格式，精确到日
//		System.out.println(df1.format(date));
//		DateFormat df2 = DateFormat.getDateTimeInstance();// 可以精确到时分秒
//		System.out.println(df2.format(date));
//		DateFormat df3 = DateFormat.getTimeInstance();// 只显示出时分秒
//		System.out.println(df3.format(date));
//		DateFormat df4 = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT); // 显示日期，周，上下午，时间（精确到秒）
//		System.out.println(df4.format(date) + "d");
//		DateFormat df5 = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG); // 显示日期,上下午，时间（精确到秒）
//		System.out.println(df5.format(date));
//		DateFormat df6 = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT); // 显示日期，上下午,时间（精确到分）
//		System.out.println(df6.format(date));
//		DateFormat df7 = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM); // 显示日期，时间（精确到分）
		
		String  sss=timeStamp2Date("1540869170","yyyy-MM-dd HH:mm:ss"); 
		
		String  fff=date2TimeStamp("2018-10-30 11:22:30","yyyy-MM-dd HH:mm:ss"); 
		System.out.println("按照Java默认的日期格式，默认的区域                      : " + sss+",ffff="+fff);
	}
}
