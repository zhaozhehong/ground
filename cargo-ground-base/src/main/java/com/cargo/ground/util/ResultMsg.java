package com.cargo.ground.util;
//com.shenzhenair.bpm
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

public class ResultMsg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CodeEnum code = CodeEnum.SUCCESS;

	private String msg="操作成功";
	
	private Object data;

	public CodeEnum getCode() {
		return code;
	}

	public void setCode(CodeEnum code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public void error(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		try { 
			e.printStackTrace(pw);
			this.msg = sw.toString()  ;
			sw.close();
		} catch (Exception e2) {
			this.msg = "ErrorInfoFromException "+e2.getMessage();
		} finally { 
			pw.close();
		}

	}

	public enum CodeEnum {
		SUCCESS, // 成功
		FAILURE, // 失败
		ERROR // 错误
	}

}
