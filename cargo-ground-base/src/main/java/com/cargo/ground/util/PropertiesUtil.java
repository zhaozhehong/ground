package com.cargo.ground.util;

import org.springframework.stereotype.Component;

@Component
public class PropertiesUtil {
//	@Value("${tempexport.path}")
	private String tempPath;

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

}
