package com.cargo.ground.util.wBridge;

import com.cargo.ground.util.wBridge.HandlesStrMsg.CommonHandleBackMsg;
import com.cargo.ground.util.wBridge.HandlesStrMsg.DefaultHandleBackMsg;
import com.cargo.ground.util.wBridge.HandlesStrMsg.HandlebackMsgInf;

/**
 * @Desc 地磅 服务
 * @LastPeson xuxu
 **/
public class weighBridgeService {
    /**
     * 除皮
     */
    public Object removeTare() {
        //指定的实现类(除皮实现类)
        HandlebackMsgInf handlebackMsgInf = new DefaultHandleBackMsg(Constants.REMOVE_TARE,Constants.REMOVE_TARE_DESC);
        //用指定的实现类(除皮实现类)去处理 不一样的指令
        CommonHandleBackMsg.handleBySpecial(handlebackMsgInf);
        return handlebackMsgInf.getBackeMsg();
    }


    /***
     * 置零
     */
    public String reset(){
        return CommonHandleBackMsg.defaultCommonHadle(Constants.RESET,Constants.RRESET_DESC);
    }

    /**
     * 取仪表数据
     */
    public String readInfo(){
        return  CommonHandleBackMsg.defaultCommonHadle(Constants.READ_INFO,Constants.READ_INFO_DESC);
    }

    /**
     * 修改单号
     */
    public String updateOrder(){
        return  CommonHandleBackMsg.defaultCommonHadle(Constants.UPDATE_ORDER,Constants.UPDATE_ORDER_DESC);
    }

    /**
     * 修改件数
     */
    public String updateNum(){
        return  CommonHandleBackMsg.defaultCommonHadle(Constants.UPDATE_NUM,Constants.UPDATE_NUM_DESC);
    }

    /**
     * 确认单号
     */
    public String sureNum(){
        return  CommonHandleBackMsg.defaultHadle(Constants.SURE_NUM);
    }

}
