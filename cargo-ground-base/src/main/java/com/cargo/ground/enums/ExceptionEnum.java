package com.cargo.ground.enums;

/**
 * @ClassName: ExceptionEnum
 * @Description: 异常枚举类 定义异常信息
 * @author: zengjianwen
 * @date: 2018年11月16日 上午11:05:21
 */
public enum ExceptionEnum {
	UNKNOWN_ERROR(-1, "未知错误"),
	USER_NOT_FIND(1, "用户不存在"),
	USER_INFO_ERROR(2, "用户名或密码错误"), CODE_800001(800001, "参数校验失败！"), CODE_800003(800003, "调用验签接口失败！"),
	// 导入限制
	CODE_800004(800004, "文件为空！"), CODE_800005(800005, "文件名或文件大小为空"), CODE_800006(800006, "文件大小不能大于5M"),
	CODE_800007(800007, "文件必须是excel2007版本格式"), CODE_800008(800008, "导入数据为空"), CODE_800009(800009, "导入数据不能超过5000条"),
	CODE_900000(900000, "导入数据失败！请联系管理员"),CODE_900003(900003, "导入方法异常"),
	// 导出限制
	CODE_900001(900001, "导出数据为空"), CODE_900002(900002, "导出数据不能超过5000条"),
	
;

    private Integer code;

    private String msg;

    ExceptionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
