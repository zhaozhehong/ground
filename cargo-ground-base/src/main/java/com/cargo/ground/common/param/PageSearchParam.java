package com.cargo.ground.common.param;

/**
 * @ClassName: PageSearchParam
 * @Description: 分页筛选的公共参数
 * @author: zengjianwen
 * @date: 2018年11月8日 上午10:40:48
 */
public class PageSearchParam {  
	//前端传递的页码  
	private String pageNo;
	//前端传递的每页记录数  
	private String pageSize;

	/**
	 * @return the pageNo
	 */
	public String getPageNo() {
		return pageNo;
	}
	/**
	 * @param pageNo the pageNo to set
	 */
	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}
	/**
	 * @return the pageSize
	 */
	public String getPageSize() {
		return pageSize;
	}
	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

}
