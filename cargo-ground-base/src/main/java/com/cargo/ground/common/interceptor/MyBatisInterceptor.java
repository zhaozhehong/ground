package com.cargo.ground.common.interceptor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {
                MappedStatement.class, Object.class})})
public class MyBatisInterceptor implements Interceptor {  
	private static final Logger log = LoggerFactory.getLogger(MyBatisInterceptor.class);
	  private static final String CREATEUSERID = "createUserId";
	    private static final String CREATETIME = "createTime";
	    private static final String UPDATEUSERID = "updateUserId";
	    private static final String UPDATETIME = "updateTime";
	    private static final String ISVALID = "isValid";


	    public Object intercept(Invocation invocation) throws Throwable {
	    	log.debug("自动注入时间拦截器！");
	        if (invocation.getArgs() == null || invocation.getArgs().length <= 1) {
	            return invocation.proceed();
	        }
	        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
	        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
	        if (sqlCommandType == null || SqlCommandType.DELETE == sqlCommandType) {
	            return invocation.proceed();
	        }
	        Object parameter = invocation.getArgs()[1];
	        if (parameter == null) {
	            return invocation.proceed();
	        }
	        Field[] fields = parameter.getClass().getDeclaredFields();
	        if (fields == null || fields.length == 0) {
	            return invocation.proceed();
	        }
	        String userId = "system", userName = "system";
//	        SessionContainer sessionContainer = (SessionContainer) SpringContextUtil.getBean("sessionContainer");
//	        try {
//	            userId = sessionContainer.getCurrentUser().getUsername();
//	            userName = sessionContainer.getCurrentUser().getChinesename();
//	        } catch (Exception e) {
//	            LOGGER.error("get loggin userinfo error");
//	            userId = "admin";
//	            userName = "admin";
//	        }
	        if (SqlCommandType.UPDATE == sqlCommandType) {
	            for (Field field : fields) {
	                initField(false, userId, userName, field, parameter);
	            }
	        } else if (SqlCommandType.INSERT == sqlCommandType) {
	            for (Field field : fields) {
	                initField(true, userId, userName, field, parameter);
	            }
	        }
	        return invocation.proceed();
	    }


	    public Object plugin(Object o) {
	        return Plugin.wrap(o, this);
	    }


	    public void setProperties(Properties properties) {
	    }

	    private void initField(boolean isInsert, String userId, String userName, Field field, Object parameter) {
	        if (UPDATETIME.equals(field.getName()) && null == getFieldValue(field, parameter)) {
	            field.setAccessible(true);
	            try {
	                field.set(parameter, new Date());
	            } catch (IllegalAccessException e) {
	                log.error("set field value error");
	            }
	            field.setAccessible(false);
	        } else if (UPDATEUSERID.equals(field.getName()) && null == getFieldValue(field, parameter)) {
	            field.setAccessible(true);
	            try {
	                field.set(parameter, userId);
	            } catch (IllegalAccessException e) {
	                log.error("set field value error");
	            }
	            field.setAccessible(false);
	        }
	        if (isInsert) {
	            if (CREATETIME.equals(field.getName()) && null == getFieldValue(field, parameter)) {
	                field.setAccessible(true);
	                try {
	                    field.set(parameter, new Date());
	                } catch (IllegalAccessException e) {
	                    log.error("set field value error", e);
	                }
	                field.setAccessible(false);
	            } else if (CREATEUSERID.equals(field.getName()) && null == getFieldValue(field, parameter)) {
	                field.setAccessible(true);
	                try {
	                    field.set(parameter, userId);
	                } catch (IllegalAccessException e) {
	                    log.error("set field value error", e);
	                }
	                field.setAccessible(false);
	            } else if (ISVALID.equals(field.getName()) && null == getFieldValue(field, parameter)) {
	                field.setAccessible(true);
	                try {
	                    field.set(parameter, "Y");
	                } catch (IllegalAccessException e) {
	                    log.error("set field value error");
	                }
	                field.setAccessible(false);
	            }
	        }
	    }

	    private Object getFieldValue(Field field, Object object) {
	        try {
	            if (field.getGenericType().toString().equals(
	                    "class java.util.Date")) {
	                Method m = (Method) object.getClass().getMethod(
	                        "get" + getMethodName(field.getName()));
	                return m.invoke(object);
	            }
	            if (field.getGenericType().toString().equals(
	                    "class java.lang.String")) {
	                Method m = (Method) object.getClass().getMethod(
	                        "get" + getMethodName(field.getName()));
	                return (String) m.invoke(object);
	            }
	        } catch (Exception e) {
	            log.error("get field value error", e);
	        }
	        return null;
	    }

	    private String getMethodName(String fildeName) {
	        byte[] items = fildeName.getBytes();
	        items[0] = (byte) ((char) items[0] - 'a' + 'A');
	        return new String(items);
	    }
   
}  
