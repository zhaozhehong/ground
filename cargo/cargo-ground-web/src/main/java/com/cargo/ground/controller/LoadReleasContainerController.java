package com.cargo.ground.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadReleasContainer;
import com.cargo.addgoods.vo.LoadContainerVo;
import com.cargo.ground.feign.LoadReleasContainerFeignClient;
import com.cargo.ground.response.Result;
/**
 * 
 * 
 * @author yuwei
 * @Description: 容器释放之后还原的相关业务
 * 2018年11月15日下午1:42:42
 */
@RestController
public class LoadReleasContainerController {
	
	@Autowired
	LoadReleasContainerFeignClient loadReleasContainerFeignClient;
	/**
	 * 
	 * @param loadReleasContainer
	 * @author yuwei
	 * @Description: 保存方法
	 * 2018年11月15日下午2:22:05
	 */
	@GetMapping("/saveLoadReleasContainer")
	public  Result<String> saveLoadReleasContainer(LoadReleasContainer loadReleasContainer){
		
		return loadReleasContainerFeignClient.saveLoadReleasContainer(loadReleasContainer);
		
	}
	/**
	 * 
	 * @param loadContainerVo
	 * @return
	 * @author yuwei
	 * @Description: 查询释放容器的数据
	 * 2018年11月17日上午11:42:28
	 */
	@GetMapping("/selectByLoadReleasContainer")
	public  Result<String>  selectByLoadReleasContainer(LoadContainerVo loadContainerVo){
		
			return loadReleasContainerFeignClient.selectByLoadReleasContainer(loadContainerVo);
		
	}
	/**
	 * 
	 * @param container
	 * @param releaseDate
	 * @return
	 * @author yuwei
	 * @Description: 容器还原 （0是成功，1是失败）
	 * 2018年11月17日下午3:35:02
	 */
	@GetMapping("/restoreByContainer")
	public  Result<String>  restoreByContainer(String  container,String releaseDate){
		
		return loadReleasContainerFeignClient.restoreByContainer(container, releaseDate);
	}
}
