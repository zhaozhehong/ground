package com.cargo.ground.feign;

import com.cargo.addgoods.entity.LoadContainer;
import com.cargo.addgoods.vo.LoadPplanVo;
import com.cargo.ground.entity.CheckInBoxVo;
import com.cargo.ground.response.Result;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Desc
 * @LastPeson xuxu
 **/
@FeignClient("addgoods")
public interface LoadContainerFeignClient {

    @RequestMapping("/loadContainer/queryContainerByFlight")
    public Result<List<LoadContainer>> queryContainerByFlight(Long flightNum);

    @RequestMapping("/queryLoadPreplans")
    public Result<LoadPplanVo> queryByContainerNum(String flightNum, String containerNum);

    @RequestMapping("/loadContainer/queryDefaultLoadContainers")
    public Result<List<LoadContainer>> queryDefaultLoadContainers();

    @RequestMapping("/loadContainer/free")
    public Result<Boolean> free(String containerNum);

    @RequestMapping("/loadContainer/saveMark")
    public Result save(Double volume, String spaces,String containerNum , List<String> codes);

    @RequestMapping("/loadContainer/checkInBox")
    public Result<Integer> checkIn(CheckInBoxVo checkInBoxVo);

}
