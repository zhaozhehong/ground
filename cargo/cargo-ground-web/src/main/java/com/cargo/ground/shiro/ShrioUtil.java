package com.cargo.ground.shiro;

import com.cargo.ground.entity.config.Config;
import com.cargo.ground.entity.user.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import java.util.Map;


public class ShrioUtil {

    /**
     * @Description 获取当前登录用户
     * @Date 15:26 2018/8/15
     * @Param []
     * @Return com.kj.base.entity.model.User
     **/
    public static User getCurrentUser() {
        try {
            Subject subject = SecurityUtils.getSubject();
            return (User) subject.getPrincipal();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @Description 保存超级管理员配置
     * @Date 15:26 2018/8/15
     * @Param [configMap]
     * @Return void
     **/
    public static void setCompanyConfig(Map configMap) {
        Config config = new Config(configMap);
        SecurityUtils.getSubject().getSession().setAttribute("conpanyConfig", config);
    }

    /**
     * @Description 获取超级管理员配置
     * @Date 15:26 2018/8/15
     * @Return com.kj.base.common.model.CompanyConfig
     **/
    public static Config getCompanyConfig() {
        return (Config) SecurityUtils.getSubject().getSession().getAttribute("conpanyConfig");
    }
}