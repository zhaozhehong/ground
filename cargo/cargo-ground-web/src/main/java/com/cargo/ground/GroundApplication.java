package com.cargo.ground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

import com.cargo.ground.mybatis.MyBatisConfigs;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {MyBatisConfigs.class}))
@EnableFeignClients(basePackages= {"com.cargo.ground.feign"})
public class GroundApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(GroundApplication.class, args);
	}
}
