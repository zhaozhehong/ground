package com.cargo.ground.shiro.listener;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;

import com.cargo.ground.shiro.RedisSessionDAO;
import com.cargo.ground.util.LoggerUtil;

public class ShiroSessionListener implements SessionListener {
	
	private Logger logger = LoggerUtil.getLogger();
	
	@Resource
	private RedisSessionDAO redisSessionDAO;

	@Override
	public void onStart(Session session) {
		// 会话创建时触发
        logger.info("PlsSessionListener session {} 被创建" + session.getId());
	}

	@Override
	public void onStop(Session session) {
		redisSessionDAO.delete(session);
        // 会话被停止时触发
        logger.info("PlsSessionListener session {} 被销毁" + session.getId());
	}

	@Override
	public void onExpiration(Session session) {
		redisSessionDAO.delete(session);
        //会话过期时触发
        logger.info("PlsSessionListener session {} 过期" + session.getId());
	}
	
}
