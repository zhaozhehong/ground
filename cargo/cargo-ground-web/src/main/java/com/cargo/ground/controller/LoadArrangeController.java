package com.cargo.ground.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.ground.feign.LoadArrangeFeignClient;
import com.cargo.ground.response.Result;

/**
 * 
 * 
 * @author yuwei
 * @Description: 机口排班相关的业务
 * 2018年11月12日上午10:40:35
 */
@RestController
public class LoadArrangeController {
	
	@Autowired
	private LoadArrangeFeignClient loadArrangeFeignClient;
	
	/**
	 * 
	 * @param flightNumber
	 * @param machine
	 * @return
	 * @author yuwei
	 * @Description: 带条件的查询机口排班的数据
	 * 2018年11月12日上午10:55:30
	 */
	@GetMapping("/selectByArrange")
	public  Result<LoadArrange> selectByArrange(String flightNumber,String machine){
		
		return loadArrangeFeignClient.selectByArrange(flightNumber, machine);
		
	}
	
	/**
	 * 
	 * @param loadArrange
	 * @author yuwei
	 * @Description: 保存方法
	 * 2018年11月12日上午10:57:23
	 */
	@GetMapping("/saveArrange")
	public  Result<String> saveArrange( LoadArrange loadArrange){
		return loadArrangeFeignClient.saveArrange(loadArrange);
		
	}
	
	/**
	 * 
	 * @param loadArrange
	 * @author yuwei
	 * @Description: 更新方法
	 * 2018年11月12日上午11:01:04
	 */
	@GetMapping("/updateArrange")
	public  Result<String> updateArrange(LoadArrange loadArrange){
		return loadArrangeFeignClient.updateArrange(loadArrange);
		
	}
	/**
	 * 
	 * @param id
	 * @author yuwei
	 * @Description:逻辑删除 
	 * 2018年11月12日上午11:00:41
	 */
	@GetMapping("/deleteArrange")
	public  Result<String> deleteArrange(Long id){
		return loadArrangeFeignClient.deleteArrange(id);
		
	}
	/**
	 * 
	 * @return
	 * @author yuwei
	 * @Description: 机口排班的导入
	 * 2018年11月20日下午7:19:05
	 */
	@GetMapping("/loadArrangeImport")
	public Result<String> loadArrangeImport() {
		return loadArrangeFeignClient.loadArrangeImport();
	}
}
