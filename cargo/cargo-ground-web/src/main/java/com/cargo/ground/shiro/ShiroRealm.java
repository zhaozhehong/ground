package com.cargo.ground.shiro;

import org.apache.log4j.Logger;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cargo.ground.feign.user.UserFeignService;
import com.cargo.ground.util.LoggerUtil;
import com.cargo.user.entity.User;

@Component
public class ShiroRealm extends AuthorizingRealm {

	private Logger logger = LoggerUtil.getLogger();

	@Autowired
	private UserFeignService  userFeignService;
//	@Resource
//	private BaseUserService  baseUserService;

	/*@Resource
	private BaseResourceService baseResourceService;*/

	/*@Autowired
	private HashedCredentialsMatcher hashedCredentialsMatcher;*/

	/**
	 * 权限认证，为当前登录的Subject授予角色和权限
	 *
	 * @see {}经测试：本例中该方法的调用时机为需授权资源被访问时
	 * @see {}经测试：并且每次访问需授权资源时都会执行该方法中的逻辑，这表明本例中默认并未启用AuthorizationCache
	 * @see {}经测试：如果连续访问同一个URL（比如刷新），该方法不会被重复调用，Shiro有一个时间间隔（也就是cache时间，在ehcache-shiro.xml中配置），超过这个时间间隔再刷新页面，该方法会被执行
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

		logger.info("##################执行Shiro权限认证##################");

		// 表名该用户无身份信息
		if (principals == null) {
			throw new AuthorizationException("Principal对象不能为空");
		}
		// 从realm中获取用户对象信息
		User user = (User) principals.fromRealm(getName()).iterator().next();
		// 该方法定义用户授权相关信息
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		// 将认证成功的用户信息进行授权操作
//		info.addStringPermissions(user.getPermissionList());
		return info;

	}

	/**
	 * 登录认证
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
			throws AuthenticationException {
		// UsernamePasswordToken对象用来存放提交的登录信息
		// 获取用户唯一token值，该值获取shiro管理的用户信息
		UsernamePasswordToken stlLoginToken = (UsernamePasswordToken) authenticationToken;
		// 获取数据库用户相关信息
		User user =  userFeignService.getUserByUserName(stlLoginToken.getUsername());
		if (user == null) {
			throw new UnknownAccountException();
		}
//		else if (user.getIsLocked()) {
//			throw new LockedAccountException();
//		}

		// 用户权限结果集
//		List<String> permissionList = new ArrayList<String>();
//		if (user.getRoleList() != null && user.getRoleList().size() > 0) {
//			// 遍历用户角色信息
//			for (Role role : user.getRoleList()) {
//				if (role.getResourceList() != null && role.getResourceList().size() > 0) {
//					// 获取资源集合，
//					for (Resources resource : role.getResourceList()) {
//						if (StringUtils.isNotBlank(resource.getCode())
//								&& !permissionList.contains(resource.getCode())) {
//							permissionList.add(resource.getCode());
//						}
//					}
//				}
//			}
//		}
		// FIXME 未配置权限时，默认获得所有权限。
//		if (permissionList.isEmpty()) {
//			for (Resources resource : this.baseResourceService.getAllResource()) {
//				if (StringUtils.isNotBlank(resource.getCode())) {
//					permissionList.add(resource.getCode());
//				}
//			}
//		}

		// 更新用户对象信息
//		user.setPermissionList(permissionList);
		
		 //密码:加盐
	    ByteSource bytes = ByteSource.Util.bytes(user.getUserName());
	    String pass = new SimpleHash("MD5", user.getPassWord(), bytes, 0).toString();
	    
		// 进行身份认证
		return new SimpleAuthenticationInfo(user, pass, bytes, getName());
	}

	/*@PostConstruct
	public void initCredentialsMatcher() {
		setCredentialsMatcher(hashedCredentialsMatcher);
	}*/

	@Override
	public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
		super.setCredentialsMatcher(credentialsMatcher);
	}

	@Override
	public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
		super.clearCachedAuthorizationInfo(principals);
	}

	@Override
	public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
		super.clearCachedAuthenticationInfo(principals);
	}

	@Override
	public void clearCache(PrincipalCollection principals) {
		super.clearCache(principals);
	}

	public void clearAllCachedAuthorizationInfo() {
		getAuthorizationCache().clear();
	}

	public void clearAllCachedAuthenticationInfo() {
		getAuthenticationCache().clear();
	}

	public void clearAllCache() {
		clearAllCachedAuthenticationInfo();
		clearAllCachedAuthorizationInfo();
	}

}
