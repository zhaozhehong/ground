package com.cargo.ground.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.vo.BillRetreatVo;
import com.cargo.ground.feign.LoadRetreatFeignClient;
import com.cargo.ground.response.Result;
/**
 * 
 * 
 * @author yuwei
 * @Description: 退运相关的方法
 * 2018年11月12日上午9:46:52
 */
@RestController
public class LoadRetreatController {
	
	@Autowired
	LoadRetreatFeignClient loadRetreatFeignClient;
	
	/**
	 * 
	 * @param billRetreatVo
	 * @return
	 * @author yuwei
	 * @Description: 退运原因关联运单表查询
	 * 2018年11月8日下午2:36:57
	 */
	@GetMapping("/selectByRetreat")
	public   Result<BillRetreatVo> selectByRetreat(BillRetreatVo billRetreatVo){
		
		return loadRetreatFeignClient.selectByRetreat(billRetreatVo);
		
	}
	/**
	 * 
	 * @param billRetreatVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 货物退运的导出
	 * 2018年11月8日下午2:41:19
	 */
	@GetMapping("/loadRetreatExport")
	public Result<String> loadRetreatExport(BillRetreatVo billRetreatVo,HttpServletResponse response, HttpServletRequest request) {
		
	      return loadRetreatFeignClient.loadRetreatExport(billRetreatVo, response, request);
	}
	
	/**
	 * 
	 * @param retreatId
	 * @author yuwei
	 * @Description: 取消退运
	 * 2018年11月20日上午11:32:53
	 */
	@GetMapping("/cancelRetreat")
	public Result<String> cancelRetreat(Long retreatId,Long billId) {
		
		return loadRetreatFeignClient.cancelRetreat(retreatId, billId);
	}
/**
 * 
 * @param billRetreatVo
 * @param response
 * @param request
 * @author yuwei
 * @Description: 退运锂电池日志查询的导出
 * 2018年11月8日下午4:01:41
 */
	@GetMapping("/loadBatteryExport")
	public Result<String> loadBatteryExport(BillRetreatVo billRetreatVo,HttpServletResponse response, HttpServletRequest request) {
		
	      return loadRetreatFeignClient.loadBatteryExport(billRetreatVo, response, request);
	}

}
