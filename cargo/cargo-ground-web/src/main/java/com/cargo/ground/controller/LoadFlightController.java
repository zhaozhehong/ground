package com.cargo.ground.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadFlight;
import com.cargo.addgoods.vo.LoadFlightVo;
import com.cargo.ground.feign.LoadFlightFeignClient;
import com.cargo.ground.response.Result;

/**
 * 
 * 
 * @author yuwei
 * @Description: 航班动态相关的业务
 * 2018年11月12日下午2:00:44
 */
@RestController
public class LoadFlightController {
	@Autowired
	private LoadFlightFeignClient loadFlightFeignClient;
	
	/**
	 * 
	 * @param loadFlightVo
	 * @return
	 * @author yuwei
	 * @Description: 有查询条件的查询所有的航班动态的数据
	 * 2018年11月12日下午2:06:11
	 */
	@GetMapping("/selectByFlight")
	public Result<LoadFlight>   selectByFlight(LoadFlightVo loadFlightVo){
		
		return loadFlightFeignClient.selectByFlight(loadFlightVo);
		
	}
	/**
	 * 
	 * @param loadFlightVo
	 * @return
	 * @author yuwei
	 * @Description: 加货大屏展示
	 * 2018年11月12日下午2:06:41
	 */
	@GetMapping("/findByMachine")
	public   Result<LoadFlightVo>  findByMachine(LoadFlightVo loadFlightVo){
		
		return loadFlightFeignClient.findByMachine(loadFlightVo);
	}
}
