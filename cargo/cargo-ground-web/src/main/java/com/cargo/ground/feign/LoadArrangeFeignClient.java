package com.cargo.ground.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.ground.response.Result;


@FeignClient("addgoods")
public interface LoadArrangeFeignClient {
	/**
	 * 
	 * @param flightNumber
	 * @param machine
	 * @return
	 * @author yuwei
	 * @Description: 带条件的查询机口排班的数据
	 * 2018年11月12日上午10:55:30
	 */
	@GetMapping("/selectByArrange")
	public  Result<LoadArrange> selectByArrange(@RequestParam("flightNumber")String flightNumber,@RequestParam("machine")String machine);
	
	/**
	 * 
	 * @param loadArrange
	 * @author yuwei
	 * @Description: 保存方法
	 * 2018年11月12日上午10:57:23
	 */
	@RequestMapping(value="/saveArrange",method=RequestMethod.POST)
	public  Result<String> saveArrange(@RequestBody LoadArrange loadArrange);
	
	/**
	 * 
	 * @param loadArrange
	 * @author yuwei
	 * @Description: 更新方法
	 * 2018年11月12日上午11:01:04
	 */
	@RequestMapping(value="/updateArrange",method=RequestMethod.POST)
	public  Result<String> updateArrange(@RequestBody LoadArrange loadArrange);
	
	/**
	 * 
	 * @param id
	 * @author yuwei
	 * @Description:逻辑删除 
	 * 2018年11月12日上午11:00:41
	 */
	@GetMapping("/deleteArrange")
	public  Result<String> deleteArrange(@RequestParam("id") Long id);
	
	/**
	 * 
	 * @return
	 * @author yuwei
	 * @Description: 机口排班的导入
	 * 2018年11月20日下午7:19:05
	 */
	@GetMapping("/loadArrangeImport")
	public Result<String> loadArrangeImport();
	
}
