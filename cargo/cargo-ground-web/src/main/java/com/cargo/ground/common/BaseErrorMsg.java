package com.cargo.ground.common;

import java.io.Serializable;

/**
 * @author wsw
 * 2018年11月21日下午6:05:06
 * @Description: 
 */
public class BaseErrorMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 用户登录非浏览器直接传参处理 */
    public final static String NO_USER_NAME_ERROR = "用户名为空，请重新输入!";

    /** 登录用户名有乱码 */
    public final static String USER_NAME_CODE_ERROR = "用户名含有乱码，请重新输入!";

    /** 用户不存在 */
    public final static String NO_USER_ERROR = "用户不存在，请重新输入!";

    /** 帐号已被锁定 */
    public final static String USER_LOCKED_ERROR = "帐号已被锁定，请联系管理员!";

}