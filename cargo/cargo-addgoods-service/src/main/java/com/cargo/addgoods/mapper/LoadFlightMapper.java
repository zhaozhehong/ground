package com.cargo.addgoods.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cargo.addgoods.entity.LoadFlight;
import com.cargo.addgoods.vo.LoadFlightVo;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadFlightMapper extends BaseMapper<LoadFlight> {
	
	/**
	 * 有查询条件的查询所有的航班动态的数据
	 * @param tbLoadWeightVo
	 * @return
	 */
	
	 List<LoadFlight> findListAll(LoadFlightVo tbLoadFlightVo);
	 /**
	  * 
	  * @return
	  * @author yuwei
	  * @Description: 加货大屏展示
	  * 2018年11月6日下午3:46:43
	  */
	 
	 List<LoadFlightVo> findByMachine(@Param("currentStart")String currentStart,@Param("currentEnd")String currentEnd);

}
