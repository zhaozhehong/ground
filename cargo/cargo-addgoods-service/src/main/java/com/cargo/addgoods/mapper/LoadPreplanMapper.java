package com.cargo.addgoods.mapper;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cargo.addgoods.entity.LoadPreplan;
import com.cargo.addgoods.vo.LoadPreplanVo;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadPreplanMapper extends BaseMapper<LoadPreplan> {
	/**
	 * 
	 * @param container
	 * @param releaseDate
	 * @param released
	 * @return
	 * @author yuwei
	 * @Description: 查询预配清单和运单表相关的信息
	 * 2018年11月17日下午4:43:41
	 */
	List<LoadPreplanVo> selectByRelease(@Param("container")String  container,@Param("releaseDate")String  releaseDate,@Param("released")Integer released);
	/**
	 * 
	 * @param container
	 * @param releaseDate
	 * @param released
	 * @return
	 * @author yuwei
	 * @Description: 查询容器释放下面的所有预配清单
	 * 2018年11月17日下午4:44:24
	 */
	List<LoadPreplan> selectByPreplan(@Param("container")String  container,@Param("releaseDate")String  releaseDate,@Param("released")Integer released);
	/**
	 * 
	 * @param preplanId
	 * @param released
	 * @return
	 * @author yuwei
	 * @Description: 通过主键改变释放的状态
	 * 2018年11月17日下午4:45:48
	 */
	int updateByreleased(@Param("preplanId")Long preplanId,@Param("released")Integer released);


	/**
	 *
	 * @return
	 * @author 徐旭
	 * @Description: 更具 容器号 和 状态 变更
	 * 2018年11月17日下午4:45:48
	 */
	int updateByContainor(String containor, Date releaseDate);

}