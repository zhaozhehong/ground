package com.cargo.addgoods.controller;

import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.entity.LoadContainer;
import com.cargo.addgoods.entity.LoadPreplan;
import com.cargo.addgoods.enums.ContainerEnum;
import com.cargo.addgoods.vo.LoadPplanVo;
import com.cargo.addgoods.vo.LoadPreplanVo;
import com.cargo.addgoods.service.LoadBillService;
import com.cargo.addgoods.service.LoadContainerService;
import com.cargo.addgoods.service.LoadPreplanService;

import com.cargo.ground.entity.CheckInBoxVo;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @Desc 货物加载--容器查询
 * @LastPeson xuxu
 *
 *          释放容器,
 *          1: 判断状态
 *          2: 插入数据到容器释放表(集装器数据);
 *          3: 修改集装器表状态和时间(容器状态为空闲,已释放,释放时间);
 *          4: 修改预配清单表的数据状态(已释放,释放时间);
 *          核单进箱
 *          1: 向预配清单表插入数据(容器号,运单,状态为生效)
 *          2: 修改集装器表状态为 "已加货"
 *          3: "加货备注" 是不是特码?
 *          还原
 *           5: 入参 : 容器编号 + 还原时间
 *           6: 判断容器已经为空闲状态
 *           7: 根据容器号,时间查询
 *
 **/
@RestController
@RequestMapping("/loadContainer")
public class LoadContainerController {

    @Autowired
    LoadContainerService containerService;

    @Autowired
    LoadPreplanService loadPreplanService;

    @Autowired
    LoadBillService loadBillService;


    /* 根据航班号查询容器 */
    @RequestMapping("/queryContainerByFlight")
    public Result<List<LoadContainer>>  queryContainerByFlight(Long flightNum) {
        List<LoadContainer> loadContainers = containerService.queryLoadContainersByFlightNum(flightNum);
        // 根据容器
        // 如果容器管理表,没有数据则,
        // 则提示用户去维护
        return ResultUtil.success(loadContainers);
    }

    //根据容器号,查询 预配清单表
    /**
     * 1: 查询预配清单
     * 2: 关联运单表,查询 体积 , 总重量 , 件数，舱位；
     * flightNum 不能为空;
     * return 当前容器下的所有运单的信息以及统计信息;
     * ToDo 备注信息 暂时未处理
     **/
    @RequestMapping("/queryLoadPreplans")
    public Result<LoadPplanVo>  queryByContainerNum(String flightNum, String containerNum) {
        if (flightNum == null || containerNum == null) {
            //航班号不能为空; //容器号不能为空
            return null;
        }
        LoadPplanVo loadPplanVo = new LoadPplanVo();
        List<LoadPreplanVo> preplanVos = new ArrayList<>();
        //如果,查询出 预配清单 没有数据;则,将默认生成一条
        List<LoadPreplan> loadPreplans = loadPreplanService.queryByContainorNum(containerNum);
        //预配清单关联运单
        // 体积,重量,件数累加
        Double volume = 0.00d;
        Long weight = 0L;
        Long amount = 0L;
        // 舱位
        Set<String> warehouseSpaces = new HashSet<>();
        // 加货备注 运单备注 + 容器备注(去重)
        List<String> notes = new ArrayList<>();
        String oddNumber = null;
        if (CollectionUtils.isNotEmpty(loadPreplans)) {
            for (LoadPreplan preplan : loadPreplans) {
                if (preplan != null) {
                    volume = +preplan.getVolume();
                    weight = +preplan.getWeight();
                    amount = +preplan.getAmount();
                    //处理舱位 加货备注
                    oddNumber = preplan.getOddNumber();
                    if (StringUtils.isNotBlank(oddNumber)) {
                        LoadBill loadBill = loadBillService.queryByNum(oddNumber);
                        if (loadBill != null) {
                            warehouseSpaces.add(loadBill.getCabin());
                            warehouseSpaces.add(loadBill.getRemark());
                        }
                    }
                    LoadPreplanVo loadPreplanVo = getLoadPreplanVo(preplan);
                    preplanVos.add(loadPreplanVo);
                }
            }
        }
        //统计运单的件数,重量,体积,舱位
        containerService.getCommentByContainer(containerNum);
        loadPplanVo.setNotes(notes);
        loadPplanVo.setWarehouseSpaces(warehouseSpaces);
        loadPplanVo.setLoadPreplanVos(preplanVos);
        loadPplanVo.setAmount(amount);
        loadPplanVo.setWeight(weight);
        loadPplanVo.setVolume(volume);
        return ResultUtil.success(loadPplanVo);
    }

    /**
     * 货物加载--查询所有容器,对应页面 ... 接口
     * @return 容器列表信息
     */
    @RequestMapping("/queryDefaultLoadContainers")
    public Result<List<LoadContainer>> queryDefaultLoadContainers() {
        return ResultUtil.success(containerService.queryDefLoadContainers());
    }

    /**
     * 货物加载--释放
     * 释放,容器和运单的关系,释放容器和航班的关系;
     * 释放容器,
     *  1: 判断状态
     *  2: 插入数据到容器释放表(集装器数据);
     *  3: 修改集装器表状态和时间(容器状态为空闲,已释放,释放时间)
     *  4: 修改预配清单表的数据状态(已释放,释放时间);
     * */
    @RequestMapping("/free")
    public Result<Boolean> free(String containerNum){
        if (containerNum == null) {
            return ResultUtil.success(false);
        }
        //释放
        int rel = containerService.freeContainer(containerNum,"cureent");
        return ResultUtil.success(rel > 0);
    }

    /**
     * 加货备注
     * containerNum 容器号
     * spaces 逗号隔开,舱位数据
     * codes 加货备注
     * @return
     */
    @RequestMapping("/saveMark")
    public Result save(Double volume, String spaces,String containerNum , List<String> codes){
        /**
         * 1: 保存加货备注,加货备注 与当前的集装器(状态为正常的)关联;
         * 2: 备注(最多15)
         */
        LoadContainer container = containerService.selectByContainer(containerNum, ContainerEnum.UN_RELEAS.getValue());
        // 判断容器是否存在
        if (container == null) {
            return ResultUtil.error("容器号不存在");
        }
        //更新备注,舱位,体积
        containerService.updateMark(volume,spaces,container.getContainerNumber(),codes);
        return ResultUtil.success();
    }

    //核单进箱
    @RequestMapping("/checkInBox")
    public Result<Integer> checkIn(CheckInBoxVo checkInBoxVo){
        //保存 数据到 tb_load_preplan
        loadPreplanService.inputContainer(checkInBoxVo);
        return ResultUtil.success();
    }

    //转换 字段间的对应关系 需要转换
    private LoadPreplanVo getLoadPreplanVo(LoadPreplan preplan){
        LoadPreplanVo vo = new LoadPreplanVo();
        vo.setAmount(preplan.getAmount());
        vo.setWeight(preplan.getWeight());
        vo.setVolume(preplan.getVolume());
        vo.setOddNumber(preplan.getOddNumber());
        vo.setFlightNumber(preplan.getFlightNumber());
        vo.setContainer(preplan.getContainer());
        return vo;
    }

}
