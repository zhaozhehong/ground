package com.cargo.addgoods.mapper;

import com.cargo.addgoods.entity.LoadContainerType;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadContainerTypeMapper extends BaseMapper<LoadContainerType> {

}
