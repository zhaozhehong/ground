package com.cargo.addgoods.mapper;

import com.cargo.addgoods.entity.LoadModel;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadModelMapper extends BaseMapper<LoadModel> {

}
