package com.cargo.addgoods.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cargo.addgoods.entity.LoadPrecision;
import com.cargo.addgoods.mapper.LoadPrecisionMapper;
import com.cargo.addgoods.service.LoadPrecisionService;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
@Service
public class LoadPrecisionServiceImpl implements LoadPrecisionService {
	@Autowired
	private LoadPrecisionMapper tbLoadPrecisionMapper;
	//精度范围添加
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public void insertPrecision(LoadPrecision tbLoadPrecision) {
		
		tbLoadPrecisionMapper.save(tbLoadPrecision);
	}
	
	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadPrecision> selectByName(String precision_name) {
		List<LoadPrecision> list = tbLoadPrecisionMapper.selectByName(precision_name);
		return list;
	}
	
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int update(LoadPrecision tbLoadPrecision) {
		int id = tbLoadPrecisionMapper.update(tbLoadPrecision);
		return id;
	}
	
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public void is_deleted(Long precision_id) {
		tbLoadPrecisionMapper.delById(precision_id);
		
	}

}
