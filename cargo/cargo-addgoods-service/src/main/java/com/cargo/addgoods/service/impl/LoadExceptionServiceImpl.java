package com.cargo.addgoods.service.impl;
import com.cargo.addgoods.entity.LoadException;
import com.cargo.addgoods.mapper.LoadExceptionMapper;
import com.cargo.addgoods.service.LoadExceptionService;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * 
 * @author yuwei
 * @Description: 航班异常和运单异常的service方法
 * 2018年11月19日上午11:29:43
 */
@Service("loadExceptionService")
public class LoadExceptionServiceImpl   implements LoadExceptionService {
	
	@Autowired
	LoadExceptionMapper loadExceptionMapper;

	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int saveLoadException(LoadException loadException) {
		return loadExceptionMapper.save(loadException);
	}
	
	
   
}
