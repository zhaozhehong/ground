package com.cargo.addgoods.service;

import java.util.List;

import com.cargo.addgoods.entity.LoadPrecision;

public interface LoadPrecisionService {
	//精度范围添加
	public void insertPrecision(LoadPrecision tbLoadPrecision);
	
	//精度范围查询
	List<LoadPrecision> selectByName(String precision_name);
	
	//精度范围编辑
	int update(LoadPrecision tbLoadPrecision);
	
	void is_deleted(Long precision_id);
}
