package com.cargo.addgoods.service;

import java.util.List;

import com.cargo.addgoods.entity.LoadArrange;

/**
 * 
 * @author Administrator
 *
 */
 
public interface LoadArrangeService  {
	/**
	 * 带条件的查询机口排班的数据
	 * @param flightNumber
	 * @param machine
	 * @return
	 */
	 List<LoadArrange> findListAll(String flightNumber,String machine );
	 
	 /**
	  * 新增
	  * @param tbLoadArrange
	  * @return
	  */
	 int insert(LoadArrange tbLoadArrange);
	 
	    /**
	      * 更新数据
	     * @param tbLoadArrange
	     * @return
	     */
	     
	    int update(LoadArrange tbLoadArrange);
	    
	    /**
	       * 逻辑删除
	     * @param id
	     * @return
	     */
	    int deleteById(Long id);
  
}
