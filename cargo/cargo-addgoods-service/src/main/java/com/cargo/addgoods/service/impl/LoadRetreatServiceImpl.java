package com.cargo.addgoods.service.impl;
import com.cargo.addgoods.enums.BillEnum;
import com.cargo.addgoods.vo.BillRetreatVo;
import com.cargo.addgoods.vo.RetreatVo;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.addgoods.mapper.LoadBillMapper;
import com.cargo.addgoods.mapper.LoadRetreatMapper;
import com.cargo.addgoods.service.LoadRetreatService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 
 * @author Administrator
 *
 */


@Service("tbLoadRetreatService")
public class LoadRetreatServiceImpl   implements LoadRetreatService {
	@Autowired
	LoadRetreatMapper tbLoadRetreatMapper;
	
	@Autowired
	LoadBillMapper loadBillMapper;

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<BillRetreatVo> selectByRetreat(BillRetreatVo billRetreatVo) {
		return tbLoadRetreatMapper.selectByRetreat(billRetreatVo);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<RetreatVo> selectByRetreatbattery(BillRetreatVo billRetreatVo) {
		return tbLoadRetreatMapper.selectByRetreatbattery(billRetreatVo);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public void delById(Long retreatId,Long billId) {
		//先删除退运表的数据
		 tbLoadRetreatMapper.delById(retreatId);
		 //改变运单表的状态
		 loadBillMapper.updateByStatus(billId, BillEnum.RECEIVE.getValue());
	}

		 
			
   
}
