package com.cargo.addgoods.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.addgoods.mapper.LoadArrangeMapper;
import com.cargo.addgoods.service.LoadArrangeService;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;

/**
 * 
 * @author Administrator
 *
 */


@Service("tbLoadArrangeService")
public class LoadArrangeServiceImpl   implements LoadArrangeService {
	@Autowired
	LoadArrangeMapper tbLoadArrangeMapper;
	
	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadArrange> findListAll(String flightNumber, String machine) {
		return tbLoadArrangeMapper.findListAll(flightNumber, machine);
	}
	
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int insert(LoadArrange tbLoadArrange) {
		return tbLoadArrangeMapper.save(tbLoadArrange);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int update(LoadArrange tbLoadArrange) {
		return tbLoadArrangeMapper.update(tbLoadArrange);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int deleteById(Long id) {
		return tbLoadArrangeMapper.delById(id);
	}	
}
