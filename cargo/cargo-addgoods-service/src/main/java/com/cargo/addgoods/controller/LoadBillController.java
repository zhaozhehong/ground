package com.cargo.addgoods.controller;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.entity.LoadException;
import com.cargo.addgoods.enums.BillEnum;
import com.cargo.addgoods.vo.LoadBillVo;
import com.cargo.ground.response.ResultUtil;
import com.cargo.ground.util.FileUtil;
import com.cargo.addgoods.service.LoadBillService;
import com.cargo.addgoods.service.LoadExceptionService;
import com.cargo.ground.response.Result;

/**
 * 
 * 
 * @author yuwei
 * @Description: 运单相关的方法
 * 2018年11月12日上午9:46:13
 */
@RestController
public class LoadBillController {
	@Autowired
	LoadBillService loadBillService;
	
	@Autowired
	LoadExceptionService loadExceptionService;
	/**
	 * 
	 * @param tbLoadBillVo
	 * @return
	 * @author yuwei
	 * @Description: 带条件查询所有运单表的信息
	 * 2018年11月12日上午10:02:07
	 */
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping("/selectByBill")
	public Result<String>  selectByBill(@RequestBody LoadBillVo tbLoadBillVo){
		Map<String, Object> map = new HashMap<String, Object>();
		List<LoadBill> listBill=loadBillService.findListAll(tbLoadBillVo);
		map.put("loadBill", listBill);
		Map mapBill=loadBillService.totalBill(tbLoadBillVo);
		map.put("total", mapBill.get("TOTAL"));
		map.put("totalAmount", mapBill.get("TOTALAMOUNT"));
		map.put("totalWeight", mapBill.get("TOTALWEIGHT"));
		return ResultUtil.success(map);
		
	}
	/**
	 * 
	 * @param loadBill
	 * @author yuwei
	 * @Description: 保存运单
	 * 2018年11月12日上午10:24:56
	 */
	@RequestMapping("/saveBill")
	public  Result<String> saveBill(@RequestBody LoadBill loadBill){
		
		loadBillService.saveBill(loadBill);
		return ResultUtil.success();
		
	}
	/**
	 * 
	 * @param billId
	 * @return
	 * @author yuwei
	 * @Description: 核单
	 * 2018年11月22日上午10:33:01
	 */
	@RequestMapping("/billAuditing")
	public  Result<String> billAuditing(Long billId){
		LoadBill loadBill=new LoadBill();
		loadBill.setBillId(1l);
		loadBill.setAuditingDate(new Date());
		loadBill.setStatus(BillEnum.RECEIVE.getValue());
		//获取当前用户信息
		/*loadBill.setAuditing("5555");*/
		loadBillService.update(loadBill);
		return ResultUtil.success();
		
	}
	/**
	 * 
	 * @param loadException
	 * @author yuwei
	 * @Description: 保存运单异常的数据
	 * 2018年11月19日上午11:38:47
	 */
	@RequestMapping("/saveLoadException")
	public  Result<String> saveLoadException(@RequestBody LoadException loadException){
		
		loadExceptionService.saveLoadException(loadException);
		
		return ResultUtil.success();
		
	}
	/**
	 * 
	 * @param billId
	 * @param status
	 * @author yuwei
	 * @Description: 取消核单改变状态
	 * 2018年11月12日上午10:27:57
	 */
	@RequestMapping("/updateByStatus")
	public  Result<String> updateByStatus(Long billId){
		
		loadBillService.updateByStatus(billId, BillEnum.UN_RECEIVE.getValue());
		return ResultUtil.success();
		
	}
	/**
	 * 
	 * @param tbLoadBillVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 收运的运单导出
	 * 2018年11月12日下午2:23:45
	 */
	@RequestMapping("/loadBillExport")
	public Result<String> loadBillExport(@RequestBody LoadBillVo tbLoadBillVo,HttpServletResponse response, HttpServletRequest request) {
		  List<LoadBill> list=loadBillService.findListAll(tbLoadBillVo);
	      FileUtil.exportExcel(list, "核单管理", "核单管理", LoadBill.class, "核单管理", response, request);
	      return ResultUtil.success();
	      
	}
}
