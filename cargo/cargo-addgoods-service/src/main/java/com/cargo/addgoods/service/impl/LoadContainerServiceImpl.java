package com.cargo.addgoods.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.entity.LoadContainer;
import com.cargo.addgoods.entity.LoadPreplan;
import com.cargo.addgoods.entity.LoadReleasContainer;
import com.cargo.addgoods.enums.ContainerEnum;
import com.cargo.addgoods.mapper.LoadContainerMapper;
import com.cargo.addgoods.mapper.LoadPreplanMapper;
import com.cargo.addgoods.mapper.LoadReleasContainerMapper;
import com.cargo.addgoods.service.LoadContainerService;
@Service
public class LoadContainerServiceImpl implements LoadContainerService {
	@Autowired
	private LoadContainerMapper tbLoadContainerMapper;
	
	@Autowired
	LoadReleasContainerMapper loadReleasContainerMapper;
	
	@Autowired
	LoadPreplanMapper loadPreplanMapper;
	//根据容器号   获取该容器号中所有的备注信息
	public List<Map<String, Object>> getCommentByContainer(Long num) {
		List<Map<String, Object>> list = tbLoadContainerMapper.getCommentByContainerNum(num);
		return list;
	}

	 //根据容器号，获取该容器内所有的运单信息
	public List<LoadBill> getBills(Long num) {
		List<LoadBill> list = tbLoadContainerMapper.getloadBills(num);
		return list;
	}

    //根据航班号,查询所有航班
	@Override
	public List<LoadContainer> queryLoadContainersByFlightNum(Long flightNum) {
	    return tbLoadContainerMapper.queryByFlightId(flightNum);
	}

	/** 查询默认 容器 状态为空,以加货**/
	@Override
	public List<LoadContainer> queryDefLoadContainers() {
		return tbLoadContainerMapper.queryDefLoadContainers();
	}

	@Override
	public LoadContainer selectByContainer(String container, Integer released) {
		
		return tbLoadContainerMapper.selectByContainer(container, released);
	}

	@Override
	public void restoreContainer(String  container,String releaseDate,Long containerId) {
		//改变LoadContainer的释放状态由1改为0
		tbLoadContainerMapper.updateByreleased(container, releaseDate,0);
		//改变LoadPreplan的释放状态由1改为0。先查出符合条件的预配清单，再改变预配清单的释放状态
		List<LoadPreplan> list=loadPreplanMapper.selectByPreplan(container, releaseDate, 1);//查询容器释放下面的所有预配清单
		for(LoadPreplan loadPreplan:list) {
			loadPreplanMapper.updateByreleased(loadPreplan.getPreplanId(), 0);//释放状态由1改为0
		}
		//删除这个容器释放之后绑定的容器管理的数据
		tbLoadContainerMapper.delById(containerId);
		//释放容器的状态由0改为1
		loadReleasContainerMapper.updateByreleased(container, releaseDate, 1);//是否还原,由0变为1
	}

	@Override
	public List<Map<String, Object>> getCommentByContainer(String num) {
		return null;
	}

	@Override
	public Integer freeContainer(String containerNum,String opener) {
        try {
            //1: 从 集装器表 查询 状态为 已加货的;
            LoadContainer container = tbLoadContainerMapper.selectByContainer(containerNum, ContainerEnum.ADD_GOODS.getValue());
            if (container == null) {
                return 0;
            }
            /***
             * 2: 释放 容器与航班关系 集装器表;
             * 1: 将集装器表的容器记录的一条记录 插入到 容器释放表,生成释放时间;
             * 2: 改变集装器表的状态;
             * 3: 释放时间回填 集装器表;
             */
            LoadReleasContainer loadReleasContainer = new LoadReleasContainer();
            Date releaseDate = new Date();
            loadReleasContainer.setReleaseDate(releaseDate);//释放时间
            //loadReleasContainer.setIdentify();//释放标示
            // loadReleasContainer.setGoodsCode();//加货备注
            loadReleasContainer.setFlightNumber(container.getJoinFlight());//进港航班
            loadReleasContainer.setRestored(0);//0: 未还原 1: 已经还原
            loadReleasContainer.setContainer(containerNum);//容器类型和容器拼接
            loadReleasContainer.setContainerType(container.getContainerType());//容器类型
            loadReleasContainer.setContainerNumber(containerNum+"-去除容器类型");//容器号
            loadReleasContainer.setPersonal(opener);//操作人
            loadReleasContainerMapper.save(loadReleasContainer);
            LoadContainer _updateLoadContainer = new LoadContainer();
            _updateLoadContainer.setContainerId(container.getContainerId());
            _updateLoadContainer.setReleaseDate(releaseDate);
            _updateLoadContainer.setReleased(1);//已释放
            _updateLoadContainer.setStatus(ContainerEnum.EMPTY.getValue());
            tbLoadContainerMapper.update(_updateLoadContainer);
            //3 : 释放 容器与运单关系 预配清单表;
            /***
             *  预配清单表改状态
             *  此容器号下,状态为 正常 记录,修改释放时间 释放状态
             */
            int rel = loadPreplanMapper.updateByContainor(containerNum,releaseDate);//释放正常的记录
            return rel;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    //根据容器号,更新备注,体积,舱位
    @Override
    public void updateMark(Double volume, String spaces, String containerCode, List<String> codes) {
        //更新体积
        tbLoadContainerMapper.updateVolume(volume,containerCode);
        //舱位
        tbLoadContainerMapper.updateSpaces(spaces,containerCode);
        //备注
        if(CollectionUtils.isNotEmpty(codes)){
            StringBuffer sd = new StringBuffer();
            sd.append("-1");
            codes.forEach(e ->{
                if (e != null) {
                    sd.append(",");
                    sd.append(e);
                }
            });
            String marks = sd.toString();
            tbLoadContainerMapper.updateMark(marks,containerCode);
        }


    }

}
