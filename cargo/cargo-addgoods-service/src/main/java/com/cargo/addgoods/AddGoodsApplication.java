package com.cargo.addgoods;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(scanBasePackages="com.cargo")
@EnableEurekaClient
@MapperScan({ "com.cargo.addgoods.mapper" })
public class AddGoodsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AddGoodsApplication.class, args);
	}
}
