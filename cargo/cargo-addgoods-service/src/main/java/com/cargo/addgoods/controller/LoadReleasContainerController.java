package com.cargo.addgoods.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadContainer;
import com.cargo.addgoods.entity.LoadReleasContainer;
import com.cargo.addgoods.vo.LoadContainerVo;
import com.cargo.addgoods.vo.LoadPreplanVo;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.addgoods.service.LoadContainerService;
import com.cargo.addgoods.service.LoadPreplanService;
import com.cargo.addgoods.service.LoadReleasContainerService;
/**
 * 
 * 
 * @author yuwei
 * @Description: 容器释放之后还原的相关业务
 * 2018年11月15日下午1:42:42
 */
@RestController
public class LoadReleasContainerController {
	@Autowired
	LoadReleasContainerService loadReleasContainerService;
	
	@Autowired
	LoadPreplanService loadPreplanService;
	
	@Autowired
	LoadContainerService loadContainerService;
	/**
	 * 
	 * @param loadReleasContainer
	 * @author yuwei
	 * @Description: 保存方法
	 * 2018年11月15日下午2:22:05
	 */
	@RequestMapping("/saveLoadReleasContainer")
	public  Result<String> saveLoadReleasContainer(@RequestBody LoadReleasContainer loadReleasContainer){
		
		loadReleasContainerService.insertSelective(loadReleasContainer);
		
		return ResultUtil.success();
		
	}
	/**
	 * 
	 * @param loadContainerVo
	 * @return
	 * @author yuwei
	 * @Description: 查询释放容器的数据
	 * 2018年11月17日上午11:42:28
	 */
	@RequestMapping("/selectByLoadReleasContainer")
	public  Result<String>  selectByLoadReleasContainer(@RequestBody LoadContainerVo loadContainerVo){
		Map<String, Object> map = new HashMap<String, Object>();
		String releaseDate="";
		List<String> listRelese=new ArrayList<String>();
		List<LoadPreplanVo> listPreplanVo=new ArrayList<LoadPreplanVo>();
			if(loadContainerVo.getReleaseDate()==null||loadContainerVo.getReleaseDate()=="") {
				listRelese= loadReleasContainerService.selectByRelesedate(loadContainerVo);
				if(listRelese.size()>0) {
					releaseDate=listRelese.get(0);
					listPreplanVo=loadPreplanService.selectByRelease(loadContainerVo.getContainer(), releaseDate, 1);//0是没有释放的，1是释放的
				}
				map.put("releaseDate", listRelese);
				map.put("releaseList", listPreplanVo);
				
			}else {
				listPreplanVo=loadPreplanService.selectByRelease(loadContainerVo.getContainer(), loadContainerVo.getReleaseDate(), 1);//0是没有释放的，1是释放的
				map.put("releaseList", listPreplanVo);
			}
		
			return ResultUtil.success(map);
		
	}
	/**
	 * 
	 * @param container
	 * @param releaseDate
	 * @return
	 * @author yuwei
	 * @Description: 容器还原 （0是成功，1是失败）
	 * 2018年11月17日下午3:35:02
	 */
	@RequestMapping("/restoreByContainer")
	public  Result<String>  restoreByContainer(String  container,String releaseDate){
		LoadContainer loadContainer=loadContainerService.selectByContainer(container, 0);//0为没有释放的，1为已经释放的
		//容器状态为0，空板箱可以还原的。不是空板箱不能还原
		if(loadContainer.getStatus()==0) {
			loadContainerService.restoreContainer(container, releaseDate,loadContainer.getContainerId());
		}else {
			return ResultUtil.error("容器不是空闲状态,不能进行还原!");
		}
		return ResultUtil.success();
	}
}
