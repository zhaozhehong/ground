package com.cargo.addgoods.service.impl;
import com.cargo.addgoods.entity.LoadWeight;
import com.cargo.addgoods.vo.LoadWeightVo;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.addgoods.mapper.LoadWeightMapper;
import com.cargo.addgoods.service.LoadWeightService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 
 * @author Administrator
 *
 */


@Service("tbLoadWeightService")
public class LoadWeightServiceImpl   implements LoadWeightService {
	@Autowired
	LoadWeightMapper tbLoadWeightMapper;

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadWeight> selectListAll(LoadWeightVo tbLoadWeightVo) {
		return tbLoadWeightMapper.selectListAll(tbLoadWeightVo);
	}
	

		 
			
   
}
