package com.cargo.addgoods.service.impl;
import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.vo.LoadBillVo;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.addgoods.mapper.LoadBillMapper;
import com.cargo.addgoods.service.LoadBillService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author Administrator
 *
 */


@Service("tbLoadBillService")
public class LoadBillServiceImpl   implements LoadBillService {
	@Autowired
	LoadBillMapper tbLoadBillMapper;

	
	/***
	 * 带条件查询所有运单表的信息
	 * @param tbLoadBillVo
	 * @return
	 */
	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadBill> findListAll(LoadBillVo tbLoadBillVo) {
		return tbLoadBillMapper.findListAll(tbLoadBillVo);
	}

	
	 /**
     * 更新运单状态
     * @param record
     * @return
     */
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int updateByStatus(Long billId, Integer status) {
		return tbLoadBillMapper.updateByStatus(billId, status);
	}

	
	 /**
     * 
     * @param loadBill
     * @author yuwei
     * @Description:核单通过插入一条数据到数据库 
     * 2018年11月12日上午10:22:26
     */
	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public void saveBill(LoadBill loadBill) {
		tbLoadBillMapper.save(loadBill);
		
	}
	 /**
     * 
     * @param tbLoadBillVo
     * @return
     * @author yuwei
     * @Description: 通过查询条件返回列表的票数，合计件数，合计重量
     * 2018年11月13日下午2:00:45
     */
	@SuppressWarnings({ "rawtypes" })
	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public Map totalBill(LoadBillVo tbLoadBillVo) {
		return tbLoadBillMapper.totalBill(tbLoadBillVo);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public LoadBill selectById(Long billId) {
		return tbLoadBillMapper.selectById(billId);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public LoadBill queryByNum(String num) {
		// TODO Auto-generated method stub
		return null;
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int update(LoadBill loadBill) {
		
		return tbLoadBillMapper.update(loadBill);
	}


}
