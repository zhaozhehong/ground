package com.cargo.addgoods.mapper;
import com.cargo.addgoods.entity.UserEntity;
import com.cargo.ground.common.base.BaseMapper;

public interface UserMapper extends BaseMapper<UserEntity> {
	 
}