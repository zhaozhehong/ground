package com.cargo.addgoods.service.impl;
import com.cargo.addgoods.entity.LoadPreplan;
import com.cargo.addgoods.vo.LoadPreplanVo;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.addgoods.mapper.LoadPreplanMapper;
import com.cargo.addgoods.service.LoadPreplanService;

import com.cargo.ground.entity.CheckInBoxVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 
 * @author Administrator
 *
 */


@Service("loadPreplanService")
public class LoadPreplanServiceImpl   implements LoadPreplanService {
	@Autowired
	LoadPreplanMapper loadPreplanMapper;

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadPreplanVo> selectByRelease(String container, String releaseDate, Integer released) {
		return loadPreplanMapper.selectByRelease(container, releaseDate, released);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int updateByreleased(Long preplanId, Integer released) {
		return loadPreplanMapper.updateByreleased(preplanId, released);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadPreplan> queryByContainorNum(String containerNum) {
		return null;
	}

	//核单进箱
	@Override
	public int inputContainer(CheckInBoxVo checkInBoxVo) {
	    //1:判断是否可以新增
        //获取 飞机号
        //2:进行新增
        LoadPreplan preplan = new LoadPreplan();
        //转换
        checkInBoxVoTPpreplan(checkInBoxVo,preplan);
        return loadPreplanMapper.save(preplan);
	}

	//实体类转换
    private void checkInBoxVoTPpreplan(CheckInBoxVo checkInBoxVo, LoadPreplan preplan) {
        //参数,属性转换
        preplan.setAmount(checkInBoxVo.getInputNum());
        preplan.setBillId(checkInBoxVo.getBillNo());
        preplan.setContainerNumber(checkInBoxVo.getContainerNo());
	}
}
