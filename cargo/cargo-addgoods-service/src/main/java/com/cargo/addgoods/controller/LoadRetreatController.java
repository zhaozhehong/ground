package com.cargo.addgoods.controller;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.vo.BillRetreatVo;
import com.cargo.addgoods.vo.RetreatVo;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.ground.util.FileUtil;
import com.cargo.addgoods.service.LoadRetreatService;
/**
 * 
 * 
 * @author yuwei
 * @Description: 退运相关的方法
 * 2018年11月12日上午9:46:52
 */
@RestController
public class LoadRetreatController {
	
	@Autowired
	LoadRetreatService loadRetreatService;
	
	/**
	 * 
	 * @param billRetreatVo
	 * @return
	 * @author yuwei
	 * @Description: 退运原因关联运单表查询
	 * 2018年11月8日下午2:36:57
	 */
	@RequestMapping("/selectByRetreat")
	public   Result<BillRetreatVo> selectByRetreat(@RequestBody BillRetreatVo billRetreatVo){
		
		List<BillRetreatVo> list=loadRetreatService.selectByRetreat(billRetreatVo);
		
		return ResultUtil.success(list);
		
	}
	/**
	 * 
	 * @param billRetreatVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 货物退运的导出
	 * 2018年11月8日下午2:41:19
	 */
	@RequestMapping("/loadRetreatExport")
	public Result<String> loadRetreatExport(@RequestBody BillRetreatVo billRetreatVo,HttpServletResponse response, HttpServletRequest request) {
		  List<BillRetreatVo> list=loadRetreatService.selectByRetreat(billRetreatVo);
	      FileUtil.exportExcel(list, "货物退运", "货物退运", BillRetreatVo.class, "货物退运", response, request);
	      return ResultUtil.success();
	}
	
	/**
	 * 
	 * @param retreatId
	 * @author yuwei
	 * @Description: 取消退运
	 * 2018年11月20日上午11:32:53
	 */
	@RequestMapping("/cancelRetreat")
	public Result<String> cancelRetreat(Long retreatId,Long billId) {
		
		loadRetreatService.delById(retreatId,billId);
		return ResultUtil.success();
	}
/**
 * 
 * @param billRetreatVo
 * @param response
 * @param request
 * @author yuwei
 * @Description: 退运锂电池日志查询的导出
 * 2018年11月8日下午4:01:41
 */
	@RequestMapping("/loadBatteryExport")
	public Result<String> loadBatteryExport(@RequestBody BillRetreatVo billRetreatVo,HttpServletResponse response, HttpServletRequest request) {
		  billRetreatVo.setReturnType(1);//查询锂电池的
		  List<RetreatVo> list=loadRetreatService.selectByRetreatbattery(billRetreatVo);
	      FileUtil.exportExcel(list, "退运锂电池日志查询", "退运锂电池日志查询", RetreatVo.class, "退运锂电池日志查询", response, request);
	      return ResultUtil.success();
	}

}
