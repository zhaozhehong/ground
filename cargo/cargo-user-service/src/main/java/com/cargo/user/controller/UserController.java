package com.cargo.user.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.user.entity.User;
import com.cargo.user.service.UserService;

@Controller
public class UserController {

	@Resource
	private UserService userService;
	

	@PostMapping("/getUserByUserName/{userName}")
	@ResponseBody
	public User getUserByUserName(@PathVariable("userName") String userName){
		User u =  userService.selectUserByUserName(userName);
		return u;
	}
	
	@GetMapping("/insert")
	@ResponseBody
	public Result<String> insert(){
		userService.save(new User("kj3","e10adc3949ba59abbe56e057f20f883e"));
		return ResultUtil.success();
	}
}
