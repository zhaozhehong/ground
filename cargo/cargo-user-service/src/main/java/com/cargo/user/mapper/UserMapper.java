package com.cargo.user.mapper;
import com.cargo.ground.common.base.BaseMapper;
import com.cargo.user.entity.User;

public interface UserMapper extends BaseMapper<User> {
	 
	User selectUserByUserName(String userName);
}