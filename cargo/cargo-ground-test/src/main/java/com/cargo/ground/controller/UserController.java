package com.cargo.ground.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.ground.config.log.OperaLog;
import com.cargo.ground.entity.UserEntity;
import com.cargo.ground.enums.UserSexEnum;
import com.cargo.ground.service.UserInfoService;
import com.cargo.ground.util.PageDataResult;
import com.cargo.ground.util.RedisUtil;

 
@RestController
public class UserController {
	
	 
	
	@Autowired
	private  UserInfoService   userInfoService;
	@Autowired
	RedisUtil<String, String> redisUtil;
	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);
	
	// 测试redis
		@RequestMapping("/testredis")
		@ResponseBody
		public String test() throws Exception {

			String user2 = redisUtil.get("kjtest12");
			if (user2 == null || user2 == "") {
				user2 = "凯捷科技有限公司！！";
				redisUtil.set("kjtest12", user2);
			}
			System.out.println(user2);

			return user2;
		}
		
	@OperaLog(description = "用户信息查询导入表")
	@RequestMapping("/getUsers")
	public List<UserEntity> getUsers() {
		UserEntity user=new UserEntity();
		user.setId(10123L);
		user.setUserName("xiaoyifei");
		return userInfoService.selectList(user);
	}
	
	/**
	 * 分页查询用户列表
	 * @return ok/fail
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getUsersPage", method = RequestMethod.POST)
	@ResponseBody
	public PageDataResult getUsersPage(@RequestParam("page") Integer page,
			@RequestParam("limit") Integer limit, UserEntity userSearch){
		logger.debug("分页查询用户列表！搜索条件：userSearch： ,page:" + page
				+ ",每页记录数量limit:" + limit);
		try {
			//  return userInfoService.getPage(userSearch, page, limit);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("查询异常");
		}
		return null;
	}
	
    @RequestMapping("/getUser")
    public UserEntity getUser(Long id) {
		return userInfoService.selectById(id);
    }
    
    @RequestMapping("/add")
    public void save(UserEntity user) {
    	try {
    		for(int i=0;i<100;i++) {
    	 user.setUserName("xiaoyifei");
    	 user.setPassWord("1233");
    	 user.setNickName("jjjj");
    	 user.setUserSex(UserSexEnum.MAN);
    	 userInfoService.save(user);
    		}
    	} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("查询异常"+e.getMessage());
		}
    }
    
    @RequestMapping(value="update")
    public void update(UserEntity user) {
    	userInfoService.update(user);
    }
    
    @RequestMapping(value="/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
		userInfoService.delById(id);
    }
    
    
}