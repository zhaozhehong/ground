package com.cargo.ground.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.common.base.impl.BaseServiceImpl;
import com.cargo.ground.entity.UserEntity;
import com.cargo.ground.mapper.UserMapper;
import com.cargo.ground.service.UserInfoService;
@Service
public class UserInfoServiceImpl extends BaseServiceImpl<UserEntity>  implements   UserInfoService {
	
	@Resource
	private UserMapper userMapper;
	
	@Override
	public BaseMapper<UserEntity> getMapper() {
		return userMapper;
	}
//    @Resource
//    private UserInfoDao userInfoDao;
    

//	@Override
//	public List<UserEntity> getList(UserEntity userSearch) {
//		// TODO Auto-generated method stub
//		return userMapper.getList(userSearch);
//	}
//	 
//
//	@Override
//	public PageDataResult getPage(UserEntity userSearch, int page, int limit) {
//		PageDataResult pdr = new PageDataResult();
//		PageHelper.startPage(page, limit);
//		List<UserEntity> urList = userMapper.getList(userSearch);
//		// 获取分页查询后的数据
//		PageInfo<UserEntity> pageInfo = new PageInfo<>(urList);
//		PageHelper.startPage(page, limit);
//		pdr.setTotals(Long.valueOf(pageInfo.getTotal()).intValue());
//		pdr.setList(urList);
//		return pdr;
//	}
//
//	 
//
//	@Override
//	public void insert(UserEntity user) {
//		  userMapper.insert(user);
//	}
//
//	@Override
//	public void update(UserEntity user) {
//		  userMapper.update(user);
//	}

	 
 
}