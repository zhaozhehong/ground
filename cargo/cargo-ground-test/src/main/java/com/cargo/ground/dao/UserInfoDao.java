package com.cargo.ground.dao;
 
import org.springframework.data.repository.CrudRepository;

import com.cargo.ground.entity.UserEntity;

public interface UserInfoDao extends CrudRepository<UserEntity,Long> {
    /**通过username查找用户信息;*/
    
}