package com.cargo.ground.controller;

/**地面系统项目
 * @Description 
 * @author kj_xiaoyifei
 * @date 2018年10月26日
**/
import javax.annotation.Resource;
import javax.jms.Destination;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
/**
 * 通过页面访问的方式生产消息
 */
@RestController
public class ProviderController {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Resource(name = "queue1")
	private Destination queue1;
	private static final Logger logger = LoggerFactory
			.getLogger(ProviderController.class);
	@Resource(name = "topic1")
	private Destination topic1;
	@RequestMapping("/send/queue1")
	public String send1() {
 		for(int i=0;i<18;i++) {
 		  System.out.println("消息发送");
 		  jmsTemplate.convertAndSend(queue1, "hello queue-1:"+i);
 		}
//		System.out.println("消息发送");
//		jmsTemplate.convertAndSend(queue1, "hello queue-1" );
		return "ok";
	}
	
	@RequestMapping("/send/topic1")
	public String send2() {
		for(int i=0;i<18;i++) {
			System.out.println("消息发送");
			logger.info("消息发送");
		   jmsTemplate.convertAndSend(topic1, "hello topic-1:"+i);
		}
		return "ok";
	}
	
	
	@RequestMapping("/send/log")
	public String sendlog() {
		for(int i=0;i<18;i++) {
		 
			logger.info("消息发送"+i+"info");
			
			logger.warn("消息发送"+i+"warn");
			
			logger.error("消息发送"+i+"error");
		    
		}
		return "ok";
	}

	
}
