package com.cargo.ground.entity;

public class AirLine {
    //主键
    private Long id;

    //航班号
    private String flightNum;

    //起始站
    private String startStation;

    //终点站
    private String endtStation;

    /**
     * 鑾峰彇主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 璁剧疆主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 鑾峰彇航班号
     *
     * @return FLIGHT_NUM - 航班号
     */
    public String getFlightNum() {
        return flightNum;
    }

    /**
     * 璁剧疆航班号
     *
     * @param flightNum 航班号
     */
    public void setFlightNum(String flightNum) {
        this.flightNum = flightNum == null ? null : flightNum.trim();
    }

    /**
     * 鑾峰彇起始站
     *
     * @return START_STATION - 起始站
     */
    public String getStartStation() {
        return startStation;
    }

    /**
     * 璁剧疆起始站
     *
     * @param startStation 起始站
     */
    public void setStartStation(String startStation) {
        this.startStation = startStation == null ? null : startStation.trim();
    }

    /**
     * 鑾峰彇终点站
     *
     * @return ENDT_STATION - 终点站
     */
    public String getEndtStation() {
        return endtStation;
    }

    /**
     * 璁剧疆终点站
     *
     * @param endtStation 终点站
     */
    public void setEndtStation(String endtStation) {
        this.endtStation = endtStation == null ? null : endtStation.trim();
    }
}