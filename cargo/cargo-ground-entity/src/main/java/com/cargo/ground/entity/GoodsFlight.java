package com.cargo.ground.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 报T货航班
 */
public class GoodsFlight  implements Serializable {
    //ID
    private Long id;

    //航班号ID
    private Long flightId;

    //起飞站
    private Long takeOff;

    //目的站
    private Long purpose;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取航班号ID
     *
     * @return FLIGHT_ID - 航班号ID
     */
    public Long getFlightId() {
        return flightId;
    }

    /**
     * 设置航班号ID
     *
     * @param flightId 航班号ID
     */
    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    /**
     * 获取起飞站
     *
     * @return TAKE_OFF - 起飞站
     */
    public Long getTakeOff() {
        return takeOff;
    }

    /**
     * 设置起飞站
     *
     * @param takeOff 起飞站
     */
    public void setTakeOff(Long takeOff) {
        this.takeOff = takeOff;
    }

    /**
     * 获取目的站
     *
     * @return PURPOSE - 目的站
     */
    public Long getPurpose() {
        return purpose;
    }

    /**
     * 设置目的站
     *
     * @param purpose 目的站
     */
    public void setPurpose(Long purpose) {
        this.purpose = purpose;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}