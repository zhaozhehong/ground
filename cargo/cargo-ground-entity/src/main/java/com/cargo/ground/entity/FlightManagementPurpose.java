package com.cargo.ground.entity;

import java.io.Serializable;

/**
 * 联程航班管理目的站
 */
public class FlightManagementPurpose  implements Serializable {
    //ID
    private Long id;

    //联程航班ID
    private Long flightNumber;

    //目的站
    private Long takeOff;

    //终点站
    private Long purpose;

    //序号
    private Long index;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取联程航班ID
     *
     * @return FLIGHT_NUMBER - 联程航班ID
     */
    public Long getFlightNumber() {
        return flightNumber;
    }

    /**
     * 设置联程航班ID
     *
     * @param flightNumber 联程航班ID
     */
    public void setFlightNumber(Long flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * 获取目的站
     *
     * @return TAKE_OFF - 目的站
     */
    public Long getTakeOff() {
        return takeOff;
    }

    /**
     * 设置目的站
     *
     * @param takeOff 目的站
     */
    public void setTakeOff(Long takeOff) {
        this.takeOff = takeOff;
    }

    /**
     * 获取终点站
     *
     * @return PURPOSE - 终点站
     */
    public Long getPurpose() {
        return purpose;
    }

    /**
     * 设置终点站
     *
     * @param purpose 终点站
     */
    public void setPurpose(Long purpose) {
        this.purpose = purpose;
    }

    /**
     * 获取序号
     *
     * @return INDEX - 序号
     */
    public Long getIndex() {
        return index;
    }

    /**
     * 设置序号
     *
     * @param index 序号
     */
    public void setIndex(Long index) {
        this.index = index;
    }
}