package com.cargo.ground.entity;

/**
 * @Desc 核单进箱参数封装
 * @LastPeson xuxu
 **/
public class CheckInBoxVo {

    //运单
    private Long billNo;
    //容器号
    private String containerNo;
    //录入重量
    private Double InputWeight;
    //录入件数
    private Long InputNum;
    //计费重量
    private Double chargingWeight;
    //加货人
    private Long addGoddsMan;
    //机口
    private Long sport;
    //开单件数
    private Long preNum;
    //开单重量
    private Double preWeight;
    //剩余件数
    private Long leftNum;
    //剩余重量
    private Double leftWeight;

    public Long getBillNo() {
        return billNo;
    }

    public void setBillNo(Long billNo) {
        this.billNo = billNo;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public Double getInputWeight() {
        return InputWeight;
    }

    public void setInputWeight(Double inputWeight) {
        InputWeight = inputWeight;
    }

    public Long getInputNum() {
        return InputNum;
    }

    public void setInputNum(Long inputNum) {
        InputNum = inputNum;
    }

    public Double getChargingWeight() {
        return chargingWeight;
    }

    public void setChargingWeight(Double chargingWeight) {
        this.chargingWeight = chargingWeight;
    }

    public Long getAddGoddsMan() {
        return addGoddsMan;
    }

    public void setAddGoddsMan(Long addGoddsMan) {
        this.addGoddsMan = addGoddsMan;
    }

    public Long getSport() {
        return sport;
    }

    public void setSport(Long sport) {
        this.sport = sport;
    }

    public Long getPreNum() {
        return preNum;
    }

    public void setPreNum(Long preNum) {
        this.preNum = preNum;
    }

    public Double getPreWeight() {
        return preWeight;
    }

    public void setPreWeight(Double preWeight) {
        this.preWeight = preWeight;
    }

    public Long getLeftNum() {
        return leftNum;
    }

    public void setLeftNum(Long leftNum) {
        this.leftNum = leftNum;
    }

    public Double getLeftWeight() {
        return leftWeight;
    }

    public void setLeftWeight(Double leftWeight) {
        this.leftWeight = leftWeight;
    }
}
