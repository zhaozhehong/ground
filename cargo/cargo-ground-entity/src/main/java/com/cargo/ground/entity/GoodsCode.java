package com.cargo.ground.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 货物代码管理
 * @author: liyiting
 */
public class GoodsCode  implements Serializable {
    //ID
    private Long id;

    //货物代码
    private String goodsCode;

    //含义
    private String meaning;

    //英文含义
    private String englishMeaning;

    //创建时间
    private Date createDate;

    //创建人
    private String createUser;

    //修改时间
    private Date modifyDate;

    //修改人
    private String modifyUser;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取货物代码
     *
     * @return GOODS_CODE - 货物代码
     */
    public String getGoodsCode() {
        return goodsCode;
    }

    /**
     * 设置货物代码
     *
     * @param goodsCode 货物代码
     */
    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode == null ? null : goodsCode.trim();
    }

    /**
     * 获取含义
     *
     * @return MEANING - 含义
     */
    public String getMeaning() {
        return meaning;
    }

    /**
     * 设置含义
     *
     * @param meaning 含义
     */
    public void setMeaning(String meaning) {
        this.meaning = meaning == null ? null : meaning.trim();
    }

    /**
     * 获取英文含义
     *
     * @return ENGLISH_MEANING - 英文含义
     */
    public String getEnglishMeaning() {
        return englishMeaning;
    }

    /**
     * 设置英文含义
     *
     * @param englishMeaning 英文含义
     */
    public void setEnglishMeaning(String englishMeaning) {
        this.englishMeaning = englishMeaning == null ? null : englishMeaning.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return MODIFY_DATE - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取修改人
     *
     * @return MODIFY_USER - 修改人
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * 设置修改人
     *
     * @param modifyUser 修改人
     */
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser == null ? null : modifyUser.trim();
    }
}