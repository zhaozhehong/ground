package com.cargo.user.entity;

import java.io.Serializable;

public class User implements Serializable{
	
	/**
	 * Administrator
	 * @Description: 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String userName;
	private String passWord;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public User() {
	}

	public User(String password, String userName) {
		this.passWord = password;
		this.userName = userName;
	}
}
