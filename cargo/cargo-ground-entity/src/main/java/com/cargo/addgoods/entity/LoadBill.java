package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;

import cn.afterturn.easypoi.excel.annotation.Excel;
/**
 * 
 * 
 * @author yuwei
 * @Description: 运单表
 * 2018年11月12日下午3:21:06
 */
public class LoadBill implements Serializable {

	private static final long serialVersionUID = 1L;
    /**
     * 运单id
     */
    private Long billId;

    /**
     * 航班动态id
     */
    private Long flightId;
    /**
     * 货物类型
     */
    private Integer type;
    /**
     * 加货备注
     */
    private String remark;
    /**
     * 舱位
     */
    @Excel(name = "舱位", orderNum = "5")
    private String cabin;
    /**
     * 体积
     */
    private Double volume;
    /**
     * 剩余重量
     */
    private Long remainWeight;
    /**
     * 剩余件数
     */
    private Long remainAmount;
    /**
     * 核单日期
     */
    @Excel(name = "核单日期", exportFormat = "yyyy-MM-dd HH:mm:ss",width=18,mergeVertical=false,mergeRely={0}, orderNum = "11")
    private Date auditingDate;
    /**
     * 核单人
     */
    private String auditing;
    /**
     * 状态(未收运0,已收运1,退货2，异常3)
     */
    private Integer status;
    /**
     * 承运人
     */
    private String transportUser;
    /**
     * 制单人
     */
    private String documentMaker;
    /**
     * 航程
     */
    private String range;
    /**
     * 航班日期
     */
    @Excel(name = "航班日期", exportFormat = "yyyy-MM-dd HH:mm:ss",width=18,mergeVertical=false,mergeRely={0}, orderNum = "10")
    private Date flightDate;
    /**
     * 品名
     */
    private String productName;
    /**
     * 特码
     */
    private String specialCode;
    /**
     * 航班号
     */
    @Excel(name = "航班号", orderNum = "9")
    private String flightNumber;
    /**
     * 计费重量
     */
    @Excel(name = "计费重量", orderNum = "8")
    private Long feeWeight;
    /**
     * 重量
     */
    @Excel(name = "重量", orderNum = "7")
    private Long weight;
    /**
     * 件数
     */
    @Excel(name = "件数", orderNum = "6")
    private Long amount;
    /**
     * 目的站
     */
    @Excel(name = "目的站", orderNum = "4")
    private String end;
    /**
     * 起始站
     */
    @Excel(name = "起始站", orderNum = "3")
    private String startPort;
    /**
     * 代理人
     */
    @Excel(name = "代理人", orderNum = "2")
    private String agent;
    /**
     * 单号
     */
    @Excel(name = "单号", orderNum = "1")
    private String oddNumber;
    /**
     * 前缀
     */
    @Excel(name = "前缀", orderNum = "0")
    private String prefix;
    /**
     * 承运人
     */
    private String freighter;
    /**
     * 货物代码
     */
    private String cargoCode;
    /**
     * 货物名称
     */
    private String cargoName;
    /**
     * 运单类型
     */
    private String billType;

	public Long getBillId() {
		return billId;
	}


	public void setBillId(Long billId) {
		this.billId = billId;
	}


	public Long getFlightId() {
		return flightId;
	}


	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}
	
	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getCabin() {
		return cabin;
	}


	public void setCabin(String cabin) {
		this.cabin = cabin;
	}


	public Double getVolume() {
		return volume;
	}


	public void setVolume(Double volume) {
		this.volume = volume;
	}


	public Long getRemainWeight() {
		return remainWeight;
	}


	public void setRemainWeight(Long remainWeight) {
		this.remainWeight = remainWeight;
	}


	public Long getRemainAmount() {
		return remainAmount;
	}


	public void setRemainAmount(Long remainAmount) {
		this.remainAmount = remainAmount;
	}


	public Date getAuditingDate() {
		return auditingDate;
	}


	public void setAuditingDate(Date auditingDate) {
		this.auditingDate = auditingDate;
	}


	public String getAuditing() {
		return auditing;
	}


	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getTransportUser() {
		return transportUser;
	}


	public void setTransportUser(String transportUser) {
		this.transportUser = transportUser;
	}


	public String getDocumentMaker() {
		return documentMaker;
	}


	public void setDocumentMaker(String documentMaker) {
		this.documentMaker = documentMaker;
	}


	public String getRange() {
		return range;
	}


	public void setRange(String range) {
		this.range = range;
	}


	public Date getFlightDate() {
		return flightDate;
	}


	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getSpecialCode() {
		return specialCode;
	}


	public void setSpecialCode(String specialCode) {
		this.specialCode = specialCode;
	}


	public String getFlightNumber() {
		return flightNumber;
	}


	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}


	public Long getFeeWeight() {
		return feeWeight;
	}


	public void setFeeWeight(Long feeWeight) {
		this.feeWeight = feeWeight;
	}


	public Long getWeight() {
		return weight;
	}


	public void setWeight(Long weight) {
		this.weight = weight;
	}


	public Long getAmount() {
		return amount;
	}


	public void setAmount(Long amount) {
		this.amount = amount;
	}


	public String getEnd() {
		return end;
	}


	public void setEnd(String end) {
		this.end = end;
	}





	public String getStartPort() {
		return startPort;
	}


	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}


	public String getAgent() {
		return agent;
	}


	public void setAgent(String agent) {
		this.agent = agent;
	}


	public String getOddNumber() {
		return oddNumber;
	}


	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}


	public String getPrefix() {
		return prefix;
	}


	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	

	public String getBillType() {
		return billType;
	}


	public void setBillType(String billType) {
		this.billType = billType;
	}
	
	


	public String getFreighter() {
		return freighter;
	}


	public void setFreighter(String freighter) {
		this.freighter = freighter;
	}


	public String getCargoCode() {
		return cargoCode;
	}


	public void setCargoCode(String cargoCode) {
		this.cargoCode = cargoCode;
	}


	public String getCargoName() {
		return cargoName;
	}


	public void setCargoName(String cargoName) {
		this.cargoName = cargoName;
	}


	public LoadBill() {
		super();
	}


	public LoadBill(Long billId, Long flightId, Integer type, String remark, String cabin, Double volume,
			Long remainWeight, Long remainAmount, Date auditingDate, String auditing, Integer status,
			String transportUser, String documentMaker, String range, Date flightDate, String productName,
			String specialCode, String flightNumber, Long feeWeight, Long weight, Long amount, String end,
			String startPort, String agent, String oddNumber, String prefix, String freighter, String cargoCode,
			String cargoName, String billType) {
		super();
		this.billId = billId;
		this.flightId = flightId;
		this.type = type;
		this.remark = remark;
		this.cabin = cabin;
		this.volume = volume;
		this.remainWeight = remainWeight;
		this.remainAmount = remainAmount;
		this.auditingDate = auditingDate;
		this.auditing = auditing;
		this.status = status;
		this.transportUser = transportUser;
		this.documentMaker = documentMaker;
		this.range = range;
		this.flightDate = flightDate;
		this.productName = productName;
		this.specialCode = specialCode;
		this.flightNumber = flightNumber;
		this.feeWeight = feeWeight;
		this.weight = weight;
		this.amount = amount;
		this.end = end;
		this.startPort = startPort;
		this.agent = agent;
		this.oddNumber = oddNumber;
		this.prefix = prefix;
		this.freighter = freighter;
		this.cargoCode = cargoCode;
		this.cargoName = cargoName;
		this.billType = billType;
	}


	@Override
	public String toString() {
		return "TbLoadBill [billId=" + billId + ", flightId=" + flightId + ", type=" + type + ", remark=" + remark
				+ ", cabin=" + cabin + ", volume=" + volume + ", remainWeight=" + remainWeight + ", remainAmount="
				+ remainAmount + ", auditingDate=" + auditingDate + ", auditing=" + auditing + ", status=" + status
				+ ", transportUser=" + transportUser + ", documentMaker=" + documentMaker + ", range=" + range
				+ ", flightDate=" + flightDate + ", productName=" + productName + ", specialCode=" + specialCode
				+ ", flightNumber=" + flightNumber + ", feeWeight=" + feeWeight + ", weight=" + weight + ", amount="
				+ amount + ", end=" + end + ", startPort=" + startPort + ", agent=" + agent + ", oddNumber=" + oddNumber
				+ ", prefix=" + prefix + ", freighter=" + freighter + ", cargoCode=" + cargoCode + ", cargoName="
				+ cargoName + ", billType=" + billType + "]";
	}


	
	
	

	

	
}