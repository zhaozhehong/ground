package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * 预配清单表
 * @author Administrator
 *
 */

public class LoadPreplan implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 预配清单ID主键
     */
    private Long preplanId;
    
	  /**
     * 机口
     */
    private Integer mouth;
    /**
     * 运单ID
     */
    private Long billId;
    /**
     * 容器ID
     */
    private Long containId;
    /**
     * 体积
     */
    private Double volume;
    /**
     * 重量
     */
    private Long weight;
    /**
     * 件数
     */
    private Long amount;
    /**
     * 单号
     */
    private String oddNumber;
    /**
     * 核对结果
     */
    private Integer checkResult;
    /**
     * 备注
     */
    private String remark;
    /**
     * 容器号
     */
    private String containerNumber;
    
    private String flightNumber;//航班号
    
    private String goodsCode;//加货备注编码
    
    private String goodsName;//加货备注名称
    
    private String identify;//释放标识

    private Integer released;//是否释放
    
    private String containerType;//容器类型
    
    private String container;//容器类型和容器拼接
    
    //释放时间
    private Date releaseDate;

	public Long getPreplanId() {
		return preplanId;
	}



	public void setPreplanId(Long preplanId) {
		this.preplanId = preplanId;
	}



	public Integer getMouth() {
		return mouth;
	}



	public void setMouth(Integer mouth) {
		this.mouth = mouth;
	}



	public Long getBillId() {
		return billId;
	}



	public void setBillId(Long billId) {
		this.billId = billId;
	}



	public Long getContainId() {
		return containId;
	}



	public void setContainId(Long containId) {
		this.containId = containId;
	}



	public Double getVolume() {
		return volume;
	}



	public void setVolume(Double volume) {
		this.volume = volume;
	}



	public Long getWeight() {
		return weight;
	}



	public void setWeight(Long weight) {
		this.weight = weight;
	}



	public Long getAmount() {
		return amount;
	}



	public void setAmount(Long amount) {
		this.amount = amount;
	}



	public String getOddNumber() {
		return oddNumber;
	}



	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}



	public Integer getCheckResult() {
		return checkResult;
	}



	public void setCheckResult(Integer checkResult) {
		this.checkResult = checkResult;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	




	public String getFlightNumber() {
		return flightNumber;
	}



	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}



	public String getGoodsCode() {
		return goodsCode;
	}



	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}



	public String getGoodsName() {
		return goodsName;
	}



	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}



	public String getIdentify() {
		return identify;
	}



	public void setIdentify(String identify) {
		this.identify = identify;
	}



	public Integer getReleased() {
		return released;
	}



	public void setReleased(Integer released) {
		this.released = released;
	}

   


	public String getContainerNumber() {
		return containerNumber;
	}



	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}



	public String getContainerType() {
		return containerType;
	}



	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}



	public String getContainer() {
		return container;
	}



	public void setContainer(String container) {
		this.container = container;
	}



	public Date getReleaseDate() {
		return releaseDate;
	}



	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}



	public LoadPreplan() {
		super();
	}






	

}