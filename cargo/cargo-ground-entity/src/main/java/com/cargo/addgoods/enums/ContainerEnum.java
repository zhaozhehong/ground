package com.cargo.addgoods.enums;

/**
 * @Desc 容器状态
 * 状态（0 空板箱 1 已加货 2 上舱单 3 拉货  4 出港 5 异常 ）
 * @LastPeson xuxu
 **/
public enum  ContainerEnum {

    EMPTY("空板箱",0),
    ADD_GOODS("已加货",1),
    PRE_MANIFEST("上舱单",2),
    PULL_IN("拉货",3),
    OUT_PORT("出港",4),
    EXP("异常",5),

    //是否已释放
    UN_RELEAS("正常",0),
    RELEASED("已释放",1);

    private String desc;
    private Integer value;
    ContainerEnum(String _desc,Integer _value){
        this.desc = _desc;
        this.value = _value;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public Integer getValue() {
        return value;
    }
    public void setValue(Integer value) {
        this.value = value;
    }
}
