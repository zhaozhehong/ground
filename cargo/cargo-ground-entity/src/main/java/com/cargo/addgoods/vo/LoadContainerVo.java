package com.cargo.addgoods.vo;
/**
 * 
 * 
 * @author yuwei
 * @Description: 容器相关的查询条件
 * 2018年11月15日下午7:40:43
 */
public class LoadContainerVo {
	
	 private String container;//容器类型和容器拼接
	 
	 private String startDate;//开始日期（查询条件的）
	 
	 private String endDate;//结束日期（查询条件的）
	 
	 private String releaseDate;//释放操作时间

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	 
	 
	 

}
