package com.cargo.addgoods.vo;

import java.util.Date;

import cn.afterturn.easypoi.excel.annotation.Excel;

public class RetreatVo {
	
	
    private String prefix;//前缀
	
    @Excel(name = "代理人", orderNum = "5")
    private String agent;// 代理人（运单）
    
    @Excel(name = "运单号", orderNum = "3")
	private String oddNumber;//单号
    
	@Excel(name = "预配航班号", orderNum = "2")
	private String flightNumber;//航班号
    
	@Excel(name = "目的站", orderNum = "4")
	private String end;//目的站
	
	
    private String startPort;//起始站
	
	
    private String returnDateStart;//退运开始时间
    
    private String returnDateEnd;//退运结束时间
    
    @Excel(name = "加货员", orderNum = "13")
    private String returnUser;//退运人或者加货员
    /**
     * 锂电池退运的查询需要带这个参数,值为1
     */
    private Integer returnType;//退运类型（0是非锂电池 1是锂电池）
    
    private String freighter;//承运人
    
   
    private String productName;//品名
    
    private String retreatCode;//退运原因代码
    
    @Excel(name = "开单重量", orderNum = "7")
    private Long weight;//开单重量
    
    @Excel(name = "开单件数", orderNum = "6")
    private Long amount;//开单件数
    
    @Excel(name = "未收运重量", orderNum = "10")
    private Long returnWeight;//退运重量或者未收运重量
    
   
    private Long returnAmount;//退运件数
    
    @Excel(name = "日期", exportFormat = "yyyy-MM-dd HH:mm:ss",width=18,mergeVertical=false,mergeRely={0}, orderNum = "1")
    private Date returnDate;// 退运时间
    
    @Excel(name = "退运原因", orderNum = "11")
    private String retreatCause;//退运原因
    
    @Excel(name = "违规详情", orderNum = "12")
    private String remark;//违规详情
    
    @Excel(name = "序号", orderNum = "0")
    private String serialNumber;//序号
    
    @Excel(name = "收运重量", orderNum = "9")
    private Long collectionWeight;//收运重量
    
    @Excel(name = "收运件数", orderNum = "8")
    private Long collectionAmount;//收运件数
    
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getOddNumber() {
		return oddNumber;
	}
	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getStartPort() {
		return startPort;
	}
	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}
	public String getReturnDateStart() {
		return returnDateStart;
	}
	public void setReturnDateStart(String returnDateStart) {
		this.returnDateStart = returnDateStart;
	}
	public String getReturnDateEnd() {
		return returnDateEnd;
	}
	public void setReturnDateEnd(String returnDateEnd) {
		this.returnDateEnd = returnDateEnd;
	}
	public String getReturnUser() {
		return returnUser;
	}
	public void setReturnUser(String returnUser) {
		this.returnUser = returnUser;
	}
	public Integer getReturnType() {
		return returnType;
	}
	public void setReturnType(Integer returnType) {
		this.returnType = returnType;
	}
	public String getFreighter() {
		return freighter;
	}
	public void setFreighter(String freighter) {
		this.freighter = freighter;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getRetreatCode() {
		return retreatCode;
	}
	public void setRetreatCode(String retreatCode) {
		this.retreatCode = retreatCode;
	}
	public Long getWeight() {
		return weight;
	}
	public void setWeight(Long weight) {
		this.weight = weight;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Long getReturnWeight() {
		return returnWeight;
	}
	public void setReturnWeight(Long returnWeight) {
		this.returnWeight = returnWeight;
	}
	public Long getReturnAmount() {
		return returnAmount;
	}
	public void setReturnAmount(Long returnAmount) {
		this.returnAmount = returnAmount;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public String getRetreatCause() {
		return retreatCause;
	}
	public void setRetreatCause(String retreatCause) {
		this.retreatCause = retreatCause;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Long getCollectionWeight() {
		return collectionWeight;
	}
	public void setCollectionWeight(Long collectionWeight) {
		this.collectionWeight = collectionWeight;
	}
	public Long getCollectionAmount() {
		return collectionAmount;
	}
	public void setCollectionAmount(Long collectionAmount) {
		this.collectionAmount = collectionAmount;
	}
   
}
