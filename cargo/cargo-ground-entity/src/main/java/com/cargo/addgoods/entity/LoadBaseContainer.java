package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * 容器表
 * @author Administrator
 *
 */
public class LoadBaseContainer implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 容器号
     */
    private Long baseContainerId;
	 /**
     * 是否删除
     */
    private Integer isDeleted;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改用户
     */
    private String updateUser;
    /**
     * 添加时间
     */
    private Date createTime;
    /**
     * 添加用户
     */
    private String createUser;
    /**
     * 所属人
     */
    private String baseContainerOwner;
    /**
     * 容器类型ID
     */
    private Long containerTypeId;
    
    

	public Long getBaseContainerId() {
		return baseContainerId;
	}



	public void setBaseContainerId(Long baseContainerId) {
		this.baseContainerId = baseContainerId;
	}



	public Integer getIsDeleted() {
		return isDeleted;
	}



	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}



	public Date getUpdateTime() {
		return updateTime;
	}



	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}



	public String getUpdateUser() {
		return updateUser;
	}



	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}



	public Date getCreateTime() {
		return createTime;
	}



	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}



	public String getCreateUser() {
		return createUser;
	}



	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}



	public String getBaseContainerOwner() {
		return baseContainerOwner;
	}



	public void setBaseContainerOwner(String baseContainerOwner) {
		this.baseContainerOwner = baseContainerOwner;
	}



	public Long getContainerTypeId() {
		return containerTypeId;
	}



	public void setContainerTypeId(Long containerTypeId) {
		this.containerTypeId = containerTypeId;
	}



	public LoadBaseContainer() {
		super();
	}



	public LoadBaseContainer(Long baseContainerId, Integer isDeleted, Date updateTime, String updateUser,
			Date createTime, String createUser, String baseContainerOwner, Long containerTypeId) {
		super();
		this.baseContainerId = baseContainerId;
		this.isDeleted = isDeleted;
		this.updateTime = updateTime;
		this.updateUser = updateUser;
		this.createTime = createTime;
		this.createUser = createUser;
		this.baseContainerOwner = baseContainerOwner;
		this.containerTypeId = containerTypeId;
	}



	@Override
	public String toString() {
		return "TbLoadBaseContainer [baseContainerId=" + baseContainerId + ", isDeleted=" + isDeleted + ", updateTime="
				+ updateTime + ", updateUser=" + updateUser + ", createTime=" + createTime + ", createUser="
				+ createUser + ", baseContainerOwner=" + baseContainerOwner + ", containerTypeId=" + containerTypeId
				+ "]";
	}



	
	

}