package com.cargo.addgoods.entity;

import java.io.Serializable;
/**
 * 
 * 
 * @author yuwei
 * @Description: 异常表
 * 2018年11月19日下午1:59:33
 */
public class LoadException implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long exceptionId;//运单异常ID
	private String exceptionText;//异常内容，内容多条的话用逗号隔开存入
	private Integer exceptionType;//异常类型
	private Long flightBillId;//航班id/运单id
	private String exceptionCode;//异常内容对应的code值
	public Long getExceptionId() {
		return exceptionId;
	}
	public void setExceptionId(Long exceptionId) {
		this.exceptionId = exceptionId;
	}
	public String getExceptionText() {
		return exceptionText;
	}
	public void setExceptionText(String exceptionText) {
		this.exceptionText = exceptionText;
	}
	public Integer getExceptionType() {
		return exceptionType;
	}
	public void setExceptionType(Integer exceptionType) {
		this.exceptionType = exceptionType;
	}
	public Long getFlightBillId() {
		return flightBillId;
	}
	public void setFlightBillId(Long flightBillId) {
		this.flightBillId = flightBillId;
	}
	public String getExceptionCode() {
		return exceptionCode;
	}
	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}
	
	
}
