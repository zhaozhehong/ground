package com.cargo.addgoods.entity;

import java.util.Date;


/**
 * 
 * 
 * @author yuwei
 * @Description: 释放的容器数据
 * 2018年11月15日上午11:32:47
 */
public class LoadReleasContainer {
    //集装器释放id
    private Long releasecontainerId;

    //容器号
    private String containerNumber;

    //航班号
    private String flightNumber;

    //加货备注编码
    private String goodsCode;

    //加货备注名称
    private String goodsName;

    //所属人
    private String personal;

    //释放时间
    private Date releaseDate;

    //释放标识
    private String identify;

    //是否还原
    private Integer restored;//0是未还原的,1是已经还原的
    
    private String containerType;//容器类型
    
    private String container;//容器类型和容器拼接

    /**
     * 鑾峰彇集装器释放id
     *
     * @return RELEASECONTAINER_ID - 集装器释放id
     */
    public Long getReleasecontainerId() {
        return releasecontainerId;
    }

    /**
     * 璁剧疆集装器释放id
     *
     * @param releasecontainerId 集装器释放id
     */
    public void setReleasecontainerId(Long releasecontainerId) {
        this.releasecontainerId = releasecontainerId;
    }

    /**
     * 鑾峰彇容器号
     *
     * @return CONTAINER_NUMBER - 容器号
     */
    public String getContainerNumber() {
        return containerNumber;
    }

    /**
     * 璁剧疆容器号
     *
     * @param containerNumber 容器号
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * 鑾峰彇航班号
     *
     * @return FLIGHT_NUMBER - 航班号
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * 璁剧疆航班号
     *
     * @param flightNumber 航班号
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber == null ? null : flightNumber.trim();
    }

    /**
     * 鑾峰彇加货备注编码
     *
     * @return GOODS_CODE - 加货备注编码
     */
    public String getGoodsCode() {
        return goodsCode;
    }

    /**
     * 璁剧疆加货备注编码
     *
     * @param goodsCode 加货备注编码
     */
    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode == null ? null : goodsCode.trim();
    }

    /**
     * 鑾峰彇加货备注名称
     *
     * @return GOODS_NAME - 加货备注名称
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * 璁剧疆加货备注名称
     *
     * @param goodsName 加货备注名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName == null ? null : goodsName.trim();
    }

    /**
     * 鑾峰彇所属人
     *
     * @return PERSONAL - 所属人
     */
    public String getPersonal() {
        return personal;
    }

    /**
     * 璁剧疆所属人
     *
     * @param personal 所属人
     */
    public void setPersonal(String personal) {
        this.personal = personal == null ? null : personal.trim();
    }

    /**
     * 鑾峰彇释放时间
     *
     * @return RELEASE_DATE - 释放时间
     */
    public Date getReleaseDate() {
        return releaseDate;
    }

    /**
     * 璁剧疆释放时间
     *
     * @param releaseDate 释放时间
     */
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * 鑾峰彇释放标识
     *
     * @return IDENTIFY - 释放标识
     */
    public String getIdentify() {
        return identify;
    }

    /**
     * 璁剧疆释放标识
     *
     * @param identify 释放标识
     */
    public void setIdentify(String identify) {
        this.identify = identify == null ? null : identify.trim();
    }

    /**
     * 鑾峰彇是否还原
     *
     * @return RESTORED - 是否还原
     */
    public Integer getRestored() {
        return restored;
    }

    /**
     * 璁剧疆是否还原(0为未还原 1为已还原)
     *
     * @param restored 是否还原
     */
    public void setRestored(Integer restored) {
        this.restored = restored;
    }

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}
    
    
}