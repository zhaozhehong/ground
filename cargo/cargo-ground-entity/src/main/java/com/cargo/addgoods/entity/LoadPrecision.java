package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;


//精度范围
public class LoadPrecision implements Serializable{
	private static final long serialVersionUID = 1l;
	
	private Long precisionId;			//精度ID
	private String precisionName;		//名称
	private String precisionType;		//类型
	private Long precisionValue;		//值
	private String precisionComment;	//备注
	private String terminalCode;		//航站代码
	private String terminalName;		//航站名称
	private String register;			//登记人
	private Date registTime;			//登记时间
	private String createUser;
	private Date createTime;
	private String updateUser;
	private Date updateTime;
	private Integer isDeleted;
	public Long getPrecisionId() {
		return precisionId;
	}
	public void setPrecisionId(Long precisionId) {
		this.precisionId = precisionId;
	}
	public String getPrecisionName() {
		return precisionName;
	}
	public void setPrecisionName(String precisionName) {
		this.precisionName = precisionName;
	}
	public String getPrecisionType() {
		return precisionType;
	}
	public void setPrecisionType(String precisionType) {
		this.precisionType = precisionType;
	}
	public Long getPrecisionValue() {
		return precisionValue;
	}
	public void setPrecisionValue(Long precisionValue) {
		this.precisionValue = precisionValue;
	}
	public String getPrecisionComment() {
		return precisionComment;
	}
	public void setPrecisionComment(String precisionComment) {
		this.precisionComment = precisionComment;
	}
	public String getTerminalCode() {
		return terminalCode;
	}
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	public String getTerminalName() {
		return terminalName;
	}
	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}
	public String getRegister() {
		return register;
	}
	public void setRegister(String register) {
		this.register = register;
	}
	public Date getRegistTime() {
		return registTime;
	}
	public void setRegistTime(Date registTime) {
		this.registTime = registTime;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	@Override
	public String toString() {
		return "LoadPrecision [precisionId=" + precisionId + ", precisionName=" + precisionName + ", precisionType="
				+ precisionType + ", precisionValue=" + precisionValue + ", precisionComment=" + precisionComment
				+ ", terminalCode=" + terminalCode + ", terminalName=" + terminalName + ", register=" + register
				+ ", registTime=" + registTime + ", createUser=" + createUser + ", createTime=" + createTime
				+ ", updateUser=" + updateUser + ", updateTime=" + updateTime + ", isDeleted=" + isDeleted + "]";
	}
	
	
	
	
	
	
}
