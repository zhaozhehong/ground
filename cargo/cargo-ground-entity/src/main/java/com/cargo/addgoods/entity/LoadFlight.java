package com.cargo.addgoods.entity;

import java.io.Serializable;
/**
 * 航班动态表
 */

import java.util.Date;
public class LoadFlight implements Serializable {

	private static final long serialVersionUID = 1L;
	

    /**
     * 航班动态id
     */
    private Long flightId;
	
	 /**
     * 加货结束时间
     */
    private Date goodsDate;
    /**
     * 实达时间
     */
    private Date actualArriveDate;
    /**
     * 预达时间
     */
    private Date prepareArriveDate;
    /**
     * 计达时间
     */
    private Date planArriveDate;
    /**
     * 备降
     */
    private String flightPrepare;
    /**
     * 取消
     */
    private String flightCancel;
    /**
     * 延误
     */
    private String flightDelay;
    /**
     * 航班性质(国航还是深航)
     */
    private Integer flightProperty;
    /**
     * 结载时间
     */
    private Date loadDate;
    /**
     * 计划降落时间
     */
    private Date landDate;
    /**
     * 机位
     */
    private String seat;
    /**
     * 状态(正常,临时取消,延误,起飞,备降,返航,到达,取消)
     */
    private Integer status;
    /**
     * 实飞时间
     */
    private Date actualDate;
    /**
     * 预飞时间
     */
    private Date prepareDate;
    /**
     * 起始站
     */
    private String startPort;
    /**
     * 目的站
     */
    private String end;
    /**
     * 飞机号
     */
    private String airNumber;
    /**
     * 机型
     */
    private String flightType;
    /**
     * VIP
     */
    private String vip;
    /**
     * 承运人
     */
    private String freighter;
    /**
     * 计飞时间
     */
    private Date planDate;
    /**
     * 航班号
     */
    private String flightNumber;
    /**
     * 航班日期
     */
    private Date flightDate;
	public Long getFlightId() {
		return flightId;
	}
	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}
	public Date getGoodsDate() {
		return goodsDate;
	}
	public void setGoodsDate(Date goodsDate) {
		this.goodsDate = goodsDate;
	}
	public Date getActualArriveDate() {
		return actualArriveDate;
	}
	public void setActualArriveDate(Date actualArriveDate) {
		this.actualArriveDate = actualArriveDate;
	}
	public Date getPrepareArriveDate() {
		return prepareArriveDate;
	}
	public void setPrepareArriveDate(Date prepareArriveDate) {
		this.prepareArriveDate = prepareArriveDate;
	}
	public Date getPlanArriveDate() {
		return planArriveDate;
	}
	public void setPlanArriveDate(Date planArriveDate) {
		this.planArriveDate = planArriveDate;
	}
	public String getFlightPrepare() {
		return flightPrepare;
	}
	public void setFlightPrepare(String flightPrepare) {
		this.flightPrepare = flightPrepare;
	}
	public String getFlightCancel() {
		return flightCancel;
	}
	public void setFlightCancel(String flightCancel) {
		this.flightCancel = flightCancel;
	}
	public String getFlightDelay() {
		return flightDelay;
	}
	public void setFlightDelay(String flightDelay) {
		this.flightDelay = flightDelay;
	}
	public Integer getFlightProperty() {
		return flightProperty;
	}
	public void setFlightProperty(Integer flightProperty) {
		this.flightProperty = flightProperty;
	}
	public Date getLoadDate() {
		return loadDate;
	}
	public void setLoadDate(Date loadDate) {
		this.loadDate = loadDate;
	}
	public Date getLandDate() {
		return landDate;
	}
	public void setLandDate(Date landDate) {
		this.landDate = landDate;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getActualDate() {
		return actualDate;
	}
	public void setActualDate(Date actualDate) {
		this.actualDate = actualDate;
	}
	public Date getPrepareDate() {
		return prepareDate;
	}
	public void setPrepareDate(Date prepareDate) {
		this.prepareDate = prepareDate;
	}
	public String getStartPort() {
		return startPort;
	}
	public void setStartPort(String startPort) {
		this.startPort = startPort;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getAirNumber() {
		return airNumber;
	}
	public void setAirNumber(String airNumber) {
		this.airNumber = airNumber;
	}
	public String getFlightType() {
		return flightType;
	}
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}
	public String getVip() {
		return vip;
	}
	public void setVip(String vip) {
		this.vip = vip;
	}
	public Date getPlanDate() {
		return planDate;
	}
	public void setPlanDate(Date planDate) {
		this.planDate = planDate;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public Date getFlightDate() {
		return flightDate;
	}
	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}
	
	public String getFreighter() {
		return freighter;
	}
	public void setFreighter(String freighter) {
		this.freighter = freighter;
	}
	
	public LoadFlight() {
		super();
	}
	public LoadFlight(Long flightId, Date goodsDate, Date actualArriveDate, Date prepareArriveDate,
			Date planArriveDate, String flightPrepare, String flightCancel, String flightDelay, Integer flightProperty,
			Date loadDate, Date landDate, String seat, Integer status, Date actualDate, Date prepareDate,
			String startPort, String end, String airNumber, String flightType, String vip, String freighter,
			Date planDate, String flightNumber, Date flightDate) {
		super();
		this.flightId = flightId;
		this.goodsDate = goodsDate;
		this.actualArriveDate = actualArriveDate;
		this.prepareArriveDate = prepareArriveDate;
		this.planArriveDate = planArriveDate;
		this.flightPrepare = flightPrepare;
		this.flightCancel = flightCancel;
		this.flightDelay = flightDelay;
		this.flightProperty = flightProperty;
		this.loadDate = loadDate;
		this.landDate = landDate;
		this.seat = seat;
		this.status = status;
		this.actualDate = actualDate;
		this.prepareDate = prepareDate;
		this.startPort = startPort;
		this.end = end;
		this.airNumber = airNumber;
		this.flightType = flightType;
		this.vip = vip;
		this.freighter = freighter;
		this.planDate = planDate;
		this.flightNumber = flightNumber;
		this.flightDate = flightDate;
	}
	@Override
	public String toString() {
		return "TbLoadFlight [flightId=" + flightId + ", goodsDate=" + goodsDate + ", actualArriveDate="
				+ actualArriveDate + ", prepareArriveDate=" + prepareArriveDate + ", planArriveDate=" + planArriveDate
				+ ", flightPrepare=" + flightPrepare + ", flightCancel=" + flightCancel + ", flightDelay=" + flightDelay
				+ ", flightProperty=" + flightProperty + ", loadDate=" + loadDate + ", landDate=" + landDate + ", seat="
				+ seat + ", status=" + status + ", actualDate=" + actualDate + ", prepareDate=" + prepareDate
				+ ", startPort=" + startPort + ", end=" + end + ", airNumber=" + airNumber + ", flightType="
				+ flightType + ", vip=" + vip + ", freighter=" + freighter + ", planDate=" + planDate
				+ ", flightNumber=" + flightNumber + ", flightDate=" + flightDate + "]";
	}
	
    
    
	
	
}
