package com.cargo.addgoods.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import cn.afterturn.easypoi.excel.annotation.Excel;
/**
 * 机口排班表
 */
public class LoadArrange implements Serializable {

	private static final long serialVersionUID = 1L;
	 /**
     * 排班id
     */
    private Long arrangeId;
    /** 
    * 机口
    */
   @Excel(name = "机口", orderNum = "0")
   private String machine;
    /**
     * 航班号
     */
    @Excel(name = "航班号", orderNum = "1")
    private String flightNumber;
    /**
     * 目的站
     */
    @Excel(name = "目的站", orderNum = "2")
    private String endPort;
    
    /**
     * 起飞时间
     */
    @Excel(name = "起飞时间", importFormat = "yyyy-MM-dd HH:mm:ss" ,  orderNum = "3")
    private Date flyDate;
    
    /**
     * 机型
     */
    @Excel(name = "机型", orderNum = "4")
    private String flightType;
   
    /**
     * 结载时间
     */
    @Excel(name = "结载时间", importFormat = "yyyy-MM-dd HH:mm:ss" ,  orderNum = "5")
    private Date loadDate;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 是否删除
     */
    private Integer isDeleted;//0不删除 1删除


	public Long getArrangeId() {
		return arrangeId;
	}


	public void setArrangeId(Long arrangeId) {
		this.arrangeId = arrangeId;
	}


	public String getFlightNumber() {
		return flightNumber;
	}


	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}


	public String getMachine() {
		return machine;
	}


	public void setMachine(String machine) {
		this.machine = machine;
	}


	public String getEndPort() {
		return endPort;
	}


	public void setEndPort(String endPort) {
		this.endPort = endPort;
	}


	public String getFlightType() {
		return flightType;
	}


	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public Date getFlyDate() {
		return flyDate;
	}

	public void setFlyDate(Date flyDate) {
		this.flyDate = flyDate;
	}


	public Date getLoadDate() {
		return loadDate;
	}


	public void setLoadDate(Date loadDate) {
		this.loadDate = loadDate;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public Date getUpdateTime() {
		return updateTime;
	}


	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public Integer getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public LoadArrange() {
		super();
	}


	public LoadArrange(Long arrangeId, String flightNumber, String machine, String endPort, String flightType,
			Date flyDate, Date loadDate, Date createTime, Date updateTime, String createUser, String updateUser,
			Integer isDeleted) {
		super();
		this.arrangeId = arrangeId;
		this.flightNumber = flightNumber;
		this.machine = machine;
		this.endPort = endPort;
		this.flightType = flightType;
		this.flyDate = flyDate;
		this.loadDate = loadDate;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.createUser = createUser;
		this.updateUser = updateUser;
		this.isDeleted = isDeleted;
	}


	@Override
	public String toString() {
		return "TbLoadArrange [arrangeId=" + arrangeId + ", flightNumber=" + flightNumber + ", machine=" + machine
				+ ", endPort=" + endPort + ", flightType=" + flightType + ", flyDate=" + flyDate + ", loadDate="
				+ loadDate + ", createTime=" + createTime + ", updateTime=" + updateTime + ", createUser=" + createUser
				+ ", updateUser=" + updateUser + ", isDeleted=" + isDeleted + "]";
	}



	

	
}