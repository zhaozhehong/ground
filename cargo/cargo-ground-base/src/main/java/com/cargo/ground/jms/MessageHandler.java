package com.cargo.ground.jms;

import java.math.BigInteger;

import javax.jms.Message;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.log4j.spi.LoggingEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**地面系统项目
 * @Description 
 * @author kj_xiaoyifei
 * @date 2018年10月26日
**/

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
//
//import com.cargo.ground.entity.SysLogEntity;
//import com.cargo.ground.sevice.SysLogService;
import com.cargo.ground.util.StringDateUtil;
/**
 * 接收处理消息
 */
@Component
public class MessageHandler {
	private static final Logger logger = LoggerFactory
			.getLogger(MessageHandler.class);
//	@Autowired
//	private  SysLogService   sysLogService;
	@JmsListener(destination="queue-1")
	public void recieve1(String message) {
		System.out.println("recieve1###################" + message + "###################");
	}
	
	@JmsListener(destination="topic-1", containerFactory="topicListenerContainer")
	public void recieve2(String message) {
		System.out.println("recieve2###################" + message + "###################");
	}
	

	
//	@JmsListener(destination="queue-2")
//	public void recieve3(String message) {
//		System.out.println("recieve3###################" + message + "###################");
//	}
	
//	@JmsListener(destination="topic-2", containerFactory="topicListenerContainer")
//	public void recieve4(String message) {
//		System.out.println("recieve4###################" + message + "###################");
//	}
//	@JmsListener(destination="ListenerLog", containerFactory="topicListenerContainer")
//	  public void onMessage(Message message) {
//	        try {
//	        	System.out.println("recieve3###################" + message + "###################");
//	            // receive log event in your consumer
// 	            LoggingEvent event = (LoggingEvent)((ActiveMQObjectMessage)message).getObject();
//	            System.out.println("Logging project: [" + event.getLevel() + "]: "+ event.getMessage());
//	            
//	            String producecerId=message.getJMSMessageID();
//	            long eeee=message.getJMSTimestamp();
//	          String datetiem=  StringDateUtil.timeStamp2Date(String.valueOf(eeee).substring(0, 10), "");
//	           String  levet= event.getLevel().toString();
//	           String  messages= event.getMessage()+"";
//	           String  logname=event.getLoggerName();     
//	           
//	           System.out.println("producecerId=" +producecerId + ",jMSTimestamp= "+ datetiem+ ",levet= "+ levet+ ",messages= "+ messages+ ",logname= "+ logname);
//	           System.out.println("Logging project: [" + event.getLevel() + "]: "+ event.getMessage());
//	            
//	        } catch (Exception e) {
//	            e.printStackTrace();
//	        }
//	    }
	
//	@JmsListener(destination="ListenerLog", containerFactory="topicListenerContainer")
//	  public void onMessage(Message message) {
//	        try {
//	        	System.out.println("recieve3###################" + message + "###################");
//	            // receive log event in your consumer
//	            LoggingEvent event = (LoggingEvent)((ActiveMQObjectMessage)message).getObject();
//	            System.out.println("Logging project: [" + event.getLevel() + "]: "+ event.getMessage());
//	            
//	            String producecerId=message.getJMSMessageID();
//	            long eeee=message.getJMSTimestamp();
//	          String datetiem=  StringDateUtil.timeStamp2Date(String.valueOf(eeee).substring(0, 10), "");
//	           String  levet= event.getLevel().toString();
//	           String  messages= event.getMessage()+"";
//	           String  logname=event.getLoggerName(); 
//	           
////	           SysLogEntity  sysLogEntity=new SysLogEntity();
////	           sysLogEntity.setLogId(BigInteger.valueOf(System.currentTimeMillis()));
////	           sysLogEntity.setLevel(event.getLevel().toString());
////	           sysLogEntity.setCreateTimestamp(System.currentTimeMillis());
////	           sysLogEntity.setLogname(event.getLoggerName());
////	           sysLogEntity.setLogDateTime(StringDateUtil.timeStamp2Date(String.valueOf(eeee).substring(0, 10), ""));
////	           sysLogEntity.setMessages(event.getMessage().toString());
////	           sysLogEntity.setServerId(message.getJMSMessageID());
////	           sysLogService.save(sysLogEntity);
////	           
//	           System.out.println("producecerId=" +producecerId + ",jMSTimestamp= "+ datetiem+ ",levet= "+ levet+ ",messages= "+ messages+ ",logname= "+ logname);
//	           System.out.println("Logging project: [" + event.getLevel() + "]: "+ event.getMessage());
//	            
//	        } catch (Exception e) {
//	        	logger.error(e.getMessage());
//	        }
//	    }
	  
}