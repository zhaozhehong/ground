package com.cargo.ground.common.exception;

import com.cargo.ground.util.ResultMsg.CodeEnum;

public class BpmException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CodeEnum code = CodeEnum.ERROR;

	public BpmException() {
		super();
	}

	 
	public BpmException(String message) {
		super(message);
	}

	 
	public BpmException(String message, Throwable cause) {
		super(message, cause);
	}

	 
	public BpmException(Throwable cause) {
		super(cause);
	}

	 
	protected BpmException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		 
		super(message, cause, enableSuppression, writableStackTrace);
	}


	public CodeEnum getCode() {
		return code;
	}


	public void setCode(CodeEnum code) {
		this.code = code;
	}

	
}
