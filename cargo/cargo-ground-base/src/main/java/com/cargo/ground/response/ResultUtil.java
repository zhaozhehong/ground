package com.cargo.ground.response;

import com.cargo.ground.enums.ExceptionEnum;

/**
 * @ClassName: ResultUtil
 * @Description: 封装返回值工具类
 * @author: zengjianwen
 * @date: 2018年11月16日 上午11:26:23
 */
public class ResultUtil {
	/**
	 * @Title: success
	 * @Description: 该方法可以用于增删改操作正确时 需要返回结果时调用
	 * @return
	 * @return: Result<T>
	 */
	public static <T> Result<T> success(Object data) {
		return new Result<T>(0, "SUCCESS", data);
	}
	/**
	 * @Title: success
	 * @Description: 该方法可以用于增删改操作正确 不需要返回结果时调用
	 * @return
	 * @return: Result<T>
	 */
	public static <T> Result<T> success() {
		return new Result<T>(0, "SUCCESS");
	}
	/**
	 * @Title: error
	 * @Description: 该方法可以用于增删改操作异常 需要返回结果时调用
	 * @param msg
	 * @return
	 * @return: Result<T>
	 */
	public static <T> Result<T> error(String msg) {
		return new Result<T>(1, msg);
	}
	public static <T> Result<T> error(Integer code, String msg) {
		return new Result<T>(code, msg);
	}
	/**
	 * @Title: error
	 * @Description: 该方法可以用于增删改操作异常，定义并返回异常信息时调用
	 * @param exceptionEnum
	 * @return
	 * @return: Result<T>
	 */
	public static <T> Result<T> error(ExceptionEnum exceptionEnum) {
		return new Result<T>(exceptionEnum.getCode(), exceptionEnum.getMsg());
	}
	public static <T> Result<T> result(Integer resultCode, String resultMsg, Object result) {
		return new Result<T>(resultCode, resultMsg, result);
	}
}
