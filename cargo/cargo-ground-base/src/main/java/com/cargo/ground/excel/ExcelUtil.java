package com.cargo.ground.excel;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.web.multipart.MultipartFile;

import com.cargo.ground.enums.ExceptionEnum;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.ground.tool.WDWUtil;

/**
 * @ClassName: ExcelUtil
 * @Description: Excel转换工具类
 * @author: zengjianwen
 * @date: 2018年11月20日 下午2:48:01
 */
public class ExcelUtil {
    public static final String OFFICE_EXCEL_2003_POSTFIX = "xls";
    public static final String OFFICE_EXCEL_2010_POSTFIX = "xlsx";

    public static final String EMPTY = "";
    public static final String POINT = ".";
    public static final String LIB_PATH = "lib";
    public static final String STUDENT_INFO_XLS_PATH = LIB_PATH + "/student_info" + POINT + OFFICE_EXCEL_2003_POSTFIX;
    public static final String STUDENT_INFO_XLSX_PATH = LIB_PATH + "/student_info" + POINT + OFFICE_EXCEL_2010_POSTFIX;
    public static final String NOT_EXCEL_FILE = " : Not the Excel file!";
    public static final String PROCESSING = "Processing...";

    public List<?> readExcel(String path) throws IOException {
        return readXls(path);
    }

    public List<?> readXls(String path) throws IOException {
        System.out.println(this.PROCESSING + path);
        InputStream is = new FileInputStream(path);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);

        // Student student = null;
        List<?> list = new ArrayList();
        // Read the Sheet
        for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
            HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
            if (hssfSheet == null) {
                continue;
            }
            // Read the Row
            for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow != null && hssfRow.getCell(0) != null) {
                    // student = new Student();
                    HSSFCell no = hssfRow.getCell(0);
                    HSSFCell name = hssfRow.getCell(1);
                    HSSFCell age = hssfRow.getCell(2);
                    HSSFCell score = hssfRow.getCell(3);
                    System.out.println(rowNum);
                    System.out.println(getValue(no));
                    System.out.println(getValue(name));
                    System.out.println(getValue(age));
                    System.out.println(getValue(score));
                    // student.setNo(getValue(no));
                    // student.setName(getValue(name));
                    // student.setAge(getValue(age));
                    // student.setScore(Float.valueOf(getValue(score)));
                    // list.add(student);
                }
            }
        }
        return list;
    }

    public static String getValue(HSSFCell hssfCell) {
        if (hssfCell.getCellType() == hssfCell.CELL_TYPE_BOOLEAN) {
            return String.valueOf(hssfCell.getBooleanCellValue()).trim();
        } else if (hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) {
            return String.valueOf(hssfCell.getNumericCellValue()).trim();
        } else {
            return String.valueOf(hssfCell.getStringCellValue()).trim();
        }
    }

    public static String getStringValue(HSSFCell c) {
        if (c == null) {
            return "";
        } else {
            return c.getStringCellValue().trim();
        }
    }

    public static void ListToExcel(List<String> headers, List<List> contents, OutputStream out, String sheetName) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(sheetName);
//        workbook.setSheetName(0, sheetName);
        try {
            HSSFCell cell;
            HSSFRow row;
            sheet.autoSizeColumn(1, true);
            sheet.setDefaultColumnWidth(20);
            HSSFCellStyle style = workbook.createCellStyle();// 创建一个单元的样式
            // 背景色的设定
            style.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 设置水平居中
            style.setVerticalAlignment(HSSFCellStyle.ALIGN_CENTER);// 上下居中
            HSSFFont font = workbook.createFont();
            font.setFontName("黑体");
            font.setFontHeightInPoints((short) 22);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            //第一行為列头
            int k = 0;
            row = sheet.createRow(k);
            k++;
            for (int i = 0; i < headers.size(); i++) {
                cell = row.createCell(i);
                cell.setCellValue(headers.get(i));
                cell.setCellStyle(style);
            }
            List list;
            for (int i = 0; i < contents.size(); i++) {
                list = contents.get(i);
                row = sheet.createRow(k + i);
                for (int j = 0; j < list.size(); j++) {
                    cell = row.createCell(j);
                    cell.setCellStyle(style);
                    cell.setCellValue(list.get(j) == null ? "" : list.get(j).toString());
                }
            }
            workbook.write(out);
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void ListToExcel(List<String> headers0, List<List> contents0, List<String> headers1, List<List> contents1, OutputStream out, String sheetName) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(sheetName);
//         workbook.setSheetName(0, sheetName);
        try {
            HSSFCell cell;
            HSSFRow row;
            sheet.autoSizeColumn(1, true);
            sheet.setDefaultColumnWidth(20);
            HSSFCellStyle style = workbook.createCellStyle();// 创建一个单元的样式
            // 背景色的设定
            style.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 设置水平居中
            style.setVerticalAlignment(HSSFCellStyle.ALIGN_CENTER);// 上下居中
            HSSFFont font = workbook.createFont();
            font.setFontName("黑体");
            font.setFontHeightInPoints((short) 22);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            //第一行為列头
            int k = 0;
            row = sheet.createRow(k);
            k++;
            for (int i = 0; i < headers0.size(); i++) {
                cell = row.createCell(i);
                cell.setCellValue(headers0.get(i));
                cell.setCellStyle(style);
            }
            List list;
            for (int i = 0; i < contents0.size(); i++) {
                list = contents0.get(i);
                row = sheet.createRow(k + i);
                for (int j = 0; j < list.size(); j++) {
                    cell = row.createCell(j);
                    cell.setCellStyle(style);
                    cell.setCellValue(list.get(j) == null ? "" : list.get(j).toString());
                }
            }
            if (headers1.size() > 1) {
                k = 2;
                row = sheet.createRow(2);
                k++;
            }
            for (int i = 0; i < headers1.size(); i++) {
                cell = row.createCell(i);
                cell.setCellValue(headers1.get(i));
                cell.setCellStyle(style);
            }
            for (int i = 0; i < contents1.size(); i++) {
                list = contents1.get(i);
                row = sheet.createRow(k + i);
                for (int j = 0; j < list.size(); j++) {
                    cell = row.createCell(j);
                    cell.setCellStyle(style);
                    cell.setCellValue(list.get(j) == null ? "" : list.get(j).toString());
                }
            }
            workbook.write(out);
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
	// ================
	public static boolean isExcel2003(String filePath) {
		return filePath.matches("^.+\\.(?i)(xls)$");
	}
	public static boolean isExcel2007(String filePath) {
		return filePath.matches("^.+\\.(?i)(xlsx)$");
	}
	public static boolean validateExcel(String filePath) {
		if (filePath == null || !(WDWUtil.isExcel2007(filePath))) {
			return false;
		}
		return true;
	}
	/**
	 * @Title: checkExcel
	 * @Description: 校验Excel
	 * @param file
	 * @return
	 * @return: Result<String>
	 */
	public static Result<String> checkExcel(MultipartFile file) {
		if (file == null) {
			return ResultUtil.error(ExceptionEnum.CODE_800004);
		}
		String fileName = file.getOriginalFilename();
		// 判断文件大小、即名称
		long size = file.getSize();
		if (StringUtils.isBlank(fileName) || size == 0) {
			return ResultUtil.error(ExceptionEnum.CODE_800005);
		}
		if (size > (5 * 1024 * 1024)) {
			return ResultUtil.error(ExceptionEnum.CODE_800006);
		}
		if (!validateExcel(fileName)) {
			return ResultUtil.error(ExceptionEnum.CODE_800007);
		}
		return ResultUtil.success();
	}
}
