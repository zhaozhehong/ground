package com.cargo.ground.common.log;

import java.util.Date;

import javax.annotation.Resource;
import javax.jms.Destination;
import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Method;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.cargo.ground.config.log.OperaLog;
import com.cargo.ground.entity.log.OperateLogEntity;
import com.cargo.ground.entity.user.User;

/**
 * 地面系统项目
 * 
 * @Description
 * @author kj_xiaoyifei
 * @date 2018年10月31日
 **/

@Aspect
@Component
public class OperaLogAspect {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Resource(name = "topicLog")
	private Destination topicLog;

	@Autowired
	private HttpServletRequest request;

	// Controller层切点
	@Pointcut("@annotation(com.cargo.ground.config.log.OperaLog)")
	public void controllerAspect() {
	}

	@After(value = "controllerAspect()")
	public void doAfter(JoinPoint joinPoint) {
		// 这里获取request；千万不要写在下边的线程里，因为得不到
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		System.out.println(request.getAttribute("message") + "=======");

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					jmsTemplate.convertAndSend(topicLog, getControllerMethodDescription(joinPoint));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

//	// 可以得到方法return的值
//	@AfterReturning(returning = "ret", pointcut = "controllerAspect()")
//	public void doAfterReturning(Object ret) throws Throwable {
//		// 处理完请求，返回内容
//		System.out.println(ret);
//		Map<String, Object> map = (Map<String, Object>) ret;
//		System.out.println(map.get("total"));
//	}

	// 通过反射获取参入的参数
	public static OperateLogEntity getControllerMethodDescription(JoinPoint joinPoint) throws Exception {
		OperateLogEntity operateLogEntity = new OperateLogEntity();
		
		
		String targetName = joinPoint.getTarget().getClass().getName();
		String methodName = joinPoint.getSignature().getName();
		Object[] arguments = joinPoint.getArgs();
		Class targetClass = Class.forName(targetName);
		String description = "";

		Method[] methods = targetClass.getMethods();
		for (Method method : methods) {
			if (method.getName().equals(methodName)) {
				Class[] clazzs = method.getParameterTypes();

				if (clazzs.length == arguments.length) {
					description = method.getAnnotation(OperaLog.class).description();
					break;
				}
			}
		}

		operateLogEntity.setOperateType(methodName); //0:ADD,1:UPDATE.2:DELETE,3:QUERY,4 其他
		operateLogEntity.setOperateDesc(description); //日志信息
		operateLogEntity.setOperateModelNm(targetName); //操作模块
		operateLogEntity.setOperateSql(""); //操作SQL
		operateLogEntity.setOperateTime(new Date()); //产生时间戳
		
		//先测试写死
		operateLogEntity.setOperator("kjkjjk"); //操作人
		return operateLogEntity;
	}
}