package com.cargo.ground.entity.log;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class OperateLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private BigInteger  logId; 
 
	private String operateType; //0:ADD,1:UPDATE.2:DELETE,3:QUERY,4 其他
	private String operateDesc; //日志信息
	private String operateModelNm; //操作模块
	private String operateSql; //操作SQL
	private Date  operateTime; //产生时间戳
	private String operator; //操作人
	public OperateLogEntity() {
		super();
	}
	/**
	 * @return the logId
	 */
	public BigInteger getLogId() {
		return logId;
	}
	/**
	 * @param logId the logId to set
	 */
	public void setLogId(BigInteger logId) {
		this.logId = logId;
	}
	/**
	 * @return the operateType
	 */
	public String getOperateType() {
		return operateType;
	}
	/**
	 * @param operateType the operateType to set
	 */
	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	/**
	 * @return the operateDesc
	 */
	public String getOperateDesc() {
		return operateDesc;
	}
	/**
	 * @param operateDesc the operateDesc to set
	 */
	public void setOperateDesc(String operateDesc) {
		this.operateDesc = operateDesc;
	}
	 
	/**
	 * @return the operateSql
	 */
	public String getOperateSql() {
		return operateSql;
	}
	/**
	 * @param operateSql the operateSql to set
	 */
	public void setOperateSql(String operateSql) {
		this.operateSql = operateSql;
	}
	 
	/**
	 * @return the operateModelNm
	 */
	public String getOperateModelNm() {
		return operateModelNm;
	}
	/**
	 * @param operateModelNm the operateModelNm to set
	 */
	public void setOperateModelNm(String operateModelNm) {
		this.operateModelNm = operateModelNm;
	}
	/**
	 * @return the operateTime
	 */
	public Date getOperateTime() {
		return operateTime;
	}
	/**
	 * @param operateTime the operateTime to set
	 */
	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}
	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}
	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}
	 
 

}