package com.cargo.ground.util.wBridge;

/**
 * @Desc 常量
 * @LastPeson xuxu
 **/
public class Constants {
    /**
     * udp ip 地址
     */
    public static final String UDP_IP = "192.168.125.112";
    /**
     * udp 端口
     */
    public static final Integer UDP_PORT = 1026;

    /**
     * 16进制中要使用到的字母必须写
     **/
    public static final String str16 = "0123456789abcdef";

    /**
     * 服务端接收到数据的字节数组大小
     */
    public static final int size = 1024;

    /**
     * 除皮指令
     */
    public static final String REMOVE_TARE = "02 30 30 31 2c 30 30 30 30 30 30 30 30 30 30 2c 30 31 2c 30 30 2c 30 30 2c 31 43 03";
    /**
     * 除皮返回格式
     */
    public static final String[] REMOVE_TARE_DESC = {"起始符秤地址",
            "秤状态(eg:重量稳定等待上板卡)","流水号","命令码", "保留位","错误码(eg:无错误)",
            "总重(A)*","皮重(C)*","总货量(B)*","单号(D)*","件数(F)*","重量(E)*","校验和结束符"};

    /***
     * 置零指令
     */
    public static final String RESET = "02 30 30 31 2c 30 30 30 30 30 30 30 30 30 30 2c 30 32 2c 30 30 2c 30 30 2c 31 46 03";
    public static final String[] RRESET_DESC ={"起始符秤地址",
            "秤状态(eg:重量稳定等待上板卡)","流水号","命令码", "保留位","错误码(eg:无错误)",
            "总重(A)*","皮重(C)*","总货量(B)*","单号(D)*","件数(F)*","重量(E)*","校验和结束符"};

    /**
     * 取仪表数据指令
     */
    public static final String READ_INFO = "02 30 30 31 2c 31 32 33 34 35 31 32 33 34 35 2c 30 33 2c 30 30 2c 30 30 2c 31 45 03";
    public static final String[] READ_INFO_DESC ={"起始符秤地址",
            "秤状态(eg:重量稳定等待上板卡)","流水号","命令码", "保留位","错误码(eg:无错误)",
            "总重(A)*","皮重(C)*","总货量(B)*","单号(D)*","件数(F)*","重量(E)*","校验和结束符"};


    /**
     * 修改单号指令
     */
    public static final String UPDATE_ORDER = "02 30 30 31 2c 31 32 33 34 30 31 32 33 34 30 2c 30 34 2c 30 30 2c 30 30 2c 31 32 32 33 33 2c 30 34 03";
    public static final String[] UPDATE_ORDER_DESC ={"起始符秤地址",
            "秤状态(eg:重量稳定等待上板卡)","流水号","命令码", "保留位","错误码(修改成功，错误码返回0x30，0x30，失败返回0x30，0x31等其它值)",
            "校验和结束符"};

    /**
     * 修改件数指令
     */
    public static final String UPDATE_NUM = "02 30 30 31 2c 31 32 33 34 33 31 32 33 34 33 2c 30 35 2c 30 30 2c 30 30 2c 30 32 32 33 33 2c 30 34 03";
    public static final String[] UPDATE_NUM_DESC ={"起始符秤地址",
            "秤状态(eg:重量稳定等待上板卡)","流水号","命令码", "保留位","错误码(修改成功，错误码返回0x30，0x30，失败返回0x30，0x31等其它值)",
            "校验和结束符"};
    /**
     * 确认单号指令
     */
    public static final String SURE_NUM = "02 30 30 31 2c 30 30 30 30 30 30 30 30 30 30 2c 30 36 2c 30 30 2c 30 30 2c 31 42 03";

}
