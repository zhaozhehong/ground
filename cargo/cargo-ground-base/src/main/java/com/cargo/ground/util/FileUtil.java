package com.cargo.ground.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hpsf.NoFormatIDException;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.NestedIOException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.excel.export.styler.ExcelExportStylerColorImpl;
//@Component
public class FileUtil {
/*
 * title:excel标题
 * sheetName：sheet名字
 * fileName：excel导出名字
 */
	public static void exportExcel(List<?> list, String title, String sheetName, Class<?> pojoClass,String fileName,boolean isCreateHeader, HttpServletResponse response,HttpServletRequest request){
        ExportParams exportParams = new ExportParams(title, sheetName);
        exportParams.setCreateHeadRows(isCreateHeader);
        exportParams.setHeaderColor(HSSFColor.BLUE.index);
        defaultExport(list, pojoClass, fileName, response,request, exportParams);

    }
    public static void exportExcel(List<?> list, String title, String sheetName, Class<?> pojoClass,String fileName, HttpServletResponse response, HttpServletRequest request){
    	 ExportParams exportParams = new ExportParams(title, sheetName,ExcelType.HSSF);
//         exportParams.setHeaderColor(HSSFColor.RED.index);
//         exportParams.setColor(HSSFColor.BLUE.index);
        
    	defaultExport(list, pojoClass, fileName, response,request,exportParams);
    }
    public static void exportExcel(List<Map<String, Object>> list, String fileName, HttpServletResponse response,HttpServletRequest request){
        defaultExport(list, fileName, response,request);
    }

    private static void defaultExport(List<?> list, Class<?> pojoClass, String fileName, HttpServletResponse response,HttpServletRequest request, ExportParams exportParams) {
       
    	 exportParams.setCreateHeadRows(true);
         
         exportParams.setHeaderColor(HSSFColor.RED.index);
         
          exportParams.setColor(HSSFColor.BLUE.index);
         
         exportParams.setStyle(ExcelExportStylerColorImpl.class);
    	
    	Workbook workbook = ExcelExportUtil.exportExcel(exportParams,pojoClass,list);
    	
        if (workbook != null);
        downLoadExcel(fileName, response, request,workbook);
    }

    private static void downLoadExcel(String fileName, HttpServletResponse response,HttpServletRequest request, Workbook workbook) {
        try {
        	  SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
              String now = sdf.format(new Date());//加时间戳
              fileName = fileName + now + ".xls";
     //         final String userAgent = request.getHeader("USER-AGENT");
//              String finalFileName = null;
//              if (userAgent.contains("Mozilla") || userAgent.contains("Chrome")) {//google,火狐浏览器
//                  finalFileName = new String(fileName.getBytes(), "ISO8859-1");
//              } else {
//                  finalFileName = URLEncoder.encode(fileName, "UTF8");//其他浏览器
//              }
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            workbook.write(response.getOutputStream());
           
        } catch (IOException e) {
            throw new NoFormatIDException(e.getMessage());
        }
    }
   private static void defaultExport(List<Map<String, Object>> list, String fileName, HttpServletResponse response,HttpServletRequest  request) {
        Workbook workbook = ExcelExportUtil.exportExcel(list, ExcelType.HSSF);
        if (workbook != null);
        downLoadExcel(fileName, response,request, workbook);
    }

    public static <T> List<T> importExcel(String filePath,Integer titleRows,Integer headerRows, Class<T> pojoClass) throws NestedIOException{
        if (StringUtils.isEmpty(filePath)){
            return null;
        }
        ImportParams params = new ImportParams();
        params.setTitleRows(titleRows);
        params.setHeadRows(headerRows);
        List<T> list = null;
        try {
            list = ExcelImportUtil.importExcel(new File(filePath), pojoClass, params);
        }catch (NoSuchElementException e){
            throw new NestedIOException("模板不能为空");
        } catch (Exception e) {
            e.printStackTrace();
            throw new NestedIOException(e.getMessage());
        }
        return list;
    }
    public static <T> List<T> importExcel(MultipartFile file, Integer titleRows, Integer headerRows, Class<T> pojoClass) throws NestedIOException{
        if (file == null){
            return null;
        }
        ImportParams params = new ImportParams();
        params.setTitleRows(titleRows);
        params.setHeadRows(headerRows);
        List<T> list = null;
        try {
            list = ExcelImportUtil.importExcel(file.getInputStream(), pojoClass, params);
        }catch (NoSuchElementException e){
            throw new NestedIOException("excel文件不能为空");
        } catch (Exception e) {
            throw new NestedIOException(e.getMessage());
        }
        return list;
    }
    
    /**
     * @param entity
     *            表格标题属性
     * @param pojoClass
     *            Excel对象Class
     * @param dataSet
     *            Excel对象数据List
     */
    public static void exportBigExcel(ExportParams entity, Class<?> pojoClass,Collection<?> dataSet) {
    	ExcelExportUtil.exportBigExcel(entity, pojoClass, dataSet);
    }

    public static void closeExportBigExcel() {
    	ExcelExportUtil.closeExportBigExcel();
    }
    
    public static void bigExport(HttpServletResponse response,Workbook workbook,String temppath ) throws FileNotFoundException, IOException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

		File savefile = new File(temppath);
		temppath=temppath+ sdf.format(new Date());
		File zip = new File(temppath+".zip");  //压缩文件路径  
		if (!savefile.exists()) {
			savefile.mkdirs();
		}
		FileOutputStream fos = null;
		String  path = temppath + ".xlsx";// 加时间戳

		try {
			
			fos = new FileOutputStream(path);
			workbook.write(fos);

			

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ExcelExportUtil.closeExportBigExcel();
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		List<String> fileNames = new ArrayList<String>();
		fileNames.add(path);
		exportZip(response, fileNames, zip);
	}

	/**
	 * 文件压缩并导出到客户端
	 * 
	 * @param outPut
	 * @param fileNames
	 * @param zip
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void exportZip(HttpServletResponse response, List<String> fileNames, File zip)
			throws FileNotFoundException, IOException {
		OutputStream outPut = response.getOutputStream();
		// 1.压缩文件
		File srcFile[] = new File[fileNames.size()];
		for (int i = 0; i < fileNames.size(); i++) {
			srcFile[i] = new File(fileNames.get(i));
		}
		byte[] byt = new byte[1024];
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip));
		for (int i = 0; i < srcFile.length; i++) {
			FileInputStream in = new FileInputStream(srcFile[i]);
			out.putNextEntry(new ZipEntry(srcFile[i].getName()));
			int length;
			while ((length = in.read(byt)) > 0) {
				out.write(byt, 0, length);
			}
			out.closeEntry();
			in.close();
		}
		out.close();

		// 2.删除服务器上的临时文件(excel)
		for (int i = 0; i < srcFile.length; i++) {
			File temFile = srcFile[i];
			if (temFile.exists() && temFile.isFile()) {
				temFile.delete();
			}
		}

		// 3.返回客户端压缩文件
		FileInputStream inStream = new FileInputStream(zip);
		byte[] buf = new byte[4096];
		int readLenght;
		while ((readLenght = inStream.read(buf)) != -1) {
			outPut.write(buf, 0, readLenght);
		}
		inStream.close();
		outPut.close();

		// 4.删除压缩文件
		if (zip.exists() && zip.isFile()) {
			zip.delete();
		}
	}

 
}
