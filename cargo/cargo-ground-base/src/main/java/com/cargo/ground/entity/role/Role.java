package com.cargo.ground.entity.role;

import com.cargo.ground.entity.BaseEntitys;
import com.cargo.ground.entity.resources.Resources;

import java.util.LinkedList;
import java.util.List;

/**
 * 角色实体类
 */
public class Role extends BaseEntitys {

    private static final long serialVersionUID = 1L;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色描述
     */
    private String roleDescription;
    /**
     * 角色类型
     */
    private String roleType;
    /**
     * 使用状态 0：可用  1：不可用
     */
    private Short enableFlag;
    /**
     * 最终修改用户
     */
    private String lastUpdateUser;
    /**
     * 资源集合
     */
    private List<Resources> resourceList = new LinkedList<Resources>();

    private String roleCode;

    public Role() {
    }

    public Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription == null ? null : roleDescription.trim();
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType == null ? null : roleType.trim();
    }

    public Short getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(Short enableFlag) {
        this.enableFlag = enableFlag;
    }

    public List<Resources> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<Resources> resourceList) {
        this.resourceList = resourceList;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

}