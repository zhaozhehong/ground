package com.cargo.ground.common.base;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: BaseService
 * @Description: service层基类公共接口
 * @author: zengjianwen
 * @date: 2018年11月5日 上午11:47:55
 * @param <T>
 * @param <ID>
 */
public interface BaseService<T> {
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	T selectById(Serializable id);
	/**
	 * 查询单条记录
	 * @param entity
	 * @return
	 */
	T selectOne(T obj);
	/**
	 * @Title: selectCount
	 * @Description: 查询记录数
	 * @param obj
	 * @return
	 * @return: Long
	 */
	Long selectCount(T obj);
	/**
	 * 查询记录集合
	 * @param entity
	 * @return
	 */
	List<T> selectList(T obj);
	/**
	 * 通用的保存方法
	 * @param <T>
	 * @param entity
	 */
	int save(T obj);
	/**
	 * 通用的修改方法
	 * @param <T>
	 * @param entity
	 */
	int update(T obj);
	/**
	 * 批量更新
	 * @param list
	 * @return
	 */
	int batchUpdate(List<?> list);
	/**
	 * 删除方法
	 * @param id
	 */
	int delById(Serializable id);
	/**
	 * 批量删除
	 * @param list
	 * @return
	 */
	int delList(List<?> list);
	/**
	 * 批量删除方法
	 * @param ids
	 */
	int delArray(Serializable[] ids);
}
