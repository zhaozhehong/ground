package com.cargo.ground.util;

import java.util.UUID;

public class UUIDUtil {

	/**
	 * 获取uuid唯一标识字符串
	 * @return
	 */
	public static String getKey(){
		return UUID.randomUUID().toString().replaceAll("-", "0").toUpperCase();
	}
	 
}
