package com.cargo.ground.util.wBridge.HandlesStrMsg;


import com.cargo.ground.util.wBridge.Connction;

/**
 * @Desc 处理udp 接收和发送,提供默认的处理方法和自定义处理接口
 * @LastPeson xuxu
 **/
public class CommonHandleBackMsg {

    //默认的处理方法
    public static String defaultHadle(String send){
        return Connction.sendAndGetMsg(send);
    }

    //默认的处理方法+打印
    public static String defaultCommonHadle(String send,String[] bDesc){
        HandlebackMsgInf handlebackMsgInf = new DefaultHandleBackMsg(send,bDesc);
        handleBySpecial(handlebackMsgInf);
        return handlebackMsgInf.getBackeMsg();
    }

    //这个地方需要传入不同指令的实现类,进行特殊处理
    public static void  handleBySpecial(HandlebackMsgInf handlMd){
        //获得指令并发送到地磅
        String backMsg = Connction.sendAndGetMsg( handlMd.getSend());
        //处理返回命令
        handlMd.setBackeMsg(backMsg);
        //是否需要特殊处理,不处理,子类的实现方法可以不用写实现逻辑
        handlMd.handle();
    }


}
