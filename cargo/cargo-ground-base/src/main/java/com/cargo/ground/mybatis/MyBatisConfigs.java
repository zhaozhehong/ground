package com.cargo.ground.mybatis;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.mybatis.spring.boot.autoconfigure.MybatisProperties;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;
import com.cargo.ground.datasource.DbType;
import com.cargo.ground.datasource.DynamicDataSource;

@Configuration
@EnableTransactionManagement
public class MyBatisConfigs extends MybatisAutoConfiguration {

	private final Logger logger = Logger.getLogger(MyBatisConfigs.class);
	
	@Value("${spring.dataSource.type}")
    private Class<? extends DataSource> dataSourceType;
	@Value("${spring.datasource.druid.filters}")
    private String filters;
    @Value("${spring.datasource.druid.maxActive}")
    private int maxActive;
    @Value("${spring.datasource.druid.initialSize}")
    private int initialSize;
    @Value("${spring.datasource.druid.maxWait}")
    private int maxWait;
    @Value("${spring.datasource.druid.minIdle}")
    private int minIdle;
    @Value("${spring.datasource.druid.useUnfairLock}")
    private boolean useUnfairLock;
    @Value("${spring.datasource.druid.timeBetweenEvictionRunsMillis}")
    private int timeBetweenEvictionRunsMillis;
    @Value("${spring.datasource.druid.minEvictableIdleTimeMillis}")
    private int minEvictableIdleTimeMillis;
    @Value("${spring.datasource.druid.validationQuery}")
    private String validationQuery;
    @Value("${spring.datasource.druid.testWhileIdle}")
    private boolean testWhileIdle;
    @Value("${spring.datasource.druid.testOnBorrow}")
    private boolean testOnBorrow;
    @Value("${spring.datasource.druid.testOnReturn}")
    private boolean testOnReturn;
    @Value("${spring.datasource.druid.poolPreparedStatements}")
    private boolean poolPreparedStatements;
    @Value("${spring.datasource.druid.maxOpenPreparedStatements}")
    private int maxOpenPreparedStatements;
    @Value("${spring.datasource.druid.removeAbandoned}")
    private boolean removeAbandoned;
    @Value("${spring.datasource.druid.removeAbandonedTimeout}")
    private int removeAbandonedTimeout;
    @Value("${spring.datasource.druid.logAbandoned}")
    private boolean logAbandoned;
	
    @Bean("dynamicDataSource")
    public DynamicDataSource dynamicDataSource(
            @Qualifier("oracleCargoGroundMasterDataSource") DataSource oracleCargoGroundMasterDataSource,
            @Qualifier("oracleCargoGroundSlaveDataSource") DataSource oracleCargoGroundSlaveDataSource
            ) {
        Map<Object, Object> targetDataSources = new HashMap<Object, Object>();
        targetDataSources.put(DbType.oracleCargoGroundMaster, oracleCargoGroundMasterDataSource);
        targetDataSources.put(DbType.oracleCargoGroundSlave, oracleCargoGroundSlaveDataSource);

        DynamicDataSource dataSource = new DynamicDataSource();
        dataSource.setTargetDataSources(targetDataSources);
        return dataSource;
    }
	
    
    @Bean("oracleCargoGroundMasterDataSource")
    @ConfigurationProperties(prefix = "oracleCargoGroundMaster")
    public DataSource oracleCargoGroundMasterDataSource(){
        return setConnection((DruidDataSource)DataSourceBuilder.create().type(dataSourceType).build());
    }

    @Bean("oracleCargoGroundSlaveDataSource")
    @Primary
    @ConfigurationProperties(prefix = "oracleCargoGroundSlave")
    public DataSource oracleCargoGroundSlaveDataSource(){
        return setConnection((DruidDataSource)DataSourceBuilder.create().type(dataSourceType).build());
    }
    
    @Bean
    public SqlSessionFactory sqlSessionFactory(
            @Qualifier("dynamicDataSource") DynamicDataSource dynamicDataSource,
            @Value("${mybatis.type-aliases-package}") String typeAliasesPackage,
            @Value("${mybatis.mapper-locations}") String mapperLocations,
            @Value("${mybatis.config-locations}") String configLocation
            ) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dynamicDataSource);
        /**
         * 下边两句仅仅用于*.xml文件，如果整个持久层操作不需要使用到xml文件的话（只用注解就可以搞定），则不加
         */
        factoryBean.setTypeAliasesPackage(typeAliasesPackage);// 指定基包
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocations));//
        factoryBean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource(configLocation));
//        factoryBean.setPlugins(new Interceptor[]{pageHelper, mapperInterceptor});
        return factoryBean.getObject();
    }

    /**
     * @Description:事务管理
     */
    @Bean
    @Order(2)
    public DataSourceTransactionManager transactionManager(DynamicDataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }
    
    /**
      * 设置连接属性
     * @param dataSource
     * @return
     */
    private DataSource setConnection(DruidDataSource dataSource){
        try {
            dataSource.setFilters(filters);
        } catch (SQLException e) {
			logger.error("druid configuration initialization filter", e);
        }
        dataSource.setMaxActive(maxActive);
        dataSource.setInitialSize(initialSize);
        dataSource.setMaxWait(maxWait);
        dataSource.setMinIdle(minIdle);
        dataSource.setUseUnfairLock(useUnfairLock);
        dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        dataSource.setValidationQuery(validationQuery);
        dataSource.setTestWhileIdle(testWhileIdle);
        dataSource.setTestOnBorrow(testOnBorrow);
        dataSource.setTestOnReturn(testOnReturn);
        dataSource.setPoolPreparedStatements(poolPreparedStatements);
        dataSource.setMaxOpenPreparedStatements(maxOpenPreparedStatements);
        dataSource.setRemoveAbandoned(removeAbandoned);
        dataSource.setRemoveAbandonedTimeout(removeAbandonedTimeout);
        dataSource.setLogAbandoned(logAbandoned);
        return dataSource;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	public MyBatisConfigs(MybatisProperties properties, ObjectProvider<Interceptor[]> interceptorsProvider,
			ResourceLoader resourceLoader, ObjectProvider<DatabaseIdProvider> databaseIdProvider,
			ObjectProvider<List<ConfigurationCustomizer>> configurationCustomizersProvider) {
		super(properties, interceptorsProvider, resourceLoader, databaseIdProvider, configurationCustomizersProvider);
	}

	
	

}
