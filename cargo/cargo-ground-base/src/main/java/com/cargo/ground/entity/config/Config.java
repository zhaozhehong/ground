package com.cargo.ground.entity.config;

import com.cargo.ground.entity.BaseEntitys;

import java.util.Map;

/**
 * 超级管理员配置
 */
public class Config extends BaseEntitys {

    private Map<String, String> configMap;

    /**
     * 处理人的选择方式
     * 0：部门-处理人
     * 1：岗位-处理人
     * @Return
     */
    public String getProcessorSelectType() {
        return getStringValueByName("ProcessorSelectType");
    }

    /**
     * 是否显示风险度测量值
     * @Return
     */
    public Boolean showRiskValue() {
        return getBooleanValueByName("ShowRiskValue");
    }

    /**
     * 公司名称
     * @Return
     */
    public String getCompanyName() {
        return getStringValueByName("CompanyName");
    }

    /**
     * 租金通知书提前天数
     * @Return
     */
    public Integer getDaysBeforeRent() {
        return getIntValueByName("DaysBeforeRent");
    }

    /**
     * 租前息通知书提前天数
     * @Return
     */
    public Integer getDaysBeforeInterest() {
        return getIntValueByName("DaysBeforeInterest");
    }

    /**
     * 罚息通知书提前天数
     * @Return
     */
    public Integer getDaysBeforePenalty() {
        return getIntValueByName("DaysBeforePenalty");
    }

    /**
     * 发送邮件的服务器的IP
     * @Return
     */
    public String getMailServerHost() {
        return getStringValueByName("MailServerHost");
    }

    /**
     * 邮件服务器端口
     * @Return
     */
    public String getMailServerPort() {
        return getStringValueByName("MailServerPort");
    }

    /**
     * 代理邮件的服务器开关
     * @Return
     */
    public boolean getMailProxySwitch() {
        return getBooleanValueByName("MailProxySwitch");
    }

    /**
     * 代理邮件的服务器的IP
     * @Return
     */
    public String getMailProxyHostHost() {
        return getStringValueByName("MailProxyHostHost");
    }

    /**
     * 代理服务器端口
     * @Return
     */
    public String getMailProxyHostPort() {
        return getStringValueByName("MailProxyHostPort");
    }

    /**
     * 登陆邮件发送服务器的用户名
     * @Return
     */
    public String getMailUserName() {
        return getStringValueByName("MailUserName");
    }

    /**
     * 登陆邮件发送服务器的密码
     * @Return
     */
    public String getMailPassword() {
        return getStringValueByName("MailPassword");
    }

    /**
     * 登陆邮件发送服务器地址
     * @Return
     */
    public String getMailFromAddress() {
        return getStringValueByName("MailFromAddress");
    }

    /**
     * 租金核销指令生成提前天数
     * @Return
     */
    public Integer getVerifiyDaysBeforeRent() {
        return getIntValueByName("VerifiyDaysBeforeRent");
    }

    /**
     * 租前息销指令生成提前天数
     * @Return
     */
    public Integer getVerifiyDaysBeforeInterest() {
        return getIntValueByName("VerifiyDaysBeforeInterest");
    }

    /**
     * 手续费核销指令生成提前天数
     * @Return
     */
    public Integer getVerifiyDaysBeforeDeposit() {
        return getIntValueByName("VerifiyDaysBeforePoundage");
    }

    /**
     * 保证金核销指令生成提前天数
     * @Return
     */
    public Integer getVerifiyDaysBeforePoundage() {
        return getIntValueByName("VerifiyDaysBeforeDeposit");

    }

    /**
     * 终止指令生成提前天数
     * @Return
     */
    public Integer getVerifiyDaysBeforeTermination() {
        return getIntValueByName("VerifiyDaysBeforeTermination");
    }

    /**
     * 获取公司代码(不显示在页面上)
     * @Return
     */
    public String getCompanyCode() {
        return getStringValueByName("CompanyCode");
    }

    /**
     * true表示记账
     * 各公司配置是否记账，是否生成准凭证，比如华运，就配置0
     * 页面虽然放开此功能但是配置为0，也就是不记准凭证的账
     * 其它功能菜单没有放开，所以不会记录准凭证
     * @Return
     */
    public boolean getIfAccounting() {
        return "1".equals(getStringValueByName("IfAccounting"));
    }

    /**
     * 商机转化项目是否需要经过立项审批(不显示在页面上)
     * @Return
     */
    public Boolean getIfNeedProjectApplication() {
        return getBooleanValueByName("IfNeedProjectApplication");
    }

    /**
     * 获取流程访问路径配置
     * @Return
     */
    public String getProcessPath() {
        return getStringValueByName("ProcessPath");
    }

    /**
     * 获取登录页面地址
     * @Return
     */
    public String getLoginPage() {
        return getStringValueByName("LoginPage");
    }

    /**
     * 是否默认信息
     * @Return
     */
    public String getShowDefault() {
        return getStringValueByName("ShowDefault");
    }

    /**
     * 登录密码错误最大次数
     * @Return
     */
    public Integer getLoginRetryTimes() {
        return getIntValueByNameWithDefault("LoginRetryTimes", 3);
    }

    /**
     * 合同逾期天数阈值
     * @Return
     */
    public Integer getMaxOverdueDays() {
        return getIntValueByName("MaxOverdueDays");

    }

    /**
     * 申请手续费指令生成提前天数
     * @Return
     */
    public Integer DaysBeforeServiceCharge() {
        return getIntValueByName("DaysBeforeServiceCharge");
    }

    /**
     * 申请还本付息指令生成提前天数
     * @Return
     */
    public Integer getDaysDebtService() {
        return getIntValueByName("DaysDebtService");
    }

    /**
     * 融资合同终止指令生成提前天数
     * @Return
     */
    public Integer getDaysBeforeTermination() {
        return getIntValueByName("DaysBeforeTermination");
    }

    /**
     * 租期检查待办提醒提前天数
     */
    public Integer getDaysBeforeLeaseCheckBacklog() {
        return getIntValueByName("DaysBeforeLeaseCheckBacklog");
    }

    /**
     * 质量分类 待办提醒提前天数
     */
    public Integer getDaysBeforeQualityClassifyBacklog() {
        return getIntValueByName("DaysBeforeQualityClassifyBacklog");
    }

    public String getStringValueByName(String name) {
        String value = "";
        if (configMap.containsKey(name)) {
            value = configMap.get(name);
        }
        return value;
    }

    public Integer getIntValueByName(String name) {
        Integer value = 0;
        if (configMap.containsKey(name)) {
            try {
                value = Integer.parseInt(configMap.get(name));
            } catch (NumberFormatException e) {
                value = 0;
            }
        }
        return value;
    }

    public Integer getIntValueByNameWithDefault(String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (configMap.containsKey(name)) {
            try {
                value = Integer.parseInt(configMap.get(name));
            } catch (NumberFormatException e) {
                value = defaultValue;
            }
        }
        return value;
    }

    private Boolean getBooleanValueByName(String name) {
        Boolean value = false;
        if (configMap.containsKey(name)) {
            //value = Boolean.valueOf((String) configMap.get(name));
            String realValue = configMap.get(name);
            if ("1".equals(realValue) || "true".equals(realValue)) {
                value = true;
            }
        }
        return value;
    }

    public Config() {
    }

    public Config(Map<String, String> configMap) {
        this.configMap = configMap;
    }

    public Map<String, String> getConfigMap() {
        return configMap;
    }

    public void setConfigMap(Map<String, String> configMap) {
        this.configMap = configMap;
    }

}