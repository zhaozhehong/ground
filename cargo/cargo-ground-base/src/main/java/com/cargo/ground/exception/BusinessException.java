package com.cargo.ground.exception;

import com.cargo.ground.enums.ExceptionEnum;

/**
 * @ClassName: BusinessException
 * @Description: 自定义异常类
 * @author: zengjianwen
 * @date: 2018年11月16日 下午7:20:23
 */
public class BusinessException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	private Integer code;

	public BusinessException(ExceptionEnum exceptionEnum) {
		super(exceptionEnum.getMsg());
		this.code = exceptionEnum.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
