package com.cargo.ground.util.wBridge;

/**
 * @Desc 解析地磅返回的数据
 * @LastPeson xuxu
 **/
public class AnalysisStr {

    /***
     * 公用的方法 ,循环处理解析命令
     * */
    public static String analysisRecv(String recvStr) {
        StringBuffer result = new StringBuffer();
        if (recvStr != null) {
            //空格分隔,长度
            String[] arrs = recvStr.split(" ");
            //循环
            for (String e : arrs){
                if (e != null) {
                    //将数据转换
                    char[] hexs = e.toCharArray();
                    byte[] bytes = new byte[e.length() / 2];
                    int n;
                    for (int i = 0; i < bytes.length; i++) {
                        n = Constants.str16.indexOf(hexs[2 * i]) * 16;
                        n += Constants.str16.indexOf(hexs[2 * i + 1]);
                        //转byte
                        bytes[i] = (byte) (n & 0xff);
                    }
                    result.append(new String(bytes));
                } else {
                    System.err.println("解析返回指令失败!");
                }
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        String read ="02 30 30 31 2c 30 30 2c 31 32 33 34 35 31 32 33 34 35 2c 30 33 2c 30 30 2c 30 30 2c 2b 30 30 30 30 37 32 34 2c 2b 30 30 30 30 30 30 30 2c 2b 30 30 30 30 37 32 34 2c 30 30 30 30 30 2c 30 30 30 30 30 2c 2b 30 30 30 30 30 30 30 2c 31 45 03";
        System.err.println(analysisRecv(read));
    }
}
