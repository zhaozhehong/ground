package com.cargo.ground.datasource;
/**
 * @author wsw
 * 2018年11月21日上午11:16:05
 * @Description:数据源类型
 */
public enum DbType {
	
	/**
     * 	 主
     */
    oracleCargoGroundMaster,
    
    /**
     * 从
     */
    oracleCargoGroundSlave,
    
    
}
