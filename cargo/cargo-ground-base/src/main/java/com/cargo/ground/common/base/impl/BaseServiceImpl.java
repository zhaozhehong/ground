package com.cargo.ground.common.base.impl;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.common.base.BaseService;

/**
 * @ClassName: BaseServiceImpl
 * @Description: service层基类的实现类
 * @author: zengjianwen
 * @date: 2018年11月5日 上午11:48:06
 * @param <T>
 * @param <ID>
 */
@Service
public abstract class BaseServiceImpl<T> implements BaseService<T> {
	protected static final Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);
	public abstract BaseMapper<T> getMapper();

	@Override
	public T selectById(Serializable id) {
		return getMapper().selectById(id);
	}
	@Override
	public T selectOne(T obj) {
		return getMapper().selectOne(obj);
	}
	@Override
	public Long selectCount(T obj) {
		return getMapper().selectCount(obj);
	}
	@Override
	public List<T> selectList(T obj) {
		return getMapper().selectList(obj);
	}
	@Override
	public int save(T obj) {
		return getMapper().save(obj);
	}
	@Override
	public int update(T obj) {
		return getMapper().update(obj);
	}
	@Override
	public int batchUpdate(List<?> list) {
		return getMapper().batchUpdate(list);
	}
	@Override
	public int delById(Serializable id) {
		return getMapper().delById(id);
	}
	@Override
	public int delList(List<?> list) {
		return getMapper().delList(list);
	}
	@Override
	public int delArray(Serializable[] ids) {
		return getMapper().delArray(ids);
	}
	
}
