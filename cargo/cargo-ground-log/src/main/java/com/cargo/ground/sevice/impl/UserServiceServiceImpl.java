package com.cargo.ground.sevice.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.common.base.impl.BaseServiceImpl;
import com.cargo.ground.entity.user.User;
import com.cargo.ground.enums.ExceptionEnum;
import com.cargo.ground.exception.BusinessException;
import com.cargo.ground.http.HttpUtil;
import com.cargo.ground.mapper.UserMapper;
import com.cargo.ground.sevice.UserService;

@Service
public class UserServiceServiceImpl extends BaseServiceImpl<User> implements UserService {
	@Resource
	private UserMapper userMapper;

	// 测试demo
	@Override
	public User getUserByLoginName(String username) {
		// 验证成功情况
		logger.info("逻辑处理成功成功！");
		return new User();
	}
	@Override
	public BaseMapper<User> getMapper() {
		return userMapper;
	}
	@Override
	public User getUserById(Integer id) {
		User user = null;
		// if (user == null) {
		// throw new BusinessException(ExceptionEnum.USER_NOT_FIND);
		// }
		// 测试 调用第三方接口 如果发生异常抛出去 调用者即可抓取到该自定义的异常信息CODE_800003
		try {
			String jsonRs = HttpUtil.postByMap("url", null);
		} catch (Exception e) {
			logger.info("调用验证接口发生异常" + e.getMessage());
			e.printStackTrace();
			throw new BusinessException(ExceptionEnum.CODE_800003);
		}
		return user;
	}
}
