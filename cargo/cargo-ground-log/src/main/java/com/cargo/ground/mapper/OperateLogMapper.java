package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.log.OperateLogEntity;

/**地面系统项目
 * @Description 
 * @author kj_xiaoyifei
 * @date 2018年10月30日
**/

public interface OperateLogMapper extends BaseMapper<OperateLogEntity> {
	
	

}
