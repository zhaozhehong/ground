package com.cargo.ground.sevice.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.common.base.impl.BaseServiceImpl;
import com.cargo.ground.entity.SysLog;
import com.cargo.ground.mapper.SysLogMapper;
import com.cargo.ground.sevice.SysLogService;
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLog> implements SysLogService {
	
	@Resource
	private SysLogMapper sysLogMapper;
	
	@Override
	public BaseMapper<SysLog> getMapper() {
		return sysLogMapper;
	}
	@Override
	public List<SysLog> selectSysLogList(Map<String, Object> params) {
		return sysLogMapper.getUserList(params);
	}
}