package com.cargo.ground.jms;

import java.util.Date;

import javax.jms.Message;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.log4j.spi.LoggingEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.cargo.ground.entity.SysLog;
import com.cargo.ground.entity.log.OperateLogEntity;
import com.cargo.ground.sevice.OperateLogService;
import com.cargo.ground.sevice.SysLogService;
import com.cargo.ground.util.StringDateUtil;

	/**
	 * 接收处理消息
	 */
	@Component
	public class MessageLogHandler {
		private static final Logger logger = LoggerFactory
				.getLogger(MessageHandler.class);
		@Autowired
		private  SysLogService   sysLogService;
		
		@Autowired
		private OperateLogService operateLogService;
	 
		@JmsListener(destination="ListenerLog", containerFactory="topicListenerContainer")
		  public void onMessage(Message message) {
		        try {
		        	System.out.println("recieve3###################" + message + "###################");
		            // receive log event in your consumer
		            LoggingEvent event = (LoggingEvent)((ActiveMQObjectMessage)message).getObject();
		            System.out.println("Logging project: [" + event.getLevel() + "]: "+ event.getMessage());
		            
		            String producecerId=message.getJMSMessageID();
		            long eeee=message.getJMSTimestamp();
		          String datetiem=  StringDateUtil.timeStamp2Date(String.valueOf(eeee).substring(0, 10), "");
		           String  levet= event.getLevel().toString();
		           String  messages= event.getMessage()+"";
		           String  logname=event.getLoggerName(); 
		           
		           System.out.println("producecerId=" +producecerId + ",jMSTimestamp= "+ datetiem+ ",levet= "+ levet+ ",messages= "+ messages+ ",logname= "+ logname);
		           System.out.println("Logging project: [" + event.getLevel() + "]: "+ event.getMessage());
		       
			SysLog sysLogEntity = new SysLog();
			sysLogEntity.setLogLevel(event.getLevel().toString());
			sysLogEntity.setCreatetime(new Date());
		           sysLogEntity.setLogname(event.getLoggerName());
		           sysLogEntity.setLogDateTime( StringDateUtil.timeStampToDate(String.valueOf(eeee).substring(0, 10), ""));
		           sysLogEntity.setMessages(event.getMessage().toString());
		           sysLogEntity.setServerId(message.getJMSMessageID());
		           sysLogService.save(sysLogEntity);
		               
		        } catch (Exception e) {
		        	logger.error(e.getMessage());
		        }
		    }
		

		@JmsListener(destination="topicLog", containerFactory="topicListenerContainer")
		public void recieveOperLog(Message message) {
			 try {
		        	System.out.println("topicLog###################" + message + "###################");
		            // receive log event in your consumer
		        	OperateLogEntity operateLogEntity = (OperateLogEntity)((ActiveMQObjectMessage)message).getObject();
		            System.out.println("Logging project: [" + operateLogEntity.getOperateDesc()+"");
		            
		           
		            
		            operateLogService.save(operateLogEntity);
		               
		        } catch (Exception e) {
		        	logger.error(e.getMessage());
		        }
		}
		  
	}