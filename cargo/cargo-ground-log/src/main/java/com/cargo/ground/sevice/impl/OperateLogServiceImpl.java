package com.cargo.ground.sevice.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.common.base.impl.BaseServiceImpl;
import com.cargo.ground.entity.log.OperateLogEntity;
import com.cargo.ground.mapper.OperateLogMapper;
import com.cargo.ground.sevice.OperateLogService;
@Service
public class OperateLogServiceImpl extends BaseServiceImpl<OperateLogEntity> implements OperateLogService {
	
	@Resource
	private OperateLogMapper operateLogMapper;
	
	@Override
	public BaseMapper<OperateLogEntity> getMapper() {
		return operateLogMapper;
	}
}