package com.cargo.ground.entity;

import java.util.Date;

public class SysLog {
    private Long fdId;

    //产生日志服务器ID
    private String serverId;

    //产生日志服务器ID
    private Date logDateTime;

    //日志级别:0:ERROT,1:WARN.2:LOG,3:DEBUG,4其他
    private String logLevel;

    //日志信息
    private String messages;

    //日志信息
    private String logname;

    //日志信息
    private Date createtime;

    /**
     * @return FD_ID
     */
    public Long getFdId() {
        return fdId;
    }

    /**
     * @param fdId
     */
    public void setFdId(Long fdId) {
        this.fdId = fdId;
    }

    /**
     * 获取产生日志服务器ID
     *
     * @return SERVER_ID - 产生日志服务器ID
     */
    public String getServerId() {
        return serverId;
    }

    /**
     * 设置产生日志服务器ID
     *
     * @param serverId 产生日志服务器ID
     */
    public void setServerId(String serverId) {
        this.serverId = serverId == null ? null : serverId.trim();
    }

    /**
     * 获取产生日志服务器ID
     *
     * @return LOG_DATE_TIME - 产生日志服务器ID
     */
    public Date getLogDateTime() {
        return logDateTime;
    }

    /**
     * 设置产生日志服务器ID
     *
     * @param logDateTime 产生日志服务器ID
     */
    public void setLogDateTime(Date logDateTime) {
        this.logDateTime = logDateTime;
    }

    /**
     * 获取日志级别:0:ERROT,1:WARN.2:LOG,3:DEBUG,4其他
     *
     * @return LOG_LEVEL - 日志级别:0:ERROT,1:WARN.2:LOG,3:DEBUG,4其他
     */
    public String getLogLevel() {
        return logLevel;
    }

    /**
     * 设置日志级别:0:ERROT,1:WARN.2:LOG,3:DEBUG,4其他
     *
     * @param logLevel 日志级别:0:ERROT,1:WARN.2:LOG,3:DEBUG,4其他
     */
    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel == null ? null : logLevel.trim();
    }

    /**
     * 获取日志信息
     *
     * @return MESSAGES - 日志信息
     */
    public String getMessages() {
        return messages;
    }

    /**
     * 设置日志信息
     *
     * @param messages 日志信息
     */
    public void setMessages(String messages) {
        this.messages = messages == null ? null : messages.trim();
    }

    /**
     * 获取日志信息
     *
     * @return LOGNAME - 日志信息
     */
    public String getLogname() {
        return logname;
    }

    /**
     * 设置日志信息
     *
     * @param logname 日志信息
     */
    public void setLogname(String logname) {
        this.logname = logname == null ? null : logname.trim();
    }

    /**
     * 获取日志信息
     *
     * @return CREATETIME - 日志信息
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * 设置日志信息
     *
     * @param createtime 日志信息
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}