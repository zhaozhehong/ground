package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.Proxy;

public interface ProxyMapper extends BaseMapper<Proxy> {
}