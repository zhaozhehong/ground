package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.FlightManagementPurpose;

public interface FlightManagementPurposeMapper extends BaseMapper<FlightManagementPurpose> {
}