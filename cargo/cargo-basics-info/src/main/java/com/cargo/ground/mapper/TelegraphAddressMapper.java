package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.TelegraphAddress;

import java.util.List;
import java.util.Map;

/**
 * 电报地址管理
 * @author: liyiting
 */
public interface TelegraphAddressMapper extends BaseMapper<TelegraphAddress> {
    public List<TelegraphAddress> pages(Map<String, Object> parmMap);
}