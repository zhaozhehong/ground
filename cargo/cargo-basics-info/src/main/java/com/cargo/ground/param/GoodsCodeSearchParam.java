package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 货物代码管理
 * @author: liyiting
 */
public class GoodsCodeSearchParam extends PageSearchParam {
    private String goodsCode;

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }
}
