package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.FlightInfo;

public interface FlightInfoMapper extends BaseMapper<FlightInfo> {
}