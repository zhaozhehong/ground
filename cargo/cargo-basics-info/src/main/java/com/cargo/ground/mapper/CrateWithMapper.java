package com.cargo.ground.mapper;


import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.CrateWith;

import java.util.List;
import java.util.Map;

public interface CrateWithMapper extends BaseMapper<CrateWith> {
    public List<CrateWith> pages(Map<String, Object> parmMap);
}