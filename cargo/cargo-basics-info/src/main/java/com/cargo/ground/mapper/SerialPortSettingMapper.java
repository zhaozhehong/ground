package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.SerialPortSetting;

import java.util.List;

public interface SerialPortSettingMapper extends BaseMapper<SerialPortSetting> {
    public List<SerialPortSetting> pages();
}