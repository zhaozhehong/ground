package com.cargo.ground.sevice;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseService;
import com.cargo.ground.entity.BatteryAirLine;
import com.cargo.ground.entity.BatteryInfo;
import com.cargo.ground.param.BatteryAirLineSearchParam;
@Service
public interface BatteryInfoService extends BaseService<BatteryInfo> {
	List<BatteryAirLine> queryBatteryAirLineListByParam(BatteryAirLineSearchParam searchParam);
	void addBatchBatteryAirLine(List<BatteryAirLine> list);
	int delBatteryAirLineById(Long id);
	List<BatteryInfo> queryWithPageByParam(Map<String, Object> parmMap);
	void addBatchBatteryInfo(List<BatteryInfo> list);
}