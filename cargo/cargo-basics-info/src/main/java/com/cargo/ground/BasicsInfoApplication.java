package com.cargo.ground;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
@MapperScan(value = "com.cargo.ground.mapper")
public class BasicsInfoApplication {
	public static void main(String[] args) {
		SpringApplication.run(BasicsInfoApplication.class, args);
		// new SpringApplicationBuilder(BasicsInfoApplication.class).web(true).run(args);
	}

}