package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 报T货航班
 * @author: liyiting
 */
public class GoodsFlightSearchParam extends PageSearchParam {
    private String filghtNumber;

    public String getFilghtNumber() {
        return filghtNumber;
    }

    public void setFilghtNumber(String filghtNumber) {
        this.filghtNumber = filghtNumber;
    }
}
