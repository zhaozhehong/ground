package com.cargo.ground.param;

import com.cargo.ground.common.param.PageSearchParam;

/**
 * 运单特码
 * @author: liyiting
 */
public class WaybillCodeSearchParam extends PageSearchParam {
    //特殊代码
    private String code;

    //特码名称
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
