package com.cargo.ground.param;

public class BatteryAirLineSearchParam {
	// 航班号
	private String airNum;

	// 目的站
	private String endStation;

	// 承运人
	private String carrier;

	/**
	 * @return the airNum
	 */
	public String getAirNum() {
		return airNum;
	}

	/**
	 * @param airNum the airNum to set
	 */
	public void setAirNum(String airNum) {
		this.airNum = airNum;
	}

	/**
	 * @return the endStation
	 */
	public String getEndStation() {
		return endStation;
	}

	/**
	 * @param endStation the endStation to set
	 */
	public void setEndStation(String endStation) {
		this.endStation = endStation;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
}
