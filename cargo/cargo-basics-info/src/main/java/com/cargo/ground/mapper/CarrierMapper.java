package com.cargo.ground.mapper;


import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.Carrier;

import java.util.List;
import java.util.Map;

/**
 * 承运人
 * @author: liyiting
 */
public interface CarrierMapper extends BaseMapper<Carrier> {
    public List<Carrier> pages(Map<String, Object> parmMap);
}