package com.cargo.ground.mapper;

import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.GoodsCode;

import java.util.List;
import java.util.Map;

public interface GoodsCodeMapper extends BaseMapper<GoodsCode> {

    public List<GoodsCode> pages(Map<String, Object> parmMap);
}