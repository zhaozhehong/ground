package com.cargo.ground.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.user.entity.User;


@FeignClient("addgoods")
public interface DemoFeignClient {

	@GetMapping("/getById/{id}")
	public LoadArrange getById(@PathVariable("id") Long id);
	
	@RequestMapping(value="/getById2",method=RequestMethod.POST)
	public String getById2(@RequestBody User user);
	
}
