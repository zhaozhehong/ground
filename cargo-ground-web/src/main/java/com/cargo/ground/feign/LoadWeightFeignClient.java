package com.cargo.ground.feign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cargo.addgoods.entity.LoadWeight;
import com.cargo.addgoods.vo.LoadWeightVo;
import com.cargo.ground.response.Result;


@FeignClient("addgoods")
public interface LoadWeightFeignClient {
	
	/**
	 * 
	 * @param tbLoadWeightVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 轻泡超重的导出
	 * 2018年11月8日下午2:12:17
	 */
	@RequestMapping(value="/loadWeightExport",method=RequestMethod.POST)
	public Result<String> loadWeightExport(@RequestBody LoadWeightVo tbLoadWeightVo,@RequestParam("response") HttpServletResponse response,@RequestParam("request") HttpServletRequest request);
	
/**
 * 
 * @param tbLoadWeightVo
 * @return
 * @author yuwei
 * @Description: 有查询条件的查询所有的轻泡超重的数据
 * 2018年11月8日下午2:20:14
 */
	@RequestMapping(value="/findLoadWeight",method=RequestMethod.POST)
	public  Result<LoadWeight>  findLoadWeight(@RequestBody LoadWeightVo tbLoadWeightVo);

	
	
}
