package com.cargo.ground.feign.user;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.cargo.user.entity.User;

@FeignClient("users")
public interface UserFeignService {
	
	@PostMapping("/getUserByUserName/{userName}")
	public User getUserByUserName(@PathVariable("userName") String userName);
}
