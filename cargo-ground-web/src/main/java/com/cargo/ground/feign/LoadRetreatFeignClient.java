package com.cargo.ground.feign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cargo.addgoods.vo.BillRetreatVo;
import com.cargo.ground.response.Result;


@FeignClient("addgoods")
public interface LoadRetreatFeignClient {
	
	/**
	 * 
	 * @param billRetreatVo
	 * @return
	 * @author yuwei
	 * @Description: 退运原因关联运单表查询
	 * 2018年11月8日下午2:36:57
	 */
	@RequestMapping(value="/selectByRetreat",method=RequestMethod.POST)
	public   Result<BillRetreatVo> selectByRetreat(@RequestBody BillRetreatVo billRetreatVo);
	/**
	 * 
	 * @param billRetreatVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 货物退运的导出
	 * 2018年11月8日下午2:41:19
	 */
	@RequestMapping(value="/loadRetreatExport",method=RequestMethod.POST)
	public Result<String> loadRetreatExport(@RequestBody BillRetreatVo billRetreatVo,@RequestParam("response")HttpServletResponse response,@RequestParam("request") HttpServletRequest request);
	
	/**
	 * 
	 * @param retreatId
	 * @author yuwei
	 * @Description: 取消退运
	 * 2018年11月20日上午11:32:53
	 */
	@GetMapping("/cancelRetreat")
	public Result<String> cancelRetreat(@RequestParam("retreatId")Long retreatId,@RequestParam("billId")Long billId) ;
/**
 * 
 * @param billRetreatVo
 * @param response
 * @param request
 * @author yuwei
 * @Description: 退运锂电池日志查询的导出
 * 2018年11月8日下午4:01:41
 */
	@RequestMapping(value="/loadBatteryExport",method=RequestMethod.POST)
	public Result<String> loadBatteryExport(@RequestBody BillRetreatVo billRetreatVo,@RequestParam("response")HttpServletResponse response, @RequestParam("request")HttpServletRequest request);

	
	
}
