package com.cargo.ground.feign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.entity.LoadException;
import com.cargo.addgoods.vo.LoadBillVo;
import com.cargo.ground.response.Result;


@FeignClient("addgoods")
public interface LoadBillFeignClient {

	/**
		 * 
		 * @param tbLoadBillVo
		 * @return
		 * @author yuwei
		 * @Description: 带条件查询所有运单表的信息
		 * 2018年11月12日上午10:02:07
		 */
		@RequestMapping(value="/selectByBill",method=RequestMethod.POST)
		public Result<String>  selectByBill(@RequestBody LoadBillVo tbLoadBillVo);
		/**
		 * 
		 * @param loadBill
		 * @author yuwei
		 * @Description: 保存运单
		 * 2018年11月12日上午10:24:56
		 */
		@RequestMapping(value="/saveBill",method=RequestMethod.POST)
		public  Result<String> saveBill(@RequestBody LoadBill loadBill);
		
		/**
		 * 
		 * @param billId
		 * @return
		 * @author yuwei
		 * @Description: 核单
		 * 2018年11月22日上午10:33:01
		 */
		@RequestMapping("/billAuditing")
		public  Result<String> billAuditing(@RequestParam("billId")Long billId);
		/**
		 * 
		 * @param loadException
		 * @author yuwei
		 * @Description: 保存运单异常的数据
		 * 2018年11月19日上午11:38:47
		 */
		@RequestMapping(value="/saveLoadException",method=RequestMethod.POST)
		public  Result<String> saveLoadException(@RequestBody LoadException loadException);
		/**
		 * 
		 * @param billId
		 * @param status
		 * @author yuwei
		 * @Description: 取消核单改变状态
		 * 2018年11月12日上午10:27:57
		 */
		@GetMapping("/updateByStatus")
		public  Result<String> updateByStatus(@RequestParam("billId")Long billId);
		/**
		 * 
		 * @param tbLoadBillVo
		 * @param response
		 * @param request
		 * @author yuwei
		 * @Description: 收运的运单导出
		 * 2018年11月12日下午2:23:45
		 */
		@RequestMapping(value="/loadBillExport",method=RequestMethod.POST)
		public Result<String> loadBillExport(@RequestBody LoadBillVo tbLoadBillVo,@RequestParam("response")HttpServletResponse response,@RequestParam("request") HttpServletRequest request);
	
	
}
