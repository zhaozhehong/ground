package com.cargo.ground.feign;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cargo.addgoods.entity.LoadReleasContainer;
import com.cargo.addgoods.vo.LoadContainerVo;
import com.cargo.ground.response.Result;


@FeignClient("addgoods")
public interface LoadReleasContainerFeignClient {
	
	/**
	 * 
	 * @param loadReleasContainer
	 * @author yuwei
	 * @Description: 保存方法
	 * 2018年11月15日下午2:22:05
	 */
	@RequestMapping(value="/saveLoadReleasContainer",method=RequestMethod.POST)
	public  Result<String> saveLoadReleasContainer(@RequestBody LoadReleasContainer loadReleasContainer);
	/**
	 * 
	 * @param loadContainerVo
	 * @return
	 * @author yuwei
	 * @Description: 查询释放容器的数据
	 * 2018年11月17日上午11:42:28
	 */
	@RequestMapping(value="/selectByLoadReleasContainer",method=RequestMethod.POST)
	public  Result<String>  selectByLoadReleasContainer(@RequestBody LoadContainerVo loadContainerVo);
	/**
	 * 
	 * @param container
	 * @param releaseDate
	 * @return
	 * @author yuwei
	 * @Description: 容器还原 （0是成功，1是失败）
	 * 2018年11月17日下午3:35:02
	 */
	@GetMapping("/restoreByContainer")
	public  Result<String>  restoreByContainer(@RequestParam("container")String  container,@RequestParam("releaseDate")String releaseDate);
	
	
}
