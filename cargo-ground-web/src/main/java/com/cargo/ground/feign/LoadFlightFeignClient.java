package com.cargo.ground.feign;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cargo.addgoods.entity.LoadFlight;
import com.cargo.addgoods.vo.LoadFlightVo;
import com.cargo.ground.response.Result;


@FeignClient("addgoods")
public interface LoadFlightFeignClient {

	/**
	 * 
	 * @param loadFlightVo
	 * @return
	 * @author yuwei
	 * @Description: 有查询条件的查询所有的航班动态的数据
	 * 2018年11月12日下午2:06:11
	 */
	@RequestMapping(value="/selectByFlight",method=RequestMethod.POST)
	public Result<LoadFlight>   selectByFlight( @RequestBody LoadFlightVo loadFlightVo);
	/**
	 * 
	 * @param loadFlightVo
	 * @return
	 * @author yuwei
	 * @Description: 加货大屏展示
	 * 2018年11月12日下午2:06:41
	 */
	@RequestMapping(value="/findByMachine",method=RequestMethod.POST)
	public   Result<LoadFlightVo>  findByMachine(@RequestBody LoadFlightVo loadFlightVo);
	
	
}
