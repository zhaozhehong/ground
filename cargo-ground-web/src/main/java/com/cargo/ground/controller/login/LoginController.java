package com.cargo.ground.controller.login;

import static com.cargo.ground.common.BaseConstens.LOGIN_SUCCESS;
import static com.cargo.ground.common.BaseConstens.MESSAGE;
import static com.cargo.ground.common.BaseConstens.UTF8;
import static com.cargo.ground.common.BaseErrorMsg.NO_USER_ERROR;
import static com.cargo.ground.common.BaseErrorMsg.NO_USER_NAME_ERROR;
import static com.cargo.ground.common.BaseErrorMsg.USER_LOCKED_ERROR;
import static com.cargo.ground.common.BaseErrorMsg.USER_NAME_CODE_ERROR;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cargo.ground.controller.base.BaseController;

/**
 * @author wsw
 * 2018年11月21日下午6:01:56
 * @Description:登录
 */
@Controller
public class LoginController extends BaseController{
	
	private final static Logger logger = Logger.getLogger(LoginController.class);
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
    public String loginHtml() {
        return "login";
    }

    
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String indexHtml() {
        return "index";
    }
    
    @RequestMapping(value = "success", method = RequestMethod.GET)
    public String success() {
        return "success";
    }
    
    @RequestMapping(value = "loginCheck", method = RequestMethod.POST)
    public Map<String, Object> checkUserInfo(String userName, String password, boolean rememberMe) {
        String message = "";
        boolean loginSuccess = true;
        Map<String, Object> map = new HashMap<>();
        Subject currentUser = SecurityUtils.getSubject();

        try {
            logger.info("@@@@@@@@@@ --> begin login { loginname: " + userName + " }");
            if (StringUtils.isNotBlank(userName)) {
                userName = URLDecoder.decode(userName, UTF8).trim();
            } else {
                loginSuccess = false;
                message = NO_USER_NAME_ERROR;
                map.put(LOGIN_SUCCESS, loginSuccess);
                map.put(MESSAGE, message);
                return map;
            }
            //验证用户权限
            UsernamePasswordToken token = new UsernamePasswordToken(userName, URLDecoder.decode(password), rememberMe);
            currentUser.login(token);
        } catch (UnknownAccountException ue) {
            loginSuccess = false;
            message = NO_USER_ERROR;
        } catch (LockedAccountException ae) {
            loginSuccess = false;
            message = USER_LOCKED_ERROR;
        } catch (AuthenticationException ae) {
            loginSuccess = false;
            message = ae.getMessage();
            logger.info("@@@@@@@@@@ --> login fail , cause by :" + ae.getMessage());
        } catch (UnsupportedEncodingException e) {
            loginSuccess = false;
            message = USER_NAME_CODE_ERROR;
        }

        map.put(LOGIN_SUCCESS, loginSuccess);
        map.put(MESSAGE, message);
//        if (loginSuccess) {
//            user = ShrioUtil.getCurrentUser();
//            if (user.getRetryTimes() > 0) {
//                baseLoginUserService.unlockUser(user);
//            }
//            map.put(USER, user);
//        } else {
//            currentUser.getSession().removeAttribute(USERNAME);
//        }

//        ShrioUtil.setCompanyConfig(baseConfigItemService.getAllConfig());
//        map.put(COMPANYCONFIG, ShrioUtil.getCompanyConfig());
        logger.info("@@@@@@@@@@ --> login success { loginname: " + userName + " }");
        return map;
    }
}
