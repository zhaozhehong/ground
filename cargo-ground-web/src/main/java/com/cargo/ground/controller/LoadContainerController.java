package com.cargo.ground.controller;

import com.cargo.addgoods.entity.LoadContainer;
import com.cargo.addgoods.param.AddsMark;
import com.cargo.addgoods.param.FightContainerParam;
import com.cargo.addgoods.vo.LoadPplanVo;
import com.cargo.ground.entity.CheckInBoxVo;
import com.cargo.ground.feign.LoadContainerFeignClient;
import com.cargo.ground.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * @Desc 货物加载--容器查询
 * @LastPeson xuxu
 * <p>
 * 释放容器,
 * 1: 判断状态
 * 2: 插入数据到容器释放表(集装器数据);
 * 3: 修改集装器表状态和时间(容器状态为空闲,已释放,释放时间);
 * 4: 修改预配清单表的数据状态(已释放,释放时间);
 * 核单进箱
 * 1: 向预配清单表插入数据(容器号,运单,状态为生效)
 * 2: 修改集装器表状态为 "已加货"
 * 3: "加货备注" 是不是特码?
 * 还原
 * 5: 入参 : 容器编号 + 还原时间
 * 6: 判断容器已经为空闲状态
 * 7: 根据容器号,时间查询
 **/
@RestController
@RequestMapping("/loadContainer")
public class LoadContainerController {

    @Autowired
    LoadContainerFeignClient containerFeignClient;

    /* 根据航班号查询容器 */
    @RequestMapping("/queryContainerByFlight")
    public Result<List<LoadContainer>> queryContainerByFlight(Long flightNum) {
        return containerFeignClient.queryContainerByFlight(flightNum);
    }

    /**
     * 1: 查询预配清单
     * 2: 关联运单表,查询 体积 , 总重量 , 件数，舱位；
     * flightNum 不能为空;
     * return 当前容器下的所有运单的信息以及统计信息;
     * ToDo 备注信息 暂时未处理
     **/
    @RequestMapping("/queryLoadPreplans")
    public Result<LoadPplanVo> queryByContainerNum(String flightNum, String containerNum) {
        FightContainerParam param = new FightContainerParam();
        param.setContainerNum(containerNum);
        param.setFlightNum(flightNum);
        return containerFeignClient.queryByContainerNum(param);
    }

    /**
     * 货物加载--查询所有容器,对应页面 ... 接口
     * @return 容器列表信息
     */
    @RequestMapping("/queryDefaultLoadContainers")
    public Result<List<LoadContainer>> queryDefaultLoadContainers() {
        return containerFeignClient.queryDefaultLoadContainers();
    }

    /**
     * 货物加载--释放
     * 释放,容器和运单的关系,释放容器和航班的关系;
     * 释放容器,
     * 1: 判断状态
     * 2: 插入数据到容器释放表(集装器数据);
     * 3: 修改集装器表状态和时间(容器状态为空闲,已释放,释放时间)
     * 4: 修改预配清单表的数据状态(已释放,释放时间);
     */
    @RequestMapping("/free")
    public Result<Boolean> free(String containerNum) {
        return containerFeignClient.free(containerNum);
    }

    /**
     * 加货备注
     * containerNum 容器号
     * spaces 逗号隔开,舱位数据
     * codes 加货备注
     * @return
     */
    @RequestMapping("/save")
    public Result save(Double volume, String spaces, String containerNum, List<String> codes) {
        AddsMark mark = new AddsMark();
        mark.setCodes(codes);
        mark.setContainerNum(containerNum);
        mark.setSpaces(spaces);
        mark.setVolume(volume);
        return containerFeignClient.save(mark);
    }

    /**
     * 核单进箱
     * @param checkInBoxVo
     * @return
     */
    @RequestMapping("/checkInBox")
    public Result<Integer> checkIn(CheckInBoxVo checkInBoxVo){
        return containerFeignClient.checkIn(checkInBoxVo);
    }

}
