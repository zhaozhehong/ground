package com.cargo.ground.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.ground.feign.DemoFeignClient;
import com.cargo.user.entity.User;

//@Controller
@RestController
public class DemoCosumerController {
	
	@Autowired
	private DemoFeignClient demoFeignClient;
	

	@GetMapping("/getUser/{id}")
	public LoadArrange getUser(@PathVariable Long id) {
		LoadArrange l = demoFeignClient.getById(id);
		 return l;
	} 
	
	@GetMapping("/getById2")
	public String getById2() {
		User user = new User("khhhh","9999999");
		String str = demoFeignClient.getById2(user);
		return str;
	}
}
