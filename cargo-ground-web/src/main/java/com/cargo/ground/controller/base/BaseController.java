package com.cargo.ground.controller.base;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import com.cargo.user.entity.User;


/**
 * @author wsw
 * 2018年11月27日下午1:56:46
 * @Description: controller中需要用到的公共信息
 */
public class BaseController {

	/**获取当前用户信息*/
	protected User getCurrentLoginUser() {
		return (User)SecurityUtils.getSubject().getPrincipal();
	}
	
	/**获取当前的session，包是shiro下的*/
	protected Session getSession() {
		return SecurityUtils.getSubject().getSession();
	}
}
