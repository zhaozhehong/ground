package com.cargo.ground.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadWeight;
import com.cargo.addgoods.vo.LoadWeightVo;
import com.cargo.ground.feign.LoadWeightFeignClient;
import com.cargo.ground.response.Result;
/**
 * 
 * 
 * @author yuwei
 * @Description: 轻泡超重相关的方法
 * 2018年11月12日上午9:47:20
 */

@RestController
public class LoadWeightController {
	
	@Autowired
	LoadWeightFeignClient loadWeightFeignClient;
	
	/**
	 * 
	 * @param tbLoadWeightVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 轻泡超重的导出
	 * 2018年11月8日下午2:12:17
	 */
	@GetMapping("/loadWeightExport")
	public Result<String> loadWeightExport(LoadWeightVo tbLoadWeightVo,HttpServletResponse response, HttpServletRequest request) {
		
	      return loadWeightFeignClient.loadWeightExport(tbLoadWeightVo, response, request);
	}
	
/**
 * 
 * @param tbLoadWeightVo
 * @return
 * @author yuwei
 * @Description: 有查询条件的查询所有的轻泡超重的数据
 * 2018年11月8日下午2:20:14
 */
	@GetMapping("/findLoadWeight")
	public  Result<LoadWeight>  findLoadWeight(LoadWeightVo tbLoadWeightVo){
		
		return loadWeightFeignClient.findLoadWeight(tbLoadWeightVo);
		
	}
}
