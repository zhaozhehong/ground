package com.cargo.ground.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.entity.LoadException;
import com.cargo.addgoods.vo.LoadBillVo;
import com.cargo.ground.feign.LoadBillFeignClient;
import com.cargo.ground.response.Result;

/**
 * 
 * 
 * @author yuwei
 * @Description: 运单相关的方法
 * 2018年11月12日上午9:46:13
 */
@RestController
public class LoadBillController {
	@Autowired
	private LoadBillFeignClient loadBillFeignClient;
	/**
	 * 
	 * @param tbLoadBillVo
	 * @return
	 * @author yuwei
	 * @Description: 带条件查询所有运单表的信息
	 * 2018年11月12日上午10:02:07
	 */
	@GetMapping("/selectByBill")
	public Result<String>  selectByBill(LoadBillVo tbLoadBillVo){
		return loadBillFeignClient.selectByBill(tbLoadBillVo);
		
	}
	/**
	 * 
	 * @param loadBill
	 * @author yuwei
	 * @Description: 保存运单
	 * 2018年11月12日上午10:24:56
	 */
	@GetMapping("/saveBill")
	public  Result<String> saveBill(LoadBill loadBill){
		return loadBillFeignClient.saveBill(loadBill);
		
	}
	
	/**
	 * 
	 * @param billId
	 * @return
	 * @author yuwei
	 * @Description: 核单
	 * 2018年11月22日上午10:33:01
	 */
	@RequestMapping("/billAuditing")
	public  Result<String> billAuditing(Long billId){
		return loadBillFeignClient.billAuditing(billId);
		
	}
	/**
	 * 
	 * @param loadException
	 * @author yuwei
	 * @Description: 保存运单异常的数据
	 * 2018年11月19日上午11:38:47
	 */
	@GetMapping("/saveLoadException")
	public  Result<String> saveLoadException(LoadException loadException){
		
		return loadBillFeignClient.saveLoadException(loadException);
		
	}
	/**
	 * 
	 * @param billId
	 * @param status
	 * @author yuwei
	 * @Description: 取消核单改变状态
	 * 2018年11月12日上午10:27:57
	 */
	@GetMapping("/updateByStatus")
	public  Result<String> updateByStatus(Long billId){
		return loadBillFeignClient.updateByStatus(billId);
		
	}
	/**
	 * 
	 * @param tbLoadBillVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 收运的运单导出
	 * 2018年11月12日下午2:23:45
	 */
	@GetMapping("/loadBillExport")
	public Result<String> loadBillExport(LoadBillVo tbLoadBillVo,HttpServletResponse response, HttpServletRequest request) {
	      return loadBillFeignClient.loadBillExport(tbLoadBillVo, response, request);
	      
	}
}
