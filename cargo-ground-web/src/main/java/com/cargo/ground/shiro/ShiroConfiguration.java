package com.cargo.ground.shiro;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.ExecutorServiceSessionValidationScheduler;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.core.RedisTemplate;

import com.cargo.ground.shiro.listener.ShiroSessionListener;
import com.cargo.ground.util.LoggerUtil;

 @Configuration
public class ShiroConfiguration {
	
	private Logger logger = LoggerUtil.getLogger();

	@Resource
    private RedisTemplate<String,Object> redisTemplate;

	@Bean
	public RedisSessionDAO  redisSessionDAO(){
        RedisSessionDAO  sessionDAO = new RedisSessionDAO();
        sessionDAO.setSessionIdGenerator(sessionIdGenerator());
        sessionDAO.setCacheManager(redisCacheManager());
        sessionDAO.redisTemplate=redisTemplate;
        return sessionDAO;
    }
	 
	/**
	 * 配置session监听
	 * @return
	 */
	@Bean("sessionListener")
	public ShiroSessionListener sessionListener(){
	    ShiroSessionListener sessionListener = new ShiroSessionListener();
	    return sessionListener;
	}
	
	/**
	 * 配置会话ID生成器
	 * @return
	 */
	@Bean
	public SessionIdGenerator sessionIdGenerator() {
	    return new JavaUuidSessionIdGenerator();
	}
	
    @Bean
    public RedisCacheManager redisCacheManager() {
        RedisCacheManager manager=new RedisCacheManager();
        manager.setRedisTemplate(redisTemplate);
        return manager;
    }

    @Bean(name = "shiroRealm")
    public ShiroRealm shiroRealm(RedisCacheManager redisCacheManager) {
        ShiroRealm realm = new ShiroRealm();
      //告诉realm,使用credentialsMatcher加密算法类来验证密文
        realm.setCredentialsMatcher(hashedCredentialsMatcher());
        realm.setCacheManager(redisCacheManager);
        return realm;
    }
   /**
     * 
    *http://blog.csdn.net/wuxuyang_7788/article/details/70141812
    */
    @Bean(name = "lifecycleBeanPostProcessor")
    public static LifecycleBeanPostProcessor getLifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    @DependsOn({"lifecycleBeanPostProcessor"})
    public DefaultAdvisorAutoProxyCreator getDefaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator daap = new DefaultAdvisorAutoProxyCreator();
        daap.setProxyTargetClass(true);
        return daap;
    }
   
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager securityManager(ShiroRealm shiroRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(shiroRealm);
        securityManager.setSessionManager(getSessionManage());
        securityManager.setCacheManager(redisCacheManager());
        //配置记住我
        securityManager.setRememberMeManager(getCookieRememberMeManager());
        return securityManager;
    }

    @Bean(name = "sessionManager")
    public SessionManager getSessionManage() {
    	SessionManager sessionManager = new SessionManager();
       
        sessionManager.setDeleteInvalidSessions(true);
        sessionManager.setSessionIdCookieEnabled(true);
        sessionManager.setSessionIdCookie(getSessionIdCookie());
      //  EnterpriseCacheSessionDAO cacheSessionDAO = new RedisSessionDAO();
        sessionManager.setCacheManager(redisCacheManager());
        sessionManager.setSessionDAO(redisSessionDAO());
      // -----可以添加session 创建、删除的监听器
        Collection<SessionListener> listeners = new LinkedList<SessionListener>();
        listeners.add(sessionListener());
		sessionManager.setSessionListeners(listeners);
        
      //全局会话超时时间（单位毫秒），默认30分钟  暂时设置为10秒钟 用来测试
        sessionManager.setGlobalSessionTimeout(1800000);
        //是否开启删除无效的session对象  默认为true
        sessionManager.setDeleteInvalidSessions(true);
        //是否开启定时调度器进行检测过期session 默认为true
        sessionManager.setSessionValidationSchedulerEnabled(true);
//        sessionManager.setSessionValidationScheduler(getExecutorServiceSessionValidationScheduler());
        //设置session失效的扫描时间, 清理用户直接关闭浏览器造成的孤立会话 默认为 1个小时
        //设置该属性 就不需要设置 ExecutorServiceSessionValidationScheduler 底层也是默认自动调用ExecutorServiceSessionValidationScheduler
        sessionManager.setSessionValidationInterval(3600000);
        return sessionManager;
    }


    @Bean(name = "sessionValidationScheduler")
    public ExecutorServiceSessionValidationScheduler getExecutorServiceSessionValidationScheduler() {
        ExecutorServiceSessionValidationScheduler scheduler = new ExecutorServiceSessionValidationScheduler();
        scheduler.setInterval(900000);
        return scheduler;
    }

    
    @Bean
    public AuthorizationAttributeSourceAdvisor getAuthorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor aasa = new AuthorizationAttributeSourceAdvisor();
        aasa.setSecurityManager(securityManager);
        return aasa;
    }

    /**
     * 加载shiroFilter权限控制规则
     *
     */
    private void loadShiroFilterChain(ShiroFilterFactoryBean shiroFilterFactoryBean) {
        /////////////////////// 下面这些规则配置最好配置到配置文件中 ///////////////////////
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();
        // authc：该过滤器下的页面必须验证后才能访问，它是Shiro内置的一个拦截器org.apache.shiro.web.filter.authc.FormAuthenticationFilter
//        // anon：它对应的过滤器里面是空的,什么都没做
//        logger.info("##################从数据库读取权限规则，加载到shiroFilter中##################");
//        filterChainDefinitionMap.put("/user/edit/**", "authc,perms[user:edit]");// 这里为了测试，固定写死的值，也可以从数据库或其他配置中读取
        filterChainDefinitionMap.put("/**", "anon");//anon 可以理解为不拦截
        filterChainDefinitionMap.put("/user/login", "anon");
        filterChainDefinitionMap.put("/user/logout", "anon");
        filterChainDefinitionMap.put("/user/authcode", "anon");
        filterChainDefinitionMap.put("/user/admin", "anon");
        //filterChainDefinitionMap.put("/user/**", "authc");// 这里为了测试，只限制/user，实际开发中请修改为具体拦截的请求规则
        //这个配置可以理解为拦截，authFilter主要用于拦截未登录请求，主动返回规范JSON;user标识如果启用了记住我，则自动登录
        filterChainDefinitionMap.put("/**", "authFilter,user");//anon 可以理解为不拦截
        

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        //shiroFilterFactoryBean.getFilters().put("jCaptchaValidate", getJCaptchaValidateFilter());
    }


    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(DefaultWebSecurityManager securityManager) {
//    	public ShiroFilterFactoryBean getShiroFilterFactoryBean(DefaultWebSecurityManager securityManager) {

        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        
        // 拦截器.
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();
        // 配置不会被拦截的链接 顺序判断
       // filterChainDefinitionMap.put("/**", "anon");//anon 可以理解为不拦截
        filterChainDefinitionMap.put("/res/assets/**", "anon");
        filterChainDefinitionMap.put("/res/static/**", "anon");
        
        //暴露登录接口
        filterChainDefinitionMap.put("/loginCheck", "anon");
        

        // 配置退出过滤器,其中的具体的退出代码Shiro已经替我们实现了
        filterChainDefinitionMap.put("/loginout", "logout");
        
      
        filterChainDefinitionMap.put("/**", "anon");

        // <!-- authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问-->
//        filterChainDefinitionMap.put("/**", "authc");//该段代码必须放置在最后，否则所有请求接口都被拦截
        //filterChainDefinitionMap.put("/**", "anon");//该段代码必须放置在最后，否则所有请求接口都被拦截
        
      //设置未登陆状态下请求至登陆页面
        shiroFilterFactoryBean.setLoginUrl("/login");
        // 未授权界面;
        shiroFilterFactoryBean.setUnauthorizedUrl("/error");
        // 登录成功后要跳转的链接
        shiroFilterFactoryBean.setSuccessUrl("/index");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        logger.info("@@@@@@@@@@ --> begin init shiroFilter success!");
        
        
        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
//        shiroFilterFactoryBean.setLoginUrl("/pyramid/user/nologin");
        // 登录成功后要跳转的连接
//        shiroFilterFactoryBean.setSuccessUrl("/user");
//        shiroFilterFactoryBean.setUnauthorizedUrl("/403");
//        shiroFilterFactoryBean.getFilters().put("authFilter",new AuthcFilter());
//        loadShiroFilterChain(shiroFilterFactoryBean);
        
        
        return shiroFilterFactoryBean;
    }
    
    
    /**
     * @return
     */
    @Bean(name = "credentialsMatcher")
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
//        hashedCredentialsMatcher.setHashAlgorithmName(PasswordHelper.algorithmNmae);
//        hashedCredentialsMatcher.setHashIterations(PasswordHelper.hashIterations);
        hashedCredentialsMatcher.setHashAlgorithmName("MD5");
        hashedCredentialsMatcher.setHashIterations(1);
        hashedCredentialsMatcher.setStoredCredentialsHexEncoded(true);
        return hashedCredentialsMatcher;
    }


    @Bean(name = "rememberMeCookie")
    public SimpleCookie getRememberMeCookie() {
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        simpleCookie.setHttpOnly(true);
        simpleCookie.setMaxAge(2592000);//30天
        return simpleCookie;
    }


    @Bean(name = "sessionIdCookie")
    public SimpleCookie getSessionIdCookie() {
        SimpleCookie cookie = new SimpleCookie("mysid");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(-1);
        return cookie;
    }


    @Bean(name = "rememberMeManager")
    public CookieRememberMeManager getCookieRememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager =
                new CookieRememberMeManager();
        cookieRememberMeManager.setCipherKey(
                org.apache.shiro.codec.Base64.decode("4AvVhmFLUs0KTA3Kprsdag=="));
        cookieRememberMeManager.setCookie(getRememberMeCookie());
        return cookieRememberMeManager;
    }
}
