package com.cargo.addgoods.param;

import com.cargo.ground.common.param.PageSearchParam;

public class SysLogSearchParam extends PageSearchParam {

	private String logname;
	private String serverId;

	/**
	 * @return the serverId
	 */
	public String getServerId() {
		return serverId;
	}
	/**
	 * @param serverId the serverId to set
	 */
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}


	/**
	 * @return the logname
	 */
	public String getLogname() {
		return logname;
	}

	/**
	 * @param logname the logname to set
	 */
	public void setLogname(String logname) {
		this.logname = logname;
	}




}
