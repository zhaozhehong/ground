package com.cargo.addgoods.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadWeight;
import com.cargo.addgoods.vo.LoadWeightVo;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.ground.util.FileUtil;
import com.cargo.addgoods.service.LoadWeightService;
/**
 * 
 * 
 * @author yuwei
 * @Description: 轻泡超重相关的方法
 * 2018年11月12日上午9:47:20
 */

@RestController
public class LoadWeightController {
	
	@Autowired
	LoadWeightService loadWeightService;
	
	/**
	 * 
	 * @param tbLoadWeightVo
	 * @param response
	 * @param request
	 * @author yuwei
	 * @Description: 轻泡超重的导出
	 * 2018年11月8日下午2:12:17
	 */
	@RequestMapping("/loadWeightExport")
	public Result<String> loadWeightExport(@RequestBody LoadWeightVo tbLoadWeightVo,HttpServletResponse response, HttpServletRequest request) {
		 List<LoadWeight> list=loadWeightService.selectListAll(tbLoadWeightVo);
	      FileUtil.exportExcel(list, "轻泡超重", "轻泡超重", LoadWeight.class, "轻泡超重", response, request);
	      return ResultUtil.success();
	}
	
/**
 * 
 * @param tbLoadWeightVo
 * @return
 * @author yuwei
 * @Description: 有查询条件的查询所有的轻泡超重的数据
 * 2018年11月8日下午2:20:14
 */
	@RequestMapping("/findLoadWeight")
	public  Result<LoadWeight>  findLoadWeight(@RequestBody LoadWeightVo tbLoadWeightVo){
		
		List<LoadWeight> list=loadWeightService.selectListAll(tbLoadWeightVo);
		
		return ResultUtil.success(list);
		
	}
}
