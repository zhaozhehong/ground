package com.cargo.addgoods.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadFlight;
import com.cargo.addgoods.vo.LoadFlightVo;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.addgoods.service.LoadFlightService;

/**
 * 
 * 
 * @author yuwei
 * @Description: 航班动态相关的业务
 * 2018年11月12日下午2:00:44
 */
@RestController
public class LoadFlightController {
	@Autowired
	LoadFlightService loadFlightService;
	
	/**
	 * 
	 * @param loadFlightVo
	 * @return
	 * @author yuwei
	 * @Description: 有查询条件的查询所有的航班动态的数据
	 * 2018年11月12日下午2:06:11
	 */
	@RequestMapping("/selectByFlight")
	public Result<LoadFlight>   selectByFlight(@RequestBody LoadFlightVo loadFlightVo){
		
		List<LoadFlight>  list= loadFlightService.findListAll(loadFlightVo);
		
		return ResultUtil.success(list);
		
	}
	/**
	 * 
	 * @param loadFlightVo
	 * @return
	 * @author yuwei
	 * @Description: 加货大屏展示
	 * 2018年11月12日下午2:06:41
	 */
	@RequestMapping("/findByMachine")
	public   Result<LoadFlightVo>  findByMachine(@RequestBody LoadFlightVo loadFlightVo){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String currentTime =formatter.format(new Date()); 
		List<LoadFlightVo> list=loadFlightService.findByMachine(currentTime+" 00:00:00 ",currentTime+" 23:59:59 ");
		return ResultUtil.success(list);
	}
}
