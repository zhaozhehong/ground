package com.cargo.addgoods.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NestedIOException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.ground.enums.ExceptionEnum;
import com.cargo.ground.exception.BusinessException;
import com.cargo.ground.response.Result;
import com.cargo.ground.response.ResultUtil;
import com.cargo.ground.util.FileUtil;
import com.cargo.addgoods.service.LoadArrangeService;

/**
 * 
 * 
 * @author yuwei
 * @Description: 机口排班相关的业务
 * 2018年11月12日上午10:40:35
 */
@RestController
public class LoadArrangeController {
	protected static final Logger logger = LoggerFactory.getLogger(LoadArrangeController.class);
	@Autowired
	LoadArrangeService loadArrangeService;
	/**
	 * 
	 * @param flightNumber
	 * @param machine
	 * @return
	 * @author yuwei
	 * @Description: 带条件的查询机口排班的数据
	 * 2018年11月12日上午10:55:30
	 */
	@RequestMapping("/selectByArrange")
	public  Result<LoadArrange> selectByArrange(String flightNumber,String machine){
		
		List<LoadArrange> list=loadArrangeService.findListAll(flightNumber, machine);
		
		return ResultUtil.success(list);
		
	}
	/**
	 * 
	 * @param loadArrange
	 * @author yuwei
	 * @Description: 保存方法
	 * 2018年11月12日上午10:57:23
	 */
	@RequestMapping("/saveArrange")
	public  Result<String> saveArrange(@RequestBody LoadArrange loadArrange){
		
		loadArrangeService.insert(loadArrange);
		return ResultUtil.success();
		
	}
	
	/**
	 * 
	 * @param loadArrange
	 * @author yuwei
	 * @Description: 更新方法
	 * 2018年11月12日上午11:01:04
	 */
	@RequestMapping("/updateArrange")
	public  Result<String> updateArrange(@RequestBody LoadArrange loadArrange){
		loadArrangeService.update(loadArrange);
		return ResultUtil.success();
		
	}
	/**
	 * 
	 * @param id
	 * @author yuwei
	 * @Description:逻辑删除 
	 * 2018年11月12日上午11:00:41
	 */
	@RequestMapping("/deleteArrange")
	public  Result<String> deleteArrange(Long id){
		loadArrangeService.deleteById(id);
		return ResultUtil.success();
		
	}
	/**
	 * 
	 * @return
	 * @author yuwei
	 * @Description: 机口排班的导入
	 * 2018年11月20日下午7:19:05
	 */
	@RequestMapping("/loadArrangeImport")
	public Result<String> loadArrangeImport() {
		 String filePath = "D:\\TSBrowserDownloads\\KuGou20181108105834.xlsx";
	     
			List<LoadArrange> LoadArrangelist=null;
			try {
				LoadArrangelist = FileUtil.importExcel(filePath,0,1,LoadArrange.class);
				for(LoadArrange loadArrange:LoadArrangelist) {
					loadArrange.setIsDeleted(0);
					if(loadArrange.getFlyDate()!=null) {
						loadArrangeService.insert(loadArrange);
					}
					
				}
			} catch (NestedIOException e) {
				logger.info("调用导入接口发生异常" + e.getMessage());
				e.printStackTrace();
				throw new BusinessException(ExceptionEnum.CODE_900003);
			}
			return ResultUtil.success();
	}
	
}
