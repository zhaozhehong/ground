package com.cargo.addgoods.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.addgoods.service.LoadArrangeService;
import com.cargo.user.entity.User;

@RestController
public class DemoController {
	@Autowired
	private LoadArrangeService loadArrangeService;
	
	@GetMapping("/getById/{id}")
	public LoadArrange getById(@PathVariable Long id) {
//		List<LoadArrange> findListAll = loadArrangeService.findListAll(null, null);
		System.out.println("test ribbon ***********************************************");
		LoadArrange la = new LoadArrange();
		la.setFlightType("SZX");
		la.setEndPort("深圳");
		return la;
	}
	
	@RequestMapping("/getById2")
	public String getById2(@RequestBody User user) {
		System.out.println("userName: "+ user.getUserName());
		return "kkk" ;
	}
}
