package com.cargo.addgoods.mapper;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.vo.LoadBillVo;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadBillMapper extends BaseMapper<LoadBill> {
	/***
	 * 带条件查询所有运单表的信息
	 * @param tbLoadBillVo
	 * @return
	 */
	 List<LoadBill> findListAll(LoadBillVo tbLoadBillVo);
	 
	  /**
	     * 更新运单状态
	     * @param record
	     * @return
	     */
	    int updateByStatus(@Param("billId")Long billId,@Param("status")Integer status);
	    
	    /**
	     * 
	     * @param tbLoadBillVo
	     * @return
	     * @author yuwei
	     * @Description: 通过查询条件返回列表的票数，合计件数，合计重量
	     * 2018年11月13日下午2:00:45
	     */
		@SuppressWarnings({ "rawtypes" })
	    Map totalBill(LoadBillVo tbLoadBillVo);
}