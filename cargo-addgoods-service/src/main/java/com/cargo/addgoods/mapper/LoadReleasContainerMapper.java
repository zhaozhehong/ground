package com.cargo.addgoods.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cargo.addgoods.entity.LoadReleasContainer;
import com.cargo.addgoods.vo.LoadContainerVo;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadReleasContainerMapper extends BaseMapper<LoadReleasContainer> {
    int insertSelective(LoadReleasContainer record);
    /**
	   * 
	   * @param loadContainerVo
	   * @return
	   * @author yuwei
	   * @Description: 带条件查询某个容器下的释放时间
	   * 2018年11月16日下午4:55:28
	   */
    List<String> selectByRelesedate(LoadContainerVo loadContainerVo);
    /**
     * 
     * @param container
     * @param releaseDate
     * @param restored
     * @return
     * @author yuwei
     * @Description: 改变容器状态
     * 2018年11月17日下午5:26:06
     */
    int updateByreleased( @Param("container")String  container,@Param("releaseDate")String  releaseDate,@Param("restored") Integer restored);
    
    
}