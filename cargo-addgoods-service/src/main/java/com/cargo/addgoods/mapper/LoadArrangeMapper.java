package com.cargo.addgoods.mapper;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cargo.addgoods.entity.LoadArrange;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadArrangeMapper extends BaseMapper<LoadArrange> {
	
	/**
	 * 带条件的查询机口排班的数据
	 * @param flightNumber
	 * @param machine
	 * @return
	 */
	 List<LoadArrange> findListAll(@Param("flightNumber")String flightNumber,@Param("machine")String machine );
	 
	 /**
      * 逻辑删除
    * @param id
    * @return
    */
   int deleteById(@Param("arrangeId")Long id);
	 
}