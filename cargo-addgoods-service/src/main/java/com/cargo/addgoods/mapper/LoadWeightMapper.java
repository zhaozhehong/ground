package com.cargo.addgoods.mapper;

import java.util.List;

import com.cargo.addgoods.entity.LoadWeight;
import com.cargo.addgoods.vo.LoadWeightVo;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadWeightMapper extends BaseMapper<LoadWeight> {
	/**
	 * 有查询条件的查询所有的轻泡超重的数据
	 * @param tbLoadWeightVo
	 * @return
	 */
	
	 List<LoadWeight> selectListAll(LoadWeightVo tbLoadWeightVo);

}
