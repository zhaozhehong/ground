package com.cargo.addgoods.mapper;
import com.cargo.addgoods.entity.DictInfo;
import com.cargo.ground.common.base.BaseMapper;

public interface DictInfoMapper extends BaseMapper<DictInfo> {
	 
}