package com.cargo.addgoods.mapper;

import com.cargo.addgoods.entity.LoadException;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadExceptionMapper extends BaseMapper<LoadException> {

}
