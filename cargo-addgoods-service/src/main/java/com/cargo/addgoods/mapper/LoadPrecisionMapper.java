package com.cargo.addgoods.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cargo.addgoods.entity.LoadPrecision;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadPrecisionMapper extends BaseMapper<LoadPrecision> {
	List<LoadPrecision> selectByName(@Param("precision_name")String  precision_name);
	
	
	void deleteByid(Long precision_id);
}
