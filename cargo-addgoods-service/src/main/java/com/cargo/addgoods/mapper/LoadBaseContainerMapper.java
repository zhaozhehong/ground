package com.cargo.addgoods.mapper;
import com.cargo.addgoods.entity.LoadBaseContainer;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadBaseContainerMapper extends BaseMapper<LoadBaseContainer> {
	 
}