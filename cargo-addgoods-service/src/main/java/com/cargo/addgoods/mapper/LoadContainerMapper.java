package com.cargo.addgoods.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.entity.LoadContainer;
import com.cargo.ground.common.base.BaseMapper;


public interface LoadContainerMapper extends BaseMapper<LoadContainer> {

    //根据容器号   获取该容器号中所有的备注信息
    public List<Map<String, Object>> getCommentByContainerNum(Long num);

    //根据容器号，获取该容器内所有的运单
    public List<LoadBill> getloadBills(Long num);

    //条件查询
    public List<LoadContainer> queryByFlightId(Long flightId);

    //查询默认的容器
    public List<LoadContainer> queryDefLoadContainers();

    /**
     * @param container
     * @param released
     * @author yuwei
     * @Description: 通过容器标识查询容器信息
     * 2018年11月17日下午3:37:46
     */
    public LoadContainer selectByContainer(@Param("container") String container, @Param("released") Integer released);

    /**
     * @param container
     * @param releaseDate
     * @param released
     * @author yuwei
     * @Description: 通过条件改变状态
     * 2018年11月17日下午5:54:04
     */
    int updateByreleased(@Param("container") String container, @Param("releaseDate") String releaseDate, @Param("released") Integer released);

    //更新体积
    int updateVolume(Double volume, String containerCode);

    //舱位
    int updateSpaces(String spaces, String containerCode);

    //备注
    int updateMark(String codes, String containerCode);

}