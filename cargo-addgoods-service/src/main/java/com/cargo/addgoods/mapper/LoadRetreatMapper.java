package com.cargo.addgoods.mapper;


import java.util.List;

import com.cargo.addgoods.entity.LoadRetreat;
import com.cargo.addgoods.vo.BillRetreatVo;
import com.cargo.addgoods.vo.RetreatVo;
import com.cargo.ground.common.base.BaseMapper;

public interface LoadRetreatMapper extends BaseMapper<LoadRetreat>{
	
	/**
	 * 退运原因关联运单表
	 * @param tbLoadWeightVo
	 * @return
	 */
	
	List<BillRetreatVo> selectByRetreat(BillRetreatVo billRetreatVo);
	
	/**
	 * 
	 * @param billRetreatVo
	 * @return
	 * @author yuwei
	 * @Description: 锂电池退运原因关联运单表
	 * 2018年11月8日下午3:50:34
	 */
	List<RetreatVo> selectByRetreatbattery(BillRetreatVo billRetreatVo);

}
