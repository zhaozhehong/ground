package com.cargo.addgoods.service;

import java.util.List;

import com.cargo.addgoods.entity.LoadWeight;
import com.cargo.addgoods.vo.LoadWeightVo;

/**
 * 
 * @author Administrator
 *
 */
 
public interface LoadWeightService  {
	/**
	 * 有查询条件的查询所有的轻泡超重的数据
	 * @param tbLoadWeightVo
	 * @return
	 */
	
	 List<LoadWeight> selectListAll(LoadWeightVo tbLoadWeightVo);
  
}
