package com.cargo.addgoods.service;

import java.util.List;

import com.cargo.addgoods.entity.LoadFlight;
import com.cargo.addgoods.vo.LoadFlightVo;
/**
 * 
 * @author Administrator
 *
 */
 
public interface LoadFlightService  {
	/**
	 * 有查询条件的查询所有的航班动态的数据
	 * @param tbLoadWeightVo
	 * @return
	 */
	
	 List<LoadFlight> findListAll(LoadFlightVo tbLoadFlightVo);
	 
	 /**
	  * 
	  * @return
	  * @author yuwei
	  * @Description: 加货大屏展示
	  * 2018年11月6日下午3:46:43
	  */
	 
	 List<LoadFlightVo> findByMachine(String currentStart,String currentEnd);
  
}
