package com.cargo.addgoods.service;

import java.util.List;

import com.cargo.addgoods.entity.LoadPreplan;
import com.cargo.addgoods.vo.LoadPreplanVo;
import com.cargo.ground.entity.CheckInBoxVo;

/**
 * 
 * @author Administrator
 *
 */
 
public interface LoadPreplanService  {
	/**
	 * 
	 * @param container
	 * @param releaseDate
	 * @param released
	 * @return
	 * @author yuwei
	 * @Description: 查询预配清单表的信息
	 * 2018年11月17日上午11:33:22
	 */
	List<LoadPreplanVo> selectByRelease(String  container,String  releaseDate,Integer released);
	/**
	 * 
	 * @param preplanId
	 * @param released
	 * @return
	 * @author yuwei
	 * @Description: 通过主键修改释放的状态
	 * 2018年11月17日下午4:37:36
	 */
	int updateByreleased(Long preplanId,Integer released);

	/**
	 * @param preplanId
	 * @param released
	 * @return
	 * @author xuxu
	 * @Description: 通过主键修改释放的状态
	 * 2018年11月17日下午4:37:36
	 * */
	List<LoadPreplan> queryByContainorNum(String containerNum);

	//核单进箱
    int inputContainer(CheckInBoxVo checkInBoxVo);
}
