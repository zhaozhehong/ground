package com.cargo.addgoods.service;

import java.util.List;

import com.cargo.addgoods.entity.LoadReleasContainer;
import com.cargo.addgoods.vo.LoadContainerVo;

/**
 * 
 * @author Administrator
 *
 */
 
public interface LoadReleasContainerService  {
	/**
	 * 
	 * @param record
	 * @return
	 * @author yuwei
	 * @Description: 保存
	 * 2018年11月15日下午2:18:40
	 */
	  int insertSelective(LoadReleasContainer record);
	  /**
	   * 
	   * @param loadContainerVo
	   * @return
	   * @author yuwei
	   * @Description: 带条件查询某个容器下的释放时间
	   * 2018年11月16日下午4:55:28
	   */
	  List<String> selectByRelesedate(LoadContainerVo loadContainerVo);
	  /**
	   * 
	   * @param loadReleasContainer
	   * @return
	   * @author yuwei
	   * @Description: 通过条件查询一条数据
	   * 2018年11月17日下午5:48:43
	   */
	  LoadReleasContainer  selectByLoadReleasContainer(LoadReleasContainer loadReleasContainer);
}
