package com.cargo.addgoods.service;

import java.util.List;

/**
 * @Desc 地磅服务
 * @LastPeson xuxu
 **/
public interface LoadWeighBridgeService {

    //地磅历史数据保存
    public int save();

    //地磅历史数据查询
    public List<Object> queryByMap();

}
