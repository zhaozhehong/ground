package com.cargo.addgoods.service.impl;
import com.cargo.addgoods.entity.LoadFlight;
import com.cargo.addgoods.vo.LoadFlightVo;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.addgoods.mapper.LoadFlightMapper;
import com.cargo.addgoods.service.LoadFlightService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 
 * @author Administrator
 *
 */


@Service("tbLoadFlightService")
public class LoadFlightServiceImpl   implements LoadFlightService {
	@Autowired
	LoadFlightMapper tbLoadFlightMapper;

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadFlight> findListAll(LoadFlightVo tbLoadFlightVo) {
		return tbLoadFlightMapper.findListAll(tbLoadFlightVo);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<LoadFlightVo> findByMachine(String currentStart,String currentEnd) {
		return tbLoadFlightMapper.findByMachine(currentStart, currentEnd);
	}

		 
			
   
}
