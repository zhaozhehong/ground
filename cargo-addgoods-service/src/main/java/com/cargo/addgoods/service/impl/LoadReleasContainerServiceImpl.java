package com.cargo.addgoods.service.impl;
import com.cargo.addgoods.entity.LoadReleasContainer;
import com.cargo.addgoods.vo.LoadContainerVo;
import com.cargo.ground.datasource.DataSourceTypeAnno;
import com.cargo.ground.datasource.DbType;
import com.cargo.addgoods.mapper.LoadReleasContainerMapper;
import com.cargo.addgoods.service.LoadReleasContainerService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * 
 * @author Administrator
 *
 */


@Service("loadReleasContainerService")
public class LoadReleasContainerServiceImpl   implements LoadReleasContainerService {
	@Autowired
	LoadReleasContainerMapper loadReleasContainerMapper;

	@DataSourceTypeAnno(DbType.oracleCargoGroundMaster)
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	@Override
	public int insertSelective(LoadReleasContainer record) {
		return loadReleasContainerMapper.insertSelective(record);
	}
	
	  /**
	   * 
	   * @param loadContainerVo
	   * @return
	   * @author yuwei
	   * @Description: 带条件查询某个容器下的释放时间
	   * 2018年11月16日下午4:55:28
	   */
	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public List<String> selectByRelesedate(LoadContainerVo loadContainerVo) {
		return loadReleasContainerMapper.selectByRelesedate(loadContainerVo);
	}

	@DataSourceTypeAnno(DbType.oracleCargoGroundSlave)
	@Transactional(readOnly=true,propagation=Propagation.SUPPORTS)
	@Override
	public LoadReleasContainer selectByLoadReleasContainer(LoadReleasContainer loadReleasContainer) {
		return loadReleasContainerMapper.selectOne(loadReleasContainer);
	}

	

		 
			
   
}
