package com.cargo.addgoods.service;


import java.util.List;
import java.util.Map;

import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.vo.LoadBillVo;

/**
 * @author Administrator
 */

public interface LoadBillService {
    /***
     * 带条件查询所有运单表的信息
     * @param tbLoadBillVo
     * @return
     */
    List<LoadBill> findListAll(LoadBillVo tbLoadBillVo);

    /**
     * 更新运单状态
     *
     * @param record
     * @return
     */
    int updateByStatus(Long billId, Integer status);

    /**
     * @param loadBill
     * @author yuwei
     * @Description:核单通过插入一条数据到数据库 2018年11月12日上午10:22:26
     */
    void saveBill(LoadBill loadBill);
    
    /**
     * 
     * @param billId
     * @return
     * @author yuwei
     * @Description: 查询单条运单信息
     * 2018年11月22日上午9:58:38
     */
    LoadBill selectById(Long billId);
    /**
     * 
     * @param loadBill
     * @return
     * @author yuwei
     * @Description: 更新运单信息
     * 2018年11月22日上午10:08:47
     */
    int update(LoadBill loadBill);

    /**
     * @param tbLoadBillVo
     * @return
     * @author yuwei
     * @Description: 通过查询条件返回列表的票数，合计件数，合计重量
     * 2018年11月13日下午2:00:45
     */
    @SuppressWarnings({"rawtypes"})
    Map totalBill(LoadBillVo tbLoadBillVo);

    /**
     * 根据运单查询号查询
     * */
    LoadBill queryByNum(String num);


}
