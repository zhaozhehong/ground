package com.cargo.addgoods.service;

import com.cargo.addgoods.entity.LoadException;

/**
  * 
  * 
  * @author yuwei
  * @Description: 航班异常和运单异常信息都存入这张表里面
  * 2018年11月19日上午11:27:17
  */
public interface LoadExceptionService  {
	/**
	 * 
	 * @param loadException
	 * @return
	 * @author yuwei
	 * @Description: 保存异常的方法
	 * 2018年11月19日上午11:35:15
	 */
	int saveLoadException(LoadException loadException);
  
}
