package com.cargo.addgoods.service;

import java.util.List;

import com.cargo.addgoods.vo.BillRetreatVo;
import com.cargo.addgoods.vo.RetreatVo;
/**
 * 
 * @author Administrator
 *
 */
 
public interface LoadRetreatService  {
	/**
	 * 退运原因关联运单表
	 * @param tbLoadWeightVo
	 * @return
	 */
	
	List<BillRetreatVo> selectByRetreat(BillRetreatVo billRetreatVo);
	
	
	/**
	 * 
	 * @param billRetreatVo
	 * @return
	 * @author yuwei
	 * @Description: 锂电池退运原因关联运单表
	 * 2018年11月8日下午3:50:34
	 */
	List<RetreatVo> selectByRetreatbattery(BillRetreatVo billRetreatVo);
	
	/**
	 * 
	 * @param retreatId
	 * @return
	 * @author yuwei
	 * @Description: 取消退运
	 * 2018年11月20日上午11:36:14
	 */
	void delById(Long retreatId,Long billId);
  
}
