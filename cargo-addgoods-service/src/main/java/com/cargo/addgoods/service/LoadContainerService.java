package com.cargo.addgoods.service;

import java.util.List;
import java.util.Map;

import com.cargo.addgoods.entity.LoadBill;
import com.cargo.addgoods.entity.LoadContainer;

public interface LoadContainerService {
	//根据容器号   获取该容器号中所有的备注信息
	 public List<Map<String, Object>> getCommentByContainer(String num);
	 
	 //根据容器号，获取该容器内所有的运单信息
	 public List<LoadBill> getBills(Long num);

	 //根据航班号,查询所有容器
	public List<LoadContainer> queryLoadContainersByFlightNum(Long flightNum);

	//查询默认容器
	public List<LoadContainer> queryDefLoadContainers();
	/**
	 * 
	 * @param container
	 * @param released
	 * @return
	 * @author yuwei
	 * @Description: 通过容器标识查询容器信息
	 * 2018年11月17日下午3:37:46
	 */
	public LoadContainer selectByContainer(String  container,Integer released);
	/**
	 * 
	 * @param container
	 * @param releaseDate
	 * @param containerId
	 * @return
	 * @author yuwei
	 * @Description: 容器还原的方法
	 * 2018年11月17日下午4:56:50
	 */
	public void restoreContainer(String  container,String releaseDate,Long containerId);


	/**
	 *
	 * @return
	 * @author xuxu
	 * @Description: 容器释放
	 * 2018年11月17日下午4:56:50
	 */
	public Integer freeContainer(String  containerNum,String opener);

	/**
	 * 保存 加货备注 舱位 体积
	 * @param volume
	 * @param spaces
	 * @param containerCode
	 * @param codes
	 */
	void updateMark(Double volume, String spaces, String containerCode, List<String> codes);
}
