package com.cargo.ground.service;
import org.springframework.stereotype.Service;

import com.cargo.ground.common.base.BaseService;
import com.cargo.ground.entity.UserEntity;
@Service
public interface UserInfoService extends BaseService<UserEntity> {
}