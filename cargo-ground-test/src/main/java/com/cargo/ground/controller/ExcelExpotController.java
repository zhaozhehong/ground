package com.cargo.ground.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.NestedIOException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cargo.ground.entity.CourseEntity;
import com.cargo.ground.entity.Person;
import com.cargo.ground.entity.StudentEntity;
import com.cargo.ground.entity.TeacherEntity;
import com.cargo.ground.util.FileUtil;
import com.cargo.ground.util.PropertiesUtil;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
@Controller
@RequestMapping("/excel")
public class ExcelExpotController {
	@Resource
	private PropertiesUtil propertiesUtil;

	@RequestMapping("/bigexport")
	// 大数据量的导出
	public void bigexport(HttpServletResponse response, HttpServletRequest request)
			throws FileNotFoundException, IOException {
		List<Person> personList = new ArrayList<>();
		Workbook workbook = null;

		ExportParams params = new ExportParams("大数据测试", "测试");
		for (int i = 0; i < 2000500; i++) {
			System.out.println("构造第" + i + "条");
			Person person1 = new Person("路飞" + i, "1", new Date());
			personList.add(person1);
			if (personList.size() == 10000) {
				workbook = ExcelExportUtil.exportBigExcel(params, Person.class, personList);
				personList.clear();
			}
		}
		if(personList.size() >0) {
			workbook = ExcelExportUtil.exportBigExcel(params, Person.class, personList);
			personList.clear();
		}
		
		
		String temppath = propertiesUtil.getTempPath();
		FileUtil.bigExport(response, workbook, temppath);
	}

	@RequestMapping("/export")
	// 普通的导出
	public void export(HttpServletResponse response, HttpServletRequest request) {
		// 模拟从数据库获取需要导出的数据
		List<Person> personList = new ArrayList<>();
		Date start = new Date();
		for (int i = 0; i < 1000; i++) {
			System.out.println("构造第" + i + "条");
			Person person1 = new Person("路飞" + i, "1", new Date());
			Person person2 = new Person("娜美" + i, "2", DateUtils.addDays(new Date(), 3));
			Person person3 = new Person("索隆" + i, "1", DateUtils.addDays(new Date(), 3));
			Person person4 = new Person("小狸猫" + i, "1", DateUtils.addDays(new Date(), -10));
			personList.add(person1);
			personList.add(person2);
			personList.add(person3);
			personList.add(person4);

		}
		FileUtil.exportExcel(personList, "学生测试", "学生", Person.class, "学生", response, request);
		System.out.println(new Date().getTime() - start.getTime());
	}

	@RequestMapping("/manyexport")
	// 一对多的导出
	public void onetomanyexport(HttpServletResponse response, HttpServletRequest request) {

		// 模拟从数据库获取需要导出的数据

		List<CourseEntity> courseList = new ArrayList<>();
		Date start = new Date();
		for (int i = 0; i < 10000; i++) {
			System.out.println("构造第" + i + "条");
			CourseEntity course = new CourseEntity();
			course.setName("科技" + i);
			TeacherEntity techer = new TeacherEntity();
			techer.setId(i + "");
			techer.setName("李老师" + i);
			List<StudentEntity> studentList = new ArrayList<StudentEntity>();
			for (int j = 0; j < 8; j++) {
				StudentEntity student = new StudentEntity();
				student.setBirthday(DateUtils.addDays(new Date(), 3 + j));
				student.setName("张" + j);
				student.setSex(1);
				studentList.add(student);
			}
			course.setStudents(studentList);
			course.setMathTeacher(techer);
			courseList.add(course);
		}
		FileUtil.exportExcel(courseList, "学生课程老师", "学生课程老师", CourseEntity.class, "学生课程老师", response, request);
		System.out.println(new Date().getTime() - start.getTime());
	}
	
	 @RequestMapping("/importExcel")
	    public void importExcel() throws NestedIOException{
	        String filePath = "C:\\Users\\kj\\Downloads\\学生20181105194727.xls";
	        List<Person> personList = FileUtil.importExcel(filePath,1,1,Person.class);
	        //也可以使用MultipartFile,使用 FileUtil.importExcel(MultipartFile file, Integer titleRows, Integer headerRows, Class<T> pojoClass)导入
	        System.out.println("导入数据一共【"+personList.size()+"】行");
	 
	        //TODO 保存数据库
	    }


}
