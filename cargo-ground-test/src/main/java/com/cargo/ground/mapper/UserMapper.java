package com.cargo.ground.mapper;
import com.cargo.ground.common.base.BaseMapper;
import com.cargo.ground.entity.UserEntity;

public interface UserMapper extends BaseMapper<UserEntity> {
	 
}